<?php

namespace Drupal\document_ocr\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Component\Serialization\Json;

/**
 * Document OCR tasks list builder.
 *
 * @ingroup document_ocr
 */
class TaskListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['mapping'] = $this->t('Mapping');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\document_ocr\Entity\Task $entity */
    $row['mapping'] = ($mapping = $entity->getDocumentOcr()) ? $mapping->label() : $this->t('-');
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $ajax_attributes = [
      'class' => ['use-ajax'],
      'data-dialog-type' => 'modal',
      'data-dialog-options' => Json::encode(['width' => 620]),
    ];
    if ($entity->isPending() || $entity->isInitiated()) {
      $operations['process'] = [
        'title' => $this->t('Process'),
        'url' => $this->ensureDestination($entity->toUrl('process')),
        'attributes' => $ajax_attributes,
      ];
    }
    if ($entity->isFailed()) {
      // Restart is available only if the source file exists.
      if ($entity->getFile()) {
        $operations['restart'] = [
          'title' => $this->t('Restart'),
          'url' => $this->ensureDestination($entity->toUrl('restart')),
          'attributes' => $ajax_attributes,
        ];
      }
    }
    $operations['delete'] = [
      'title' => $this->t('Delete'),
      'url' => $this->ensureDestination($entity->toUrl('delete')),
      'attributes' => $ajax_attributes,
    ];
    return $operations;
  }

}
