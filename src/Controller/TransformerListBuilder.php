<?php

namespace Drupal\document_ocr\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * Document OCR transformers list builder.
 *
 * @ingroup document_ocr
 */
class TransformerListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['label'] = $this->t('Label');
    $header['description'] = $this->t('Description');
    $header['transformer'] = $this->t('Transformer Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = new FormattableMarkup('<strong>@name</strong>', ['@name' => $entity->label()]);
    $row['description'] = $entity->getDescription();
    if ($transformer = $entity->getTransformerPlugin()) {
      $params = ['@name' => $transformer->getName(), '@ext' => ''];
      if ($types = $transformer->getFieldTypes()) {
        $params['@ext'] = $this->t('<span class="form-item__description">Supported field types: @list</span>', [
          '@list' => join(', ', $transformer->getFieldTypes()),
        ]);
      }
      $row['transformer'] = new FormattableMarkup('@name <br> @ext', $params);
    }
    else {
      $row['transformer'] = $this->t('%name is missing', ['%name' => $entity->getTransformer()]);
    }
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $ajax_attributes = [
      'class' => ['use-ajax'],
      'data-dialog-type' => 'modal',
      'data-dialog-options' => Json::encode(['width' => DocumentOcrInterface::TRANSFORMER_CONFIG_DIALOG_WIDTH]),
    ];
    $config_dialog_attributes = $ajax_attributes;
    if ($transformer = $entity->getTransformerPlugin()) {
      $config_dialog_attributes['data-dialog-options'] = Json::encode(['width' => $transformer->getConfigDialogWidth()]);
    }
    $operations = parent::getDefaultOperations($entity);
    $operations['configuration'] = [
      'title' => $this->t('Configuration'),
      'url' => $this->ensureDestination($entity->toUrl('configuration')),
      'attributes' => $config_dialog_attributes,
    ];
    $operations['edit'] = [
      'title' => $this->t('Edit'),
      'url' => $this->ensureDestination($entity->toUrl('edit')),
      'attributes' => $ajax_attributes,
    ];
    // Default transformer should be available in the UI.
    if ($entity->id() !== 'default') {
      $operations['delete'] = [
        'title' => $this->t('Delete'),
        'url' => $this->ensureDestination($entity->toUrl('delete')),
        'attributes' => $ajax_attributes,
      ];
    }
    if (!$entity->getTransformerPlugin()) {
      // If plugin is missing the only operation available is delete.
      $operations = array_intersect_key($operations, array_flip( ['delete']));
    }
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t('Whoops looks like there are no transformers configured.
      Usually Basic transformer is configured by default but it appears like you might have deleted the config for it.
      No worries you can grab it from the module directory and re-import it.');
    return $build;
  }

}
