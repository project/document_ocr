<?php

namespace Drupal\document_ocr\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * Document OCR processors list builder.
 *
 * @ingroup document_ocr
 */
class ProcessorListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['label'] = $this->t('Label');
    $header['description'] = $this->t('Description');
    $header['processor'] = $this->t('Processor Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = new FormattableMarkup('<strong>@name</strong>', ['@name' => $entity->label()]);
    $row['description'] = $entity->getDescription();
    if ($processor = $entity->getProcessorPlugin()) {
      $row['processor'] = new FormattableMarkup('@name <br> @ext', [
        '@name' => $processor->getName(),
        '@ext' => $this->t('<span class="form-item__description">Supported Files: @list</span>', [
          '@list' => $processor->getExtensions()
        ]),
      ]);
    }
    else {
      $row['processor'] = $this->t('%name is missing', ['%name' => $entity->getProcessor()]);
    }
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $ajax_attributes = [
      'class' => ['use-ajax'],
      'data-dialog-type' => 'modal',
      'data-dialog-options' => Json::encode(['width' => DocumentOcrInterface::PROCESSOR_CONFIG_DIALOG_WIDTH]),
    ];
    $config_dialog_attributes = $ajax_attributes;
    if ($processor = $entity->getProcessorPlugin()) {
      $config_dialog_attributes['data-dialog-options'] = Json::encode(['width' => $processor->getConfigDialogWidth()]);
    }

    $operations = parent::getDefaultOperations($entity);
    $operations['configuration'] = [
      'title' => $this->t('Configuration'),
      'url' => $this->ensureDestination($entity->toUrl('configuration')),
      'attributes' => $config_dialog_attributes,
    ];
    $operations['edit'] = [
      'title' => $this->t('Edit'),
      'url' => $this->ensureDestination($entity->toUrl('edit')),
      'attributes' => $ajax_attributes,
    ];
    $operations['delete'] = [
      'title' => $this->t('Delete'),
      'url' => $this->ensureDestination($entity->toUrl('delete')),
      'attributes' => $ajax_attributes,
    ];
    if (!$entity->getProcessorPlugin()) {
      // If plugin is missing the only operation available is delete.
      $operations = array_intersect_key($operations, array_flip( ['delete']));
    }
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t('Whoops looks like there are no processors configured.');
    return $build;
  }

}
