<?php

namespace Drupal\document_ocr\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Document OCR entity related controller.
 */
class HelperController extends ControllerBase {

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * HelperController constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger')
    );
  }

  /**
   * Set Document OCR active.
   */
  public function enable($document_ocr_mapping) {
    $document_ocr_mapping->set('status', 1);
    $document_ocr_mapping->save();
    $this->messenger->addMessage($this->t('%name is active', ['%name' => $document_ocr_mapping->label()]));
    return $this->redirect('entity.document_ocr_mapping.collection');
  }

  /**
   * Set Document OCR inactive.
   */
  public function disable($document_ocr_mapping) {
    $document_ocr_mapping->set('status', 0);
    $document_ocr_mapping->save();
    $this->messenger->addMessage($this->t('%name is inactive', ['%name' => $document_ocr_mapping->label()]));
    return $this->redirect('entity.document_ocr_mapping.collection');
  }

}
