<?php

namespace Drupal\document_ocr\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Render\Markup;
use Drupal\field\Entity\FieldConfig;

/**
 * Document OCR mapping list builder.
 *
 * @ingroup document_ocr
 */
class MappingListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['label'] = $this->t('Label');
    $header['source'] = $this->t('Source Bundle');
    $header['field'] = $this->t('Field to Process');
    $header['destination'] = $this->t('Destination Bundle');
    $header['processor'] = $this->t('Processor');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $source = $this->getEntityConfigInfo($entity->getFieldSelector());
    $destination = $this->getEntityConfigInfo($entity->getDestinationBundle());
    $row = [];
    $row['label']['#markup'] = '<strong>' . $entity->label() . '</strong>';
    $row['source']['#markup'] = !empty($source['bundle_label'])
      ? $source['bundle_label'] . ' <span class="form-item__description">' . $source['entity_type_label'] . '</span>'
      : '';
    $row['field']['#markup'] = $source['field_label'] . ' <span class="form-item__description">' . $source['field'] . '</span>';
    $row['destination']['#markup'] = !empty($destination['bundle_label'])
      ? $destination['bundle_label'] . ' <span class="form-item__description">' . $destination['entity_type_label'] . '</span>'
      : '';
    $settings = $entity->getSettings();
    if ($processor = $this->getProcessor($entity->getProcessor())) {
      $row['processor']['#markup'] = $processor->label() . ' <span class="form-item__description">' . (
        !empty($settings['realtime_processing']) ? $this->t('Real-time') : $this->t('Cron')
      ) . '</span>';
    }
    else {
      $row['processor']['#markup'] = ' <span class="form-item__description">' . $this->t('Missing %name plugin', ['%name' => $entity->getProcessor()]) . '</span>';
    }
    $row['status']['#markup'] = Markup::create('<span class="views-field">' . ($entity->get('status')
        ? '<span class="marker marker--published">' . $this->t('Active') . '</span>'
        : '<span class="marker">' . $this->t('Disabled') . '</span>') . '</span>');
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t('Whoops looks like there are no entity mapping configured.
      Before you add mapping make sure you have processor configured for it.');
    unset($build['table']['#rows']);
    foreach ($this->load() as $entity) {
      if ($row = $this->buildRow($entity)) {
        $build['table'][$entity->id()] = $row;
      }
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $ajax_attributes = [
      'class' => ['use-ajax'],
      'data-dialog-type' => 'modal',
      'data-dialog-options' => Json::encode(['width' => 620]),
    ];
    if ($document_ocr_processor = $entity->getProcessorReference()) {
      $operations = parent::getDefaultOperations($entity);
      $operations['settings'] = [
        'title' => $this->t('Settings'),
        'url' => $this->ensureDestination($entity->toUrl('settings')),
        'attributes' => $ajax_attributes,
      ];
      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'url' => $this->ensureDestination($entity->toUrl('edit')),
        'attributes' => $ajax_attributes,
      ];
      $processor = $document_ocr_processor->getProcessorPlugin();
      if ($this->isTemplateRequired($document_ocr_processor)) {
        $operations['template_file'] = [
          'title' => $this->t('Mapping'),
          'url' => $entity->toUrl('template_file'),
          'attributes' => $ajax_attributes,
        ];
      }
      else {
        $operations['mapping'] = [
          'title' => $this->t('Mapping'),
          'url' => $entity->toUrl('mapping'),
        ];
      }
      $operations['process'] = [
        'title' => $this->t('Process Pending'),
        'url' => $entity->toUrl('process'),
        'attributes' => $ajax_attributes,
      ];
    }
    $operations['delete'] = [
      'title' => $this->t('Delete'),
      'url' => $this->ensureDestination($entity->toUrl('delete')),
      'attributes' => $ajax_attributes,
    ];
    return $operations;
  }

  /**
   * Helper method. Quick implementetion @TODO refine this.
   */
  protected function getEntityConfigInfo($config) {
    $info = [];
    $bundle_info = \Drupal::service("entity_type.bundle.info")->getAllBundleInfo();
    $entity_type = \Drupal::entityTypeManager()->getDefinition($config['entity_type_id']);
    $info['bundle_label'] = !empty($bundle_info[$config['entity_type_id']][$config['bundle']])
      ? $bundle_info[$config['entity_type_id']][$config['bundle']]['label']
      : $config['bundle'];
    $info['entity_type_label'] = $entity_type->getLabel();
    if (!empty($config['field'])) {
      $field = FieldConfig::loadByName($config['entity_type_id'], $config['bundle'], $config['field']);
      $info['field_label'] = $field->getLabel();
      $info['field'] = $config['field'];
    }
    return $info;
  }

  /**
   * Get available processors.
   */
  protected function getProcessor($id) {
    if ($processor = \Drupal::entityTypeManager()->getStorage('document_ocr_processor')->load($id)) {
      return $processor;
    }
  }

  /**
   * Check if processor require template or there is an exception for certain processors.
   */
  protected function isTemplateRequired($document_ocr_processor) {
    $processor = $document_ocr_processor->getProcessorPlugin()->setCredentials($document_ocr_processor->getCredentials());
    if ($skip_template = $processor->skipTemplate($document_ocr_processor)) {
      if ($processor->requireTemplate()) {
        return FALSE;
      }
    }
    else {
      if ($processor->requireTemplate()) {
        return TRUE;
      }
    }
  }

}
