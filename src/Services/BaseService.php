<?php

namespace Drupal\document_ocr\Services;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * BaseService service.
 */
class BaseService {

  use StringTranslationTrait;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The cache.default cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

   /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Cache expiration in seconds.
   */
  protected $interval = 3600;

  /**
   * Client object.
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public function __construct(FileSystemInterface $file_system, CacheBackendInterface $cache, LoggerChannelFactoryInterface $logger_factory) {
    $this->fileSystem = $file_system;
    $this->cacheBackend = $cache;
    $this->loggerFactory = $logger_factory->get('document_ocr');
  }

  /**
   * Languages list.
   */
  public function getLanguagesList() {
    $languages = [];
    $repository = \Drupal::service('document_ocr.languages_repository')->toArray();
    foreach ($repository as $item) {
      $languages[$item['code']] = $item['language'];
    }
    return $languages;
  }

}
