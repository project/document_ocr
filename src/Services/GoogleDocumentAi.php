<?php

namespace Drupal\document_ocr\Services;

use Google\ApiCore\ApiException;
use Google\Cloud\DocumentAI\V1\DocumentProcessorServiceClient;
use Google\Cloud\DocumentAI\V1\RawDocument;
use Google\Cloud\DocumentAI\V1\ProcessorVersion\State;
use Google\Cloud\DocumentAI\V1\GcsDocument;
use Google\Cloud\DocumentAI\V1\GcsDocuments;
use Google\Cloud\DocumentAI\V1\BatchDocumentsInputConfig;
use Google\Cloud\DocumentAI\V1\DocumentOutputConfig;
use Google\Cloud\DocumentAI\V1\DocumentOutputConfig\GcsOutputConfig;
use Google\Cloud\DocumentAI\V1\Document;
use Google\Protobuf\FieldMask;
use Google\Cloud\Storage\StorageClient;

/**
 * Google Document AI service.
 */
class GoogleDocumentAi extends GoogleBase {

  /**
   * The processor parameters.
   *
   * @var array
   */
  protected $processor_params;

  /**
   * Get processor version name.
   */
  public function getVersionStateName($state) {
    return State::name($state);
  }

  /**
   * Initialize servie client.
   */
  public function serviceClient($location_id = 'us') {
    try {
      $params = [
        'credentials' => $this->credentials,
      ];
      if ($location_id != 'us') {
        $params['apiEndpoint'] = $location_id . '-documentai.googleapis.com';
      }
      $this->client = new DocumentProcessorServiceClient($params);
    }
    catch (ApiException $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
    return $this;
  }

  /**
   * Get a processor name.
   */
  public function getParams() {
    return $this->processor_params;
  }

  /**
   * Set a processor name.
   */
  public function setParams($processor_name) {
    $this->processor_params = $this->client->parseName($processor_name);
    return $this;
  }

  /**
   * Get processors.
   */
  public function getProcessors($location_id) {
    $processors = [];
    $cid = 'google_document_ai:processors:' . $this->getProjectId() . ':' . $location_id;
    if ($cache = $this->cacheBackend->get($cid)) {
      $processors = $cache->data;
    }
    else {
      $this->serviceClient($location_id);
      $formatted_parent = $this->client->locationName($this->getProjectId(), $location_id);
      try {
        $response = $this->client->listProcessors($formatted_parent);
        foreach ($response as $element) {
          $processors[$element->getName()] = $element;
        }
      }
      catch (ApiException $ex) {
        $this->loggerFactory->error($ex->getMessage());
      }
      $this->cacheBackend->set($cid, $processors, time() + $this->interval, [
        'google_document_ai:processors',
        'google_document_ai:processors:' . $location_id
      ]);
    }
    return $processors;
  }

  /**
   * Get processors versions.
   */
  public function getProcessorVersions() {
    $params = $this->getParams();
    $location_id = $params['location'];
    $processor_id = $params['processor'];
    $versions = [];
    $cid = 'google_document_ai:processor_version:' . $this->getProjectId() . ':' . $processor_id;
    if ($cache = $this->cacheBackend->get($cid)) {
      $versions = $cache->data;
    }
    else {
      $this->serviceClient($location_id);
      $formatted_parent = $this->client->processorName($params['project'], $location_id, $processor_id);
      try {
        $response = $this->client->listProcessorVersions($formatted_parent);
        foreach ($response as $element) {
          $versions[$element->getName()] = $element;
        }
      }
      catch (ApiException $ex) {
        $this->loggerFactory->error($ex->getMessage());
      }
      $this->cacheBackend->set($cid, $versions, time() + $this->interval, [
        'google_document_ai:processors',
        'google_document_ai:processor_version:' . $processor_id
      ]);
    }
    return $versions;
  }

  /**
   * Get locations.
   */
  public function getLocations() {
    $locations = [];
    $cid = 'google_document_ai:locations:' . $this->getProjectId();
    if ($cache = $this->cacheBackend->get($cid)) {
      $locations = $cache->data;
    }
    else {
      try {
        $response = $this->client->listLocations([
          'name' => 'projects/' . $this->getProjectId(),
        ]);
        foreach ($response as $element) {
          $locations[$element->getLocationId()] = $this->regionNames($element->getLocationId());
        }
        $this->cacheBackend->set($cid, $locations, time() + $this->interval, [
          'google_document_ai:locations',
        ]);
      }
      catch (ApiException $ex) {
        $this->loggerFactory->error($ex->getMessage());
      }
    }
    return $locations;
  }

  /**
   * Process document.
   */
  public function processDocument($file) {
    try {
      $params = $this->getParams();
      $this->serviceClient($params['location']);
      $document_path = $this->fileSystem->realpath($file->getFileUri());

      $handle = fopen($document_path, 'rb');
      $contents = fread($handle, filesize($document_path));
      fclose($handle);

      $rawDocument = new RawDocument([
        'content' => $contents,
        'mime_type' => $file->getMimeType(),
      ]);

      if (!empty($params['processor_version'])) {
        $name = $this->client->processorVersionName($this->getProjectId(), $params['location'], $params['processor'], $params['processor_version']);
      }
      else {
        $name = $this->client->processorName($this->getProjectId(), $params['location'], $params['processor']);
      }
      $response = $this->client->processDocument($name, [
        'rawDocument' => $rawDocument
      ]);
      return $response->getDocument();
    }
    catch (ApiException $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
  }

  /**
   * Process asynchronous documents.
   */
  public function processAsynchronousDocument($task, $file, $configuration) {
    try {
      $task_data = $task->getTaskData();
      $gcs_bucket_name = $configuration['project_id'] . '-document-ocr';
      $gcs_bucket_name_output = $gcs_bucket_name . '-output';
      $bucket_prefix = $file->uuid();
      $storage = new StorageClient(['keyFile' => $this->credentials]);

      // Checking if document was already sent for processing.
      if (empty($task_data['batch_operation_name'])) {
        $bucket = $this->createBucket($storage, $gcs_bucket_name);
        $document_path = $this->fileSystem->realpath($file->getFileUri());
        $bucket->upload(fopen($document_path, 'r'), [
          'name' => $bucket_prefix . '/' . basename($document_path),
        ]);
        $document = new GcsDocument();
        $document->setGcsUri('gs://' . $gcs_bucket_name . '/' . $bucket_prefix . '/' . basename($document_path));
        $document->setMimeType($file->getMimeType());
        $gcs_documents = new GcsDocuments();
        $gcs_documents->setDocuments([$document]);
        $inputConfig = new BatchDocumentsInputConfig([
          'gcs_documents' => $gcs_documents
        ]);

        $this->createBucket($storage, $gcs_bucket_name_output);
        $outputConfig = new DocumentOutputConfig([
          'gcs_output_config' => new GcsOutputConfig([
            'gcs_uri' => 'gs://' . $gcs_bucket_name_output . '/' . $bucket_prefix . '/',
            'field_mask' => (new FieldMask())->setPaths(['text', 'entities']),
          ])
        ]);

        $params = $this->getParams();
        $this->serviceClient($params['location']);
        if (!empty($params['processor_version'])) {
          $name = $this->client->processorVersionName($this->getProjectId(), $params['location'], $params['processor'], $params['processor_version']);
        }
        else {
          $name = $this->client->processorName($this->getProjectId(), $params['location'], $params['processor']);
        }
        $response = $this->client->batchProcessDocuments($name, [
          'inputDocuments' => $inputConfig,
          'documentOutputConfig' => $outputConfig,
          'skipHumanReview' => TRUE,
        ]);
        if ($batch_operation_name = $response->getName()) {
          $task->setTaskData([
            'batch_operation_name' => $batch_operation_name,
            'gcs_bucket_name_input' => $gcs_bucket_name,
            'gcs_object_name' => basename($document_path),
            'gcs_bucket_name_output' => $gcs_bucket_name_output,
            'bucket_prefix' => $bucket_prefix,
          ]);
        }
      }
      else {
        try {
          $e = explode('/', $task_data['batch_operation_name']);
          $bucket = $storage->bucket($gcs_bucket_name_output);
          $options = ['prefix' => $bucket_prefix . '/' . $e[count($e) - 1]];
          $objects = [];
          foreach ($bucket->objects($options) as $object) {
            $objects[] = $object;
          }
          // If no results were found, something went wrong.
          // Delete the google storage data and start again.
          if (empty($objects)) {
            $task->setTaskData([
              'batch_operation_name' => FALSE,
              'gcs_bucket_name_input' => FALSE,
              'gcs_object_name' => FALSE,
              'gcs_bucket_name_output' => FALSE,
              'bucket_prefix' => FALSE,
            ]);
            return FALSE;
          }

          // If there is only 1 file no need for concatination of partioned JSON files.
          if (count($objects) == 1) {
            if ($object = reset($objects)) {
              $document = new Document();
              $document->mergeFromJsonString($object->downloadAsString());
              return $document;
            }
          }
          else {
            // Concatinating partioned JSON files.
            $json_document = ['text' => ''];
            foreach ($objects as $object) {
              $json = json_decode($object->downloadAsString(), TRUE);
              if (!empty($json['text'])) {
                $json_document['text'] .= $json['text'];
              }
            }
          }
          return !empty($json_document['text']) ? new Document($json_document) : FALSE;
        }
        catch (\Exception $ex) {
          $this->loggerFactory->error($ex->getMessage());
        }
      }
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
  }

  /**
   * Perform action when async processing is complete.
   */
  public function asynchronousComplete($task, $configuration) {
    $task_data = $task->getTaskData();
    $storage = new StorageClient(['keyFile' => $this->credentials]);
    // Deleting source file from the cloud.
    $bucket = $storage->bucket($task_data['gcs_bucket_name_input']);
    $options = ['prefix' => $task_data['bucket_prefix']];
    foreach ($bucket->objects($options) as $object) {
      $object->delete();
    }
    // Deleting processed output file from the cloud.
    $bucket = $storage->bucket($task_data['gcs_bucket_name_output']);
    $e = explode('/', $task_data['batch_operation_name']);
    $options = ['prefix' => $task_data['bucket_prefix'] . '/' . $e[count($e) - 1]];
    foreach ($bucket->objects($options) as $object) {
      $object->delete();
    }
  }

  /**
   * Get bucket name and create it if it does not exists.
   */
  protected function createBucket($storage, $gcs_bucket_name) {
    $bucket = $storage->bucket($gcs_bucket_name, TRUE);
    if (!$bucket->exists()) {
      $storage->createBucket($gcs_bucket_name);
      $bucket = $storage->bucket($gcs_bucket_name, TRUE);
    }
    return $bucket;
  }

  /**
   * Region names.
   */
  protected function regionNames($location_id) {
    $regions = [];
    $repository = \Drupal::service('document_ocr.documentai_regions_repository')->toArray();
    foreach ($repository as $item) {
      $regions[$item['region']] = $item['label'];
    }
    return !empty($regions[$location_id]) ? $regions[$location_id] : strtoupper($location_id);
  }

}
