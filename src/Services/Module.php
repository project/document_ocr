<?php

namespace Drupal\document_ocr\Services;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\document_ocr\Repository\ThemeRepository;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Document OCR module.
 */
class Module {

  /**
   * Viewer Source service.
   *
   * @var \Drupal\document_ocr\Services\Process
   */
  protected $documentProcess;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * The theme repository.
   *
   * @var \Drupal\document_ocr\Repository\ThemeRepository
   */
  protected $themeRepository;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(Process $document_process, RouteMatchInterface $current_route_match, ThemeRepository $theme_repository, LoggerChannelFactoryInterface $logger_factory) {
    $this->documentProcess = $document_process;
    $this->currentRouteMatch = $current_route_match;
    $this->themeRepository = $theme_repository;
    $this->loggerFactory = $logger_factory->get('document_ocr');
  }

  /**
   * Implements hook_theme().
   */
  public function theme() {
    return $this->themeRepository->toArray();
  }

  /**
   * Implements template_preprocess_document_ocr_progressbar().
   */
  public function preprocess_progressbar(&$variables) {
    $current_route_name = $this->currentRouteMatch->getRouteName();
    $variables['current_route_name'] = $current_route_name;
    $is_visible = TRUE;
    $index = 0;
    foreach ($variables['steps'] as $route => $title) {
      if ($current_route_name == $route && $index == 0) {
        $is_visible = FALSE;
      }
      $index++;
    }
    $variables['is_visible'] = $is_visible;
  }

  /**
   * Implements hook_entity_insert().
   */
  public function entity_insert(EntityInterface $entity) {
    try {
      $this->documentProcess->processEntity($entity);
    }
    catch (\Throwable $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
  }

  /**
   * Implements hook_entity_update().
   */
  public function entity_update(EntityInterface $entity) {
    try {
      $this->documentProcess->processEntity($entity);
    }
    catch (\Throwable $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
  }

  /**
   * Implements hook_entity_delete().
   */
  public function entity_delete(EntityInterface $entity) {
    try {
      $this->documentProcess->deleteEntity($entity);
    }
    catch (\Throwable $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
  }

  /**
   * Implements hook_cron().
   */
  public function cron() {
    try {
      $this->documentProcess->processPendingOnCron();
    }
    catch (\Throwable $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
  }

}
