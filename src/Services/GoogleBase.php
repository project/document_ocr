<?php

namespace Drupal\document_ocr\Services;

/**
 * GoogleBase service.
 */
class GoogleBase extends BaseService {

  /**
   * The Google project name.
   *
   * @var string
   */
  protected $project_id;

  /**
   * The Google Service credentials.
   *
   * @var array
   */
  protected $credentials;

  /**
   * Set credentials from JSON file.
   */
  public function setCredentials($credentials) {
    $this->credentials = $credentials;
    if (!empty($this->credentials['project_id'])) {
      $this->setProjectId($this->credentials['project_id']);
    }
    return $this;
  }

  /**
   * Get a project ID.
   */
  public function getProjectId() {
    return $this->project_id;
  }

  /**
   * Set a project ID.
   */
  public function setProjectId($id) {
    $this->project_id = $id;
    return $this;
  }

}
