<?php

namespace Drupal\document_ocr\Services;

/**
 * MicrosoftTranslate service.
 */
class MicrosoftTranslate extends BaseService {

  /**
   * The translator service version.
   *
   * @var string
   */
  protected $version = '3.0';

  /**
   * The Microsoft Translate credentials.
   *
   * @var array
   */
  protected $credentials = [];

  /**
   * Set credentials from JSON file.
   */
  public function setCredentials($credentials) {
    $this->credentials = $credentials;
    return $this;
  }

  /**
   * Get supported languages.
   */
  public function getSupportedLanguages() {
    $languages = [];
    $cid = 'microsoft_translate:languages:' . $this->version;
    if ($cache = $this->cacheBackend->get($cid)) {
      $languages = $cache->data;
    }
    else {
      try {
        $headers = "Content-type: text/xml\r\n";
        $options = [
          'http' => [
            'header' => $headers,
            'method' => 'GET'
          ]
        ];
        $languages = $this->request($this->buildUrl('languages'), $options);
      }
      catch (\Exception $ex) {
        $this->loggerFactory->error($ex->getMessage());
      }
      $this->cacheBackend->set($cid, $languages, time() + $this->interval, [
        'microsoft_translate:languages',
        'microsoft_translate:languages:' . $this->version
      ]);
    }
    return $languages;
  }

  /**
   * Translate text.
   */
  public function translate($text, $language_from, $language_to) {
    try {
      $payload = [
        ['Text' => $text],
      ];
      $content = json_encode($payload);
      $headers = "Content-type: application/json\r\n" .
        "Content-length: " . strlen($content) . "\r\n";
      if (!empty($this->credentials['Ocp-Apim-Subscription-Key'])) {
        $headers .="Ocp-Apim-Subscription-Key: {$this->credentials['Ocp-Apim-Subscription-Key']}\r\n";
      }
      if (!empty($this->credentials['Ocp-Apim-Subscription-Region'])) {
        $headers .="Ocp-Apim-Subscription-Region: {$this->credentials['Ocp-Apim-Subscription-Region']}\r\n";
      }
      $options = [
        'http' => [
          'header' => $headers,
          'method' => 'POST',
          'content' => $content
        ]
      ];
      $params = ['to' => $language];
      if (!empty($language_from)) {
        $params['from'] = $language_from;
      }
      $response = $this->request($this->buildUrl('translate', $params), $options);
      return $response;
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
  }

  /**
   * Build API request URL.
   */
  protected function buildUrl($path, $params = []) {
    $url = $this->credentials['host'] . $path . '/?api-version=' . $this->version;
    if (!empty($params)) {
      foreach ($params as $key => $value) {
        $url .= '&' . $key . '=' . $value;
      }
    }
    return $url;
  }

  /**
   * Format response.
   */
  protected function request($url, $options) {
    try {
      $context = stream_context_create($options);
      $response = file_get_contents($url, FALSE, $context);
      // Note: We convert result, which is JSON, to and from an object so we can pretty-print it.
      // We want to avoid escaping any Unicode characters that result contains. See:
      // http://php.net/manual/en/function.json-encode.php
      return json_decode($response, TRUE);
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
  }

}
