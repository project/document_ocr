<?php

namespace Drupal\document_ocr\Services;

/**
 * Document OCR Batch API.
 *
 * @ingroup document_ocr
 */
class Batch {

  /**
   * Document processing.
   */
  public static function process($item, &$context) {
    if (empty($context['results']['error'])) {
      $context['results']['error'] = [];
    }
    if (empty($context['results']['documents'])) {
      $context['results']['documents'] = [];
    }
    try {
      if ($task = \Drupal::entityTypeManager()->getStorage('document_ocr_task')->load($item->id)) {
        if ($document = $task->getDocumentOcr()) {

          $document_ocr_processor = $task->getProcessorReference();
          $processor = $document_ocr_processor->getProcessorPlugin();
          $configuration = $document_ocr_processor->getConfiguration();
          $settings = $document->getSettings();

          $attempts_limit = ($processor->isAsynchronous() && !empty($configuration['asynchronous']))
            ? $configuration['asynchronous_attempts']
            : $settings['attempts'];

          if ($task->getAttempts() < $attempts_limit && ($task->isPending() || $task->isInitiated())) {
            if ($destination = \Drupal::service('document_ocr.process')->runPendingTask($task)) {
              $context['results']['documents'][] = $item->id;
              $context['message'] = t('Processing %name.', ['%name' => $destination->label()]);
            }
            else {
              $error_message = t('Unable to process %name at this time. Please wait before you try processing again.', ['%name' => $document->label()]);
              if (!in_array($error_message, $context['results']['error']) && empty($context['results']['error'])) {
                $context['message'] = $error_message;
                $context['results']['error'][] = $error_message;
              }
            }
          }
          else {
            $task->setFailed()->save();
            $error_message = t('Attempt limit reached or document marked as processed for %name.', ['%name' => $task->getFile()->getFilename()]);
            if (!in_array($error_message, $context['results']['error']) && empty($context['results']['error'])) {
              $context['message'] = $error_message;
              $context['results']['error'][] = $error_message;
            }
          }
        }
        else {
          if ($task->isPending() || $task->isInitiated()) {
            if ($destination = \Drupal::service('document_ocr.process')->runPendingTask($task)) {
              $context['results']['documents'][] = $item->id;
              $context['message'] = t('Processing %name.', ['%name' => $destination->label()]);
            }
            else {
              $error_message = t('Unable to process %name at this time. Please wait before you try processing again.', [
                '%name' => $document?->label() ?? 'the document'
              ]);
              if (!in_array($error_message, $context['results']['error']) && empty($context['results']['error'])) {
                $context['message'] = $error_message;
                $context['results']['error'][] = $error_message;
              }
            }
          }
        }
      }
    }
    catch (\Exception $e) {
      $context['message'] = $e->getMessage();
      $context['results']['error'][] = $e->getMessage();
    }
  }

  /**
   * One-time files processing.
   */
  public static function processOneTime($item, &$context) {
    try {
      $result = \Drupal::service('document_ocr.process')->runOneTimeTask($item);
      if (!empty($result['async'])) {
        $context['results']['initiated'][] = $result['task']->id();
        $context['message'] = t('Initiating process for %name.', ['%name' => $item->file->getFilename()]);
      }
      else {
        $context['results']['documents'][] = $result['task']->id();
        $context['message'] = t('Processing %name.', ['%name' => $item->file->getFilename()]);
      }
    }
    catch (\Exception $e) {
      $context['message'] = $e->getMessage();
      $context['results']['error'][] = $e->getMessage();
    }
  }

  /**
   * Process older entities.
   */
  public static function processOlderEntity($entity, &$context) {
    try {
      $context['results']['documents'][] = $entity->id();
      $context['message'] = t('Processing %label.', ['%label' => $entity->label()]);
      \Drupal::service('document_ocr.process')->processEntity($entity);
    }
    catch (\Exception $e) {
      $context['message'] = $e->getMessage();
      $context['results']['error'][] = $e->getMessage();
    }
  }

  /**
   * Complete callback for file process.
   */
  public static function completeProcessCallback($success, $results, $operations) {
    if (!empty($results['error'])) {
      foreach ($results['error'] as $error_message) {
        self::complete($error_message, $error_message, FALSE, $results);
      }
    }
    else {
      if (!empty($results['documents'])) {
        self::complete('1 document processed.', '@count documents processed.', $success, $results);
      }
      else if (!empty($results['initiated'])) {
        self::complete('1 document process initiated.', '@count document process initiated.', $success, $results);
      }
      else {
        self::complete('1 document processed.', '@count documents processed.', $success, []);
      }
    }
  }

  /**
   * Helper method for complete callbacks.
   */
  protected static function complete($singular, $plural, $success, $results) {
    if ($success) {
      if (!empty($results['initiated'])) {
        $message = \Drupal::translation()->formatPlural(!empty($results['initiated']), $singular, $plural);
      }
      else {
        $message = \Drupal::translation()->formatPlural((!empty($results['documents']) ? count($results['documents']) : 0), $singular, $plural);
      }
      \Drupal::messenger()->addMessage($message, 'status', FALSE);
      \Drupal::logger('document_ocr')->notice($message);
    }
    else {
      \Drupal::messenger()->addWarning($singular, 'warning', FALSE);
      \Drupal::logger('document_ocr')->warning($singular);
    }
  }

}
