<?php

namespace Drupal\document_ocr\Services;

use Google\ApiCore\ApiException;
use Google\Cloud\Translate\V3\TranslationServiceClient;

/**
 * GoogleTranslate service.
 */
class GoogleTranslate extends GoogleBase {

  /**
   * The translator service location.
   *
   * @var string
   */
  protected $location = 'global';

  /**
   * Initialize servie client.
   */
  public function serviceClient() {
    try {
      $this->client = new TranslationServiceClient([
        'credentials' => $this->credentials,
      ]);
    }
    catch (ApiException $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
    return $this;
  }

  /**
   * Get supported languages.
   */
  public function getSupportedLanguages() {
    $languages = [];
    $cid = 'google_translate:languages:' . $this->getProjectId() . ':' . $this->location;
    if ($cache = $this->cacheBackend->get($cid)) {
      $languages = $cache->data;
    }
    else {
      try {
        $languages_list = $this->getLanguagesList();
        $this->serviceClient();
        $formatted_parent = TranslationServiceClient::locationName($this->getProjectId(), $this->location);
        $response = $this->client->getSupportedLanguages($formatted_parent);
        foreach ($response->getLanguages() as $language) {
          $langcode = $language->getLanguageCode();
          $languages[$langcode] = !empty($languages_list[$langcode])
            ? $this->t('@lang (@code)', ['@lang' => $languages_list[$langcode], '@code' => $langcode])
            : $langcode;
        }
      }
      catch (ApiException $ex) {
        $this->loggerFactory->error($ex->getMessage());
      }
      $this->cacheBackend->set($cid, $languages, time() + $this->interval, [
        'google_translate:languages',
        'google_translate:languages:' . $this->location
      ]);
    }
    return $languages;
  }

  /**
   * Translate text.
   */
  public function translate($text, $language_from, $language_to) {
    try {
      $this->serviceClient();
      $params = [];
      $formatted_parent = TranslationServiceClient::locationName($this->getProjectId(), $this->location);
      if (!empty($language_from)) {
        $params['sourceLanguageCode'] = $language_from;
      }
      $response = $this->client->translateText([$text], $language_to, $formatted_parent, $params);
      return $response;
    }
    catch (ApiException $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
  }

}
