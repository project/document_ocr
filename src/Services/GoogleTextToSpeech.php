<?php

namespace Drupal\document_ocr\Services;

use Google\ApiCore\ApiException;
use Google\Cloud\TextToSpeech\V1\AudioConfig;
use Google\Cloud\TextToSpeech\V1\AudioEncoding;
use Google\Cloud\TextToSpeech\V1\SynthesisInput;
use Google\Cloud\TextToSpeech\V1\TextToSpeechClient;
use Google\Cloud\TextToSpeech\V1\VoiceSelectionParams;
use Google\Cloud\TextToSpeech\V1\SsmlVoiceGender;

/**
 * GoogleTextToSpeech service.
 */
class GoogleTextToSpeech extends GoogleBase {

  /**
   * Initialize servie client.
   */
  public function serviceClient() {
    try {
      $this->client = new TextToSpeechClient([
        'credentials' => $this->credentials,
      ]);
    }
    catch (ApiException $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
    return $this;
  }

  /**
   * Get voices.
   */
  public function getVoices() {
    $languages = [];
    $cid = 'google_text2speech:languages:' . $this->getProjectId();
    if ($cache = $this->cacheBackend->get($cid)) {
      $languages = $cache->data;
    }
    else {
      try {
        $language_repository = $this->getLanguagesList();
        $this->serviceClient();
        $response = $this->client->listVoices();
        foreach ($response->getVoices() as $voice) {
          foreach ($voice->getLanguageCodes() as $langcode) {
            $languages[$langcode]['name'] = !empty($language_repository[$langcode]) ? $language_repository[$langcode] : $langcode;
            $languages[$langcode]['voices'][$this->getVoiceType($langcode, $voice->getName())][] = [
              'name' => $voice->getName(),
              'ssml_gender_code' => $voice->getSsmlGender(),
              'ssml_gender' => SsmlVoiceGender::name($voice->getSsmlGender()),
            ];
          }
        }
        // Alphabetical language sorting.
        $key_values = array_column($languages, 'name'); 
        array_multisort($key_values, SORT_ASC, $languages);
      }
      catch (ApiException $ex) {
        $this->loggerFactory->error($ex->getMessage());
      }
      $this->cacheBackend->set($cid, $languages, time() + $this->interval, [
        'google_text2speech:languages',
      ]);
    }
    return $languages;
  }

  /**
   * Convert text to speech.
   */
  public function convert($text, $configuration) {
    try {
      $this->serviceClient();
      $input = new SynthesisInput();
      $input->setText($text);
      $voice = new VoiceSelectionParams();
      $voice->setLanguageCode($configuration['language']);
      $audio_config = new AudioConfig();
      $audio_config->setSpeakingRate($configuration['speed']);
      $audio_config->setPitch($configuration['pitch']);
      $audio_config->setAudioEncoding(AudioEncoding::MP3);
      $response = $this->client->synthesizeSpeech($input, $voice, $audio_config);
      return $response;
    }
    catch (ApiException $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
  }

  /**
   * Get voice type from name.
   */
  protected function getVoiceType($langcode, $name) {
    $name = str_replace($langcode . '-', '', $name);
    $e = explode('-', $name);
    return !empty($e[0]) ? $e[0] : 'Standard';
  }

}
