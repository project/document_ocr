<?php

namespace Drupal\document_ocr\Services;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\document_ocr\Event\TaskDestinationBeforeSave;
use Drupal\document_ocr\Event\TaskDestinationAfterSave;
use Drupal\document_ocr\Plugin\ProcessorInterface;
use Drupal\document_ocr\Plugin\ProcessorManager;
use Drupal\document_ocr\Plugin\TransformerManager;
use Drupal\document_ocr\DocumentOcrInterface;
use Drupal\file\FileInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Document OCR process service.
 */
class Process {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Processor manager.
   *
   * @var \Drupal\document_ocr\Plugin\ProcessorManager
   */
  protected $processorManager;

  /**
   * Processor manager.
   *
   * @var \Drupal\document_ocr\Plugin\TransformerManager
   */
  protected $transformerManager;

  /**
   * Storage for the document_ocr_task entity.
   */
  protected $taskStorage;

  /**
   * Storage for the document_ocr_data entity.
   */
  protected $dataAsJson;

  /**
   * Event dispatcher.
   */
  private EventDispatcherInterface $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManager $entity_field_manager,
    ProcessorManager $processor,
    TransformerManager $transformer,
    EventDispatcherInterface $event_dispatcher
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->processorManager = $processor;
    $this->transformerManager = $transformer;
    $this->taskStorage = $entity_type_manager->getStorage('document_ocr_task');
    $this->dataAsJson = $entity_type_manager->getStorage('document_ocr_data');
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Process entity file.
   */
  public function processEntity(EntityInterface $entity) {
    $documents = $this->matchDocumentMapping($entity);
    foreach ($documents as $document) {
      $field = $document->getFieldSelector()['field'];
      $settings = $document->getSettings();
      // Process only active mapping.
      if (!$document->isActive()) {
        continue;
      }
      if (!$entity->isNew() && empty($settings['process_update'])) {
        continue;
      }
      if ($document_ocr_processor = $document->getProcessorReference()) {
        $configuration = $document_ocr_processor->getConfiguration();
        $processor = $document_ocr_processor->getProcessorPlugin();
        if (!$entity->get($field)->isEmpty()) {
          foreach ($entity->get($field)->referencedEntities() as $file) {
            // Only processing supported file extensions.
            if ($this->isExtensionSupported($file, $processor->getExtensions())) {
              $run_on_cron = TRUE;
              $async_init = FALSE;
              $task = $this->taskReferenceEntity($document, $entity, $file);
              $task->attachFile($file)
                ->attachField($field);
              $is_asynchronous = ($processor->isAsynchronous() && !empty($configuration['asynchronous']));
              if ($is_asynchronous) {
                if (!empty($configuration['asynchronous_initiate'])) {
                  $this->processAsyncDocument($task, $document, $file);
                }
                $async_init = TRUE;
              }
              else {
                if (!empty($settings['realtime_processing'])) {
                  if ($destination = $this->processDocument($document, $file)) {
                    $task->attachDestination($destination)
                      ->setProcessed();
                    $run_on_cron = FALSE;
                  }
                  else {
                    $task->attempt()
                      ->setPending();
                  }
                }
              }
              if ($run_on_cron && !$async_init) {
                $task->setPending();
              }
              if ($async_init) {
                $task->setInitiated();
              }
              $task->save();
            }
          }
        }
      }
    }
  }

  /**
   * Process entity file.
   */
  public function deleteEntity(EntityInterface $entity) {
    if ($entity->getEntityTypeId() == 'file') {
      $tasks = $this->taskStorage->loadByProperties(['file' => $entity->id()]);
      foreach ($tasks as $task) {
        // Deleting destination entity. @see Document OCR settings page.
        $document = $task->getDocumentOcr();
        $settings = $document->getSettings();
        if (!empty($settings['delete_destination'])) {
          $entities = $this->entityTypeManager->getStorage($task->get('entity_type_id')->value)->loadByProperties([
            'file' => $entity->id(),
          ]);
          if ($destination = reset($entities)) {
            $destination->delete();
          }
        }
        // Deleting prcoess task.
        $task->delete();
      }
    }
    // Delete multiple import task references when source node is deleted.
    $tasks = $this->taskStorage->loadByProperties([
      'source_entity_type_id' => $entity->getEntityTypeId(),
      'source_bundle' => $entity->bundle(),
      'source_entity_id' => $entity->id(),
    ]);
    foreach ($tasks as $task) {
      $task->delete();
    }
    // Delete multiple import task references when imported node is deleted.
    $tasks = $this->taskStorage->loadByProperties([
      'destination_entity_type_id' => $entity->getEntityTypeId(),
      'destination_bundle' => $entity->bundle(),
      'destination_entity_id' => $entity->id(),
    ]);
    foreach ($tasks as $task) {
      $task->delete();
    }
    // Delete processed JSON data.
    $data_json = $this->dataAsJson->loadByProperties([
      'entity_type_id' => $entity->getEntityTypeId(),
      'bundle' => $entity->bundle(),
      'entity_id' => $entity->id(),
    ]);
    foreach ($data_json as $data) {
      $data->delete();
    }
    // If processor deleted then delete all tasks associated with the processor.
    if ($entity->getEntityTypeId() == 'document_ocr_processor') {
      $tasks = $this->taskStorage->loadByProperties([
        'processor' => $entity->id(),
      ]);
      foreach ($tasks as $task) {
        $task->delete();
      }
    }
    // If mapping deleted then delete all tasks associated with the mapping.
    if ($entity->getEntityTypeId() == 'document_ocr_mapping') {
      $tasks = $this->taskStorage->loadByProperties([
        'document_ocr_mapping' => $entity->id(),
      ]);
      foreach ($tasks as $task) {
        $task->delete();
      }
    }
  }

  /**
   * Run pending import tasks until attempt limit is reached.
   */
  public function runPendingTask($task) {
    $document = $task->getDocumentOcr();
    $document_ocr_processor = $task->getProcessorReference();
    $configuration = $document_ocr_processor->getConfiguration();
    $processor = $document_ocr_processor->getProcessorPlugin();
    if (!empty($document) && !$document->isActive()) {
      return;
    }
    if ($processor->isAsynchronous() && !empty($configuration['asynchronous'])) {
      if ($task->getAttempts() >= $configuration['asynchronous_attempts']) {
        $task->setFailed()->save();
        return;
      }
    }
    if ($file = $task->getFile()) {
      if (!empty($document)) {
        if ($processor->isAsynchronous() && !empty($configuration['asynchronous'])) {
          $destination = $this->processAsyncDocument($task, $document, $file);
        }
        else {
          $destination = $this->processDocument($document, $file);
        }
      }
      else {
        // One-time pending import task.
        if ($processor->isAsynchronous() && !empty($configuration['asynchronous'])) {
          $destination = $this->processAsyncOneTimeDocument($task, $file);
        }
        else {
          $destination = $this->processOneTimeDocument($task, $file);
        }
      }
      if ($destination) {
        $task->attachDestination($destination)
          ->setProcessed()
          //->resetAttempts()
          ->save();
        return $destination;
      }
      else {
        $task->attempt()
          ->setPending();
      }
      $task->save();
    }
  }

  /**
   * Process older entity file.
   */
  public function prcoessOlderEntity($document, $entity) {
    if (!$document->isActive()) {
      return;
    }
    $field = $document->getFieldSelector()['field'];
    $settings = $document->getSettings();
    if (!$entity->get($field)->isEmpty()) {
      foreach ($entity->get($field)->referencedEntities() as $file) {
        $task = $this->taskReferenceEntity($document, $entity, $file);
        $task->attachFile($file)
          ->attachField($field)
          ->setPending()
          ->save();
      }
      return $entity;
    }
  }

  /**
   * Process all pending on cron.
   */
  public function processPendingOnCron() {
    $query = $this->taskStorage->getQuery()
      ->condition('status', [DocumentOcrInterface::TASK_STATUS_INIT, DocumentOcrInterface::TASK_STATUS_PENDING], 'IN')
      ->accessCheck(FALSE);
    $task_ids = $query->execute();
    foreach ($this->taskStorage->loadMultiple($task_ids) as $task) {
      $this->runPendingTask($task);
    }
  }

  /**
   * Process document, build destination entity using document properties and save it.
   */
  public function processDocument($document, $file) {
    $document_ocr_processor = $document->getProcessorReference();
    $processor = $document_ocr_processor->getProcessorPlugin();
    $processor->setCredentials($document_ocr_processor->getCredentials())
      ->setConfiguration($document_ocr_processor->getConfiguration())
      ->setFile($file);
    $destination = $document->getDestinationBundle();
    if ($mapping_data = $processor->getMappingData()) {
      $tasks = $this->taskStorage->loadByProperties([
        'document_ocr_mapping' => $document->id(),
        'destination_entity_type_id' => $destination['entity_type_id'],
        'destination_bundle' => $destination['bundle'],
        'file' => $file->id(),
      ]);
      $storage = $this->entityTypeManager->getStorage($destination['entity_type_id']);
      if ($task = reset($tasks)) {
        $entity = $storage->load($task->get('destination_entity_id')->value);
      }
      else {
        $entity = $storage->create([
          'bundle' => $destination['bundle'],
          'type' => $destination['bundle'],
        ]);
      }
      return $this->saveEntity($entity, $document->getMapping(), $mapping_data, $file);
    }
  }

  /**
   * Process One-Time import task.
   */
  public function processOneTimeDocument($task, $file) {
    $task_data = $task->getTaskData();
    $document_ocr_processor = $task->getProcessorReference();
    $processor = $document_ocr_processor->getProcessorPlugin();
    $processor->setCredentials($document_ocr_processor->getCredentials())
      ->setConfiguration($document_ocr_processor->getConfiguration())
      ->setFile($file);
    if ($mapping_data = $processor->getMappingData()) {
      list ($entity_type_id, $bundle) = explode('::', $task_data['destination_bundle']);
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->create([
        'bundle' => $bundle,
        'type' => $bundle,
      ]);
      return $this->saveEntity($entity, $task_data['mapping'], $mapping_data, $file, $task_data['field']);
    }
  }

  /**
   * Process document, build destination entity using document properties and save it (async).
   */
  public function processAsyncDocument($task, $document, $file) {
    $document_ocr_processor = $document->getProcessorReference();
    $processor = $document_ocr_processor->getProcessorPlugin();
    $processor->setCredentials($document_ocr_processor->getCredentials())
      ->setConfiguration($document_ocr_processor->getConfiguration())
      ->setFile($file);
    $destination = $document->getDestinationBundle();
    if ($mapping_data = $processor->getAsyncMappingData($task)) {
      $processor->asyncComplete($task);
      $tasks = $this->taskStorage->loadByProperties([
        'document_ocr_mapping' => $document->id(),
        'destination_entity_type_id' => $destination['entity_type_id'],
        'destination_bundle' => $destination['bundle'],
        'file' => $file->id(),
      ]);
      $storage = $this->entityTypeManager->getStorage($destination['entity_type_id']);
      if ($task = reset($tasks)) {
        $entity = $storage->load($task->get('destination_entity_id')->value);
      }
      else {
        $entity = $storage->create([
          'bundle' => $destination['bundle'],
          'type' => $destination['bundle'],
        ]);
      }
      return $this->saveEntity($entity, $document->getMapping(), $mapping_data, $file);
    }
  }

  /**
   * Process One-Time import task (async).
   */
  public function processAsyncOneTimeDocument($task, $file) {
    $task_data = $task->getTaskData();
    $document_ocr_processor = $task->getProcessorReference();
    $processor = $document_ocr_processor->getProcessorPlugin();
    $processor->setCredentials($document_ocr_processor->getCredentials())
      ->setConfiguration($document_ocr_processor->getConfiguration())
      ->setFile($file);
    if ($mapping_data = $processor->getAsyncMappingData($task)) {
      list ($entity_type_id, $bundle) = explode('::', $task_data['destination_bundle']);
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->create([
        'bundle' => $bundle,
        'type' => $bundle,
      ]);
      return $this->saveEntity($entity, $task_data['mapping'], $mapping_data, $file, $task_data['field']);
    }
  }

  /**
   * Run one-time import task.
   */
  public function runOneTimeTask($item) {
    $processor = $item->processor->getProcessorPlugin();
    $configuration = $item->processor->getConfiguration();
    $file = $item->file;
    // Only processing supported file extensions.
    if ($this->isExtensionSupported($file, $processor->getExtensions())) {
      $run_on_cron = TRUE;
      $async_init = FALSE;
      $task = $this->taskReferenceEntity($item->processor, NULL, $file);
      $task->setTaskData([
        'mapping' => $item->mapping,
        'destination_bundle' => $item->destination_bundle,
        'field' => $item->field,
      ]);
      $task->setFileDetails([
        'filename' => $file->getFilename(),
        'mime_type' => $file->getMimeType(),
        'size' => $file->getSize()
      ]);
      $task->attachFile($file)
        ->attachField($item->field);
      $is_asynchronous = ($processor->isAsynchronous() && !empty($configuration['asynchronous']));
      if (!$is_asynchronous) {
        if ($destination = $this->processOneTimeDocument($task, $file)) {
          $task->attachDestination($destination)
            ->setProcessed();
          $run_on_cron = FALSE;
        }
        else {
          $task->attempt()->setPending();
        }
      }
      else {
        if ($destination = $this->processAsyncOneTimeDocument($task, $file)) {
          $task->attachDestination($destination)
            ->setProcessed();
          $run_on_cron = FALSE;
        }
        else {
          $task->setInitiated();
          $async_init = TRUE;
        }
      }
      if ($run_on_cron && !$async_init) {
        $task->attempt()->setPending();
      }
      $task->save();
    }
    return [
      'task' => $task,
      'destination' => $destination,
      'async' => $async_init,
    ];
  }

  /**
   * Map fields and document properties.
   */
  protected function saveEntity(ContentEntityInterface $entity, array $mapping, ProcessorInterface $mapping_data, FileInterface $file, string $field = NULL) {
    $event = new TaskDestinationBeforeSave($file, $entity, $mapping_data);
    $event = $this->eventDispatcher->dispatch($event, TaskDestinationBeforeSave::EVENT);
    $entity = $event->getEntity();

    if (!$entity) {
      return NULL;
    }

    foreach ($mapping as $field_name => $map) {
      if (!empty($map['property']) && !empty($mapping_data->getValues()[$map['property']])) {
        $field_value = $mapping_data->getValues()[$map['property']];
        if ($document_ocr_transformer = $this->entityTypeManager->getStorage('document_ocr_transformer')->load($map['transformer'])) {
          $transformer = $this->transformerManager->createInstance($document_ocr_transformer->getTransformer());
          $transformer->setCredentials($document_ocr_transformer->getCredentials())->setMappingData($mapping_data);
          $transformer->setConfiguration($document_ocr_transformer->getConfiguration());
          $field_value = $transformer->transform($field_value);
        }
        $entity->set($field_name, $field_value);
      }
      else {
        if ($map['transformer'] == 'custom_value') {
          $custom_value = $map['custom_value'];
          foreach ($mapping_data->getValues() as $property_name => $property_value) {
            $custom_value = str_replace('%' . $property_name . '%', $property_value, $custom_value);
          }
          $entity->set($field_name, $custom_value);
        }
      }
    }
    // If title is not empty set it to the filename. The title field is required and cannot be empty.
    if ($entity->hasField('title')) {
      if (empty($entity->get('title')->value)) {
        $entity->set('title', $file->getFilename());
      }
    }
    else if ($entity->hasField('label')) {
      if (empty($entity->get('label')->value)) {
        $entity->set('label', $file->getFilename());
      }
    }
    if (!empty($field)) {
      $entity->set($field, $file->id());
    }
    $entity->save();

    $event = new TaskDestinationAfterSave($file, $entity, $mapping_data);
    $event = $this->eventDispatcher->dispatch($event, TaskDestinationAfterSave::EVENT);
    $entity = $event->getEntity();

    return $entity;
  }

  /**
   * Get entity mapping.
   */
  protected function matchDocumentMapping($entity) {
    $storage = $this->entityTypeManager->getStorage('document_ocr_mapping');
    $ids = $storage->getQuery()
      ->condition('field_selector.entity_type_id', $entity->getEntityTypeId())
      ->condition('field_selector.bundle', $entity->bundle())
      ->accessCheck(FALSE)
      ->execute();
    return $storage->loadMultiple($ids);
  }

  /**
   * Save entity mapping reference.
   */
  protected function taskReferenceEntity($document, $entity, $file) {
    if (!empty($entity)) {
      $existing = $this->taskStorage->loadByProperties([
        'document_ocr_mapping' => $document->id(),
        'source_entity_type_id' => $entity->getEntityTypeId(),
        'source_bundle' => $entity->bundle(),
        'source_entity_id' => $entity->id(),
        'source_entity_revision_id' => $entity->getRevisionId(),
        'file' => $file->id(),
      ]);
      if (!$task = reset($existing)) {
        $task = $this->taskStorage->create([]);
        $task->setDocumentOcr($document->id());
        $task->setProcessor($document->getProcessorReference()->id());
        $task->attachSource($entity);
      }
    }
    else {
      $task = $this->taskStorage->create([]);
      // When entity is not specified the $document in a processor entity.
      $task->setProcessor($document->id());
      $task->set('source_entity_id', NULL);
    }
    $task->setLastAttemptTime();
    return $task;
  }

  /**
   * Check if file extension supported.
   */
  protected function isExtensionSupported($file, $extensions) {
    $regex = '/\.(' . preg_replace('/ +/', '|', preg_quote($extensions)) . ')$/i';
    $subject = $file->isTemporary() ? $file->getFilename() : $file->getFileUri();
    return preg_match($regex, $subject);
  }

}
