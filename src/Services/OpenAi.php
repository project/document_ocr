<?php

namespace Drupal\document_ocr\Services;

/**
 * OpenAi service.
 */
class OpenAi extends BaseService {

  /**
   * Set credentials.
   */
  public function setCredentials($credentials) {
    try {
      if (!empty($credentials['baseUrl']) && !empty($credentials['apiKey']) && !empty($credentials['apiVersion'])) {
        $this->client = \OpenAI::factory()
          ->withBaseUri($credentials['baseUrl'])
          ->withHttpHeader('api-key', $credentials['apiKey'])
          ->withQueryParam('api-version', $credentials['apiVersion'])
          ->make();
      }
      else {
        $org = !empty($credentials['apiorg']) ? $credentials['apiorg'] : NULL;
        if (!empty($credentials['apikey'])) {
          $this->client = \OpenAI::client($credentials['apikey'], $org);
        }
        else {
          $this->loggerFactory->error('Missing or incorrect OpenAI API Key');
        }
      }
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
    return $this;
  }

  /**
   * Get models list.
   */
  public function getModels() {
    $list = [];
    $cid = 'openai:models:list';
    if ($cache = $this->cacheBackend->get($cid)) {
      $list = $cache->data;
    }
    else {
      try {
        $response = $this->client->models()->list();
        if (!empty($response->data)) {
          foreach ($response->data as $item) {
            $list[$item->id] = $item->id;
          }
        }
      }
      catch (\Exception $ex) {
        $this->loggerFactory->error($ex->getMessage());
      }
      ksort($list);
      $this->cacheBackend->set($cid, $list, time() + $this->interval, [$cid]);
    }
    return $list;
  }

  /**
   * Creates a completion request for the provided prompt and parameters.
   */
  public function chatRequest($configuration) {
    try {
      $response = $this->client->chat()->create($configuration);
      return $response;
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
  }

  /**
   * OpenAI audio operations.
   */
  public function audio($filepath, $configuration) {
    try {
      $params = [
        'model' => $configuration['model'],
        'file' => fopen($filepath, 'r'),
        'response_format' => 'verbose_json',
      ];
      if (!empty($configuration['temperature'])) {
        $params['temperature'] = $configuration['temperature'];
      }
      if (!empty($configuration['prompt'])) {
        $params['prompt'] = $configuration['prompt'];
      }
      if (!empty($configuration['language'])) {
        $params['language'] = $configuration['language'];
      }
      $task = $configuration['task'];
      $response = $this->client->audio()->$task($params);
      return $response;
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
    }
  }

}
