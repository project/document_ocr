<?php

namespace Drupal\document_ocr\EventSubscriber;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\document_ocr\Event\TaskDestinationAfterSave;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\document_ocr\Entity\Data;

/**
 * Class TaskEventSubscriber.
 *
 * @ingroup document_ocr
 */
class TaskEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents(): array {
    return [TaskDestinationAfterSave::EVENT => 'processData'];
  }

  /**
   * React to a ocr process being complete.
   *
   * @param \Drupal\document_ocr\Event\TaskDestinationAfterSave $event
   *   process event.
   * @throws EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function processData(TaskDestinationAfterSave $event) {
    $entity = $event->getEntity();
    $file = $event->getFile();
    $processor = $event->getProcessor();
    $configuration = $processor->getConfiguration();
    if (!empty($configuration['store_json'])) {
      if ($json = $processor->getDocumentAsJson()) {
        $existing = \Drupal::entityTypeManager()->getStorage('document_ocr_data')->loadByProperties([
          'file' => $file->id(),
        ]);
        if (!$data_entity = reset($existing)) {
          $data_entity = Data::create([]);
        }
        $data_entity->attachDestination($entity);
        $data_entity->setData($json);
        $data_entity->attachFile($file);
        $data_entity->save();
      }
    }
  }

}
