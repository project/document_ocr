<?php

namespace Drupal\document_ocr\Traits;

use Drupal\Core\File\FileSystemInterface;

/**
 * Mapping form custom value trait.
 */
trait CustomValueTrait {

  /**
   * Build custom value form element based on field type.
   */
  protected function customFieldType($field_name, $field_types, $default_value = '') {
    $element = [
      '#title' => $this->t('Custom Value'),
      '#title_display' => 'invisible',
      '#placeholder' => $this->t('Provide custom value'),
      '#default_value' => $default_value,
      '#states' => [
        'visible' => [
          ':input[data-transformer="' . $field_name . '"]' => ['value' => 'custom_value'],
        ],
      ],
    ];
    $field_type = $field_types[$field_name];
    if ($field_type == 'boolean') {
      $element['#type'] = 'checkbox';
      $element['#description'] = $this->t('Override value');
    }
    else if ($field_type == 'language') {
      $langcodes = \Drupal::languageManager()->getLanguages();
      $element['#type'] = 'select';
      $element['#options'] = array_combine(array_keys($langcodes), array_keys($langcodes));
      $element['#description'] = $this->t('Choose available language.');
    }
    else if (strstr($field_type, 'text') || strstr($field_type, 'string_long')) {
      $element['#type'] = 'textarea';
      $element['#description'] = $this->t('You may use tokens like <strong>%extracted_text%</strong> or other property names.');
    }
    else {
      $element['#type'] = (strstr($field_type, 'number')) ? 'number' : 'textfield';
      if ($element['#type'] == 'textfield') {
        $element['#description'] = $this->t('You may use tokens like <strong>%extracted_text%</strong> or other property names.');
      }
    }
    if (in_array($field_type, ['entity_reference', 'changed', 'created'])) {
      $element['#type'] = 'number';
    }
    return $element;
  }

}
