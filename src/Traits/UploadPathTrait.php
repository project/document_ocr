<?php

namespace Drupal\document_ocr\Traits;

use Drupal\Core\File\FileSystemInterface;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * Upload path trait.
 */
trait UploadPathTrait {

  /**
   * Get upload path based on system settings.
   */
  protected function getUploadPath() {
    $path = DocumentOcrInterface::PUBLIC_URI;
    if (\Drupal::service('stream_wrapper_manager')->isValidScheme('private')) {
      $path = DocumentOcrInterface::PRIVATE_URI;
    }
    $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
    return $path;
  }

}
