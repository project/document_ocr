<?php

namespace Drupal\document_ocr\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * Defines DocumentOcrProcessor annotation object.
 *
 * Plugin Namespace: Plugin\document_ocr\Plugin\document_ocr\processor.
 *
 * @see \Drupal\document_ocr\Plugin\ProcessorManager
 * @see plugin_api
 *
 * @Annotation
 */
class DocumentOcrProcessor extends Plugin {

  /**
   * Plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * Plugin name.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

  /**
   * Plugin group.
   *
   * @var string
   */
  public $group;

  /**
   * Dialog width.
   *
   * @var string
   */
  public $config_dialog_width = DocumentOcrInterface::PROCESSOR_CONFIG_DIALOG_WIDTH;

  /**
   * Plugin support file extensions.
   *
   * @var string
   */
  public $extensions;

  /**
   * Indicates required elements.
   *
   * @var array
   */
  public $requires = [];

  /**
   * Indicates supported functionality.
   *
   * @var array
   */
  public $supports = [];

}
