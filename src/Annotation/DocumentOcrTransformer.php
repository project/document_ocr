<?php

namespace Drupal\document_ocr\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * Defines DocumentOcrTransformer annotation object.
 *
 * Plugin Namespace: Plugin\document_ocr\Plugin\document_ocr\transformer.
 *
 * @see \Drupal\document_ocr\Plugin\TransformerManager
 * @see plugin_api
 *
 * @Annotation
 */
class DocumentOcrTransformer extends Plugin {

  /**
   * Plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * Plugin name.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

  /**
   * Plugin group.
   *
   * @var string
   */
  public $group;

  /**
   * Dialog width.
   *
   * @var string
   */
  public $config_dialog_width = DocumentOcrInterface::TRANSFORMER_CONFIG_DIALOG_WIDTH;

  /**
   * Plugin description.
   *
   * @var string
   */
  public $description;

  /**
   * Indicates required elements.
   *
   * @var array
   */
  public $requires = [];

  /**
   * A list of supported field types.
   *
   * @var array
   */
  public $field_types = [];

}
