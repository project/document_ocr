<?php

namespace Drupal\document_ocr\Event;

/**
 * Event that is fired before a destination entity is created.
 */
class TaskDestinationBeforeSave extends TaskDestinationBase {
  const EVENT = 'document_ocr.task_create_desination_before';
}
