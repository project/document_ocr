<?php

namespace Drupal\document_ocr\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Base class for TaskDestination events.
 */
abstract class TaskDestinationBase extends Event {

  /**
   * The file to process.
   *
   * @var \Drupal\file\FileInterface
   */
  private $file;

  /**
   * The entity to fill with the processed data.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  private $entity;

  /**
   * The processor.
   *
   * @var \Drupal\document_ocr\Plugin\ProcessorInterface|null
   */
  private $processor;

  /**
   * Initializes the event.
   * @param $file
   * @param $entity
   * @param $processor
   */
  public function __construct($file, $entity, $processor) {
    $this->file = $file;
    $this->entity = $entity;
    $this->processor = $processor;
  }

  /**
   * Retrieves the processed file.
   */
  public function getFile() {
    return $this->file;
  }

  /**
   * Returns the entity with the processed data.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Returns the processor data.
   */
  public function getProcessor() {
    return $this->processor;
  }

}
