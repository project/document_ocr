<?php

namespace Drupal\document_ocr\ParamConverter;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Route;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\document_ocr\Plugin\ProcessorManager;

/**
 * Resolves "document_ocr_processor" type parameters in routes.
 */
class ProcessorConverter implements ParamConverterInterface {

  /**
   * Processor manager.
   *
   * @var \Drupal\document_ocr\Plugin\ProcessorManager
   */
  private $processor;

  /**
   * Logger service.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * ProcessorConverter constructor.
   *
   * @param \Drupal\document_ocr\Plugin\ProcessorManager $processor
   *   The Processor plugin manager.
   * @param LoggerChannelFactoryInterface $logger_factory
   */
  public function __construct(ProcessorManager $processor, LoggerChannelFactoryInterface $logger_factory) {
    $this->processor = $processor;
    $this->loggerFactory = $logger_factory->get('document_ocr');
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    try {
      $id = str_replace('-', '_', $value);
      $processor = $this->processor->createInstance($id);
      if ($processor->requirementsAreMet()) {
        return $processor;
      }
      $this->loggerFactory->error($id . ' processor plugin requirements are not met. Check composer.json file for missing dependencies.');
    }
    catch (PluginException $e) {
      throw new NotFoundHttpException();
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return (!empty($definition['type']) && $definition['type'] == 'document_ocr_processor');
  }

}
