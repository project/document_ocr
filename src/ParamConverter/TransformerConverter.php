<?php

namespace Drupal\document_ocr\ParamConverter;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Route;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\document_ocr\Plugin\TransformerManager;

/**
 * Resolves "document_ocr_transformer" type parameters in routes.
 */
class TransformerConverter implements ParamConverterInterface {

  /**
   * Transformer manager.
   *
   * @var \Drupal\document_ocr\Plugin\TransformerManager
   */
  private $transformer;

  /**
   * Logger service.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * TransformerConverter constructor.
   *
   * @param \Drupal\document_ocr\Plugin\TransformerManager $transformer
   *   The Transformer plugin manager.
   * @param LoggerChannelFactoryInterface $logger_factory
   */
  public function __construct(TransformerManager $transformer, LoggerChannelFactoryInterface $logger_factory) {
    $this->transformer = $transformer;
    $this->loggerFactory = $logger_factory->get('document_ocr');
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    try {
      $id = str_replace('-', '_', $value);
      $transformer = $this->transformer->createInstance($id);
      if ($transformer->requirementsAreMet()) {
        return $transformer;
      }
      $this->loggerFactory->error($id . ' processor plugin requirements are not met. Check composer.json file for missing dependencies.');
    }
    catch (PluginException $e) {
      throw new NotFoundHttpException();
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return (!empty($definition['type']) && $definition['type'] == 'document_ocr_transformer');
  }

}
