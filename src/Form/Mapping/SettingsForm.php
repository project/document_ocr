<?php

namespace Drupal\document_ocr\Form\Mapping;

use Drupal\Core\Form\FormStateInterface;

/**
 * Document OCR Mapping form.
 *
 * @ingroup document_ocr
 */
class SettingsForm extends BaseMappingForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'document_ocr_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $document_ocr_processor = NULL) {
    $processor = $document_ocr_processor->getProcessorPlugin();
    if (($processor->requireCredentials() && empty($document_ocr_processor->getCredentials())) || $this->getKeyVal('processor') != $document_ocr_processor->id()) {
      return $this->redirecToListing();
    }
    $form = parent::buildForm($form, $form_state);
    if (!$this->isProcessorAsynchronous($document_ocr_processor)) {
      $attempt_values = range(3, 20);
      $form['realtime_processing'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Real-time processing (slower, not recommeded)'),
        '#description' => $this->t('Document OCR configured entity will be processed immediately, otherwise entity processing will be queued and executed on cron. When a processor is using asynchronous method for processing this option is ignored.'),
      ];
      $form['attempts'] = [
        '#type' => 'select',
        '#title' => $this->t('Attempt limit'),
        '#options' => array_combine($attempt_values, $attempt_values),
        '#description' => $this->t('Set the limit for the processing attempts. When a processor is using asynchronous method for processing this option is ignored.'),
        '#default_value' => 3,
      ];
    }
    else {
      $form['processing_info'] = [
        '#type' => 'item',
        '#markup' => $this->t('%name processor is using asynchronous method for document processing thus attempt limits are configured in the processor configuration form.', ['%name' => $document_ocr_processor->label()]),
        '#prefix' => '<div class="form-item__description">',
        '#suffix' => '</div>'
      ];
    }
    $form['delete_destination'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete destination entity on source file delete'),
      '#description' => $this->t('This option allows imported content deletion when files associated with it are deleted.'),
      '#default_value' => TRUE,
    ];
    $form['process_update'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Re-run document processing on source entity update'),
      '#description' => $this->t('This option allows to re-run document processing when source entity is updated.'),
      '#default_value' => TRUE,
    ];
    $form['actions']['submit']['#value'] = $this->t('Save Mapping');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->setKeyVal('settings', [
      'realtime_processing' => !empty($form_state->getValue('realtime_processing')),
      'attempts' => !empty($form_state->getValue('attempts')) ? (int) $form_state->getValue('attempts') : 3,
      'delete_destination' => (bool) $form_state->getValue('delete_destination'),
      'process_update' => (bool) $form_state->getValue('process_update'),
    ]);
    $this->saveMapping();
  }

}
