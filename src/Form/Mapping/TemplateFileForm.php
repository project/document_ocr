<?php

namespace Drupal\document_ocr\Form\Mapping;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Template file form and redirect to mapping tool.
 *
 * @ingroup document_ocr
 */
class TemplateFileForm extends BaseMappingForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'document_ocr_template_file_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $document_ocr_mapping = NULL) {
    $form = parent::buildForm($form, $form_state);
    // Disable progressbar.
    unset($form['progressbar']);
    $processor = $this->getProcessor($document_ocr_mapping->getProcessor())->getProcessorPlugin();
    $form_state->set('document_ocr_mapping', $document_ocr_mapping);
    $form['template_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Template File'),
      '#upload_location' => $this->getUploadPath(),
      '#upload_validators' => [
        'file_validate_extensions' => [$processor->getExtensions()],
      ],
      '#description' => $this->t('Upload template file for the processor. The file will be used to get data for the mapping tool. <strong>Please try use the same file you used to create the mapping</strong>.'),
      '#required' => TRUE,
    ];
    $form['actions']['submit']['#value'] = $this->t('Continue to Mapping');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $template_file = $form_state->getValue('template_file', 0);
    if (!empty($template_file[0])) {
      if ($file = File::load($template_file[0])) {
        $this->setKeyVal('template_file', $file);
      }
    }
    $form_state->setRedirect('entity.document_ocr_mapping.mapping', [
      'document_ocr_mapping' => $form_state->get('document_ocr_mapping')->id(),
    ]);
  }

}
