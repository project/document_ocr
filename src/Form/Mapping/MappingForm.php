<?php

namespace Drupal\document_ocr\Form\Mapping;

use Drupal\Core\Form\FormStateInterface;
use Drupal\document_ocr\Traits\CustomValueTrait;

/**
 * Document OCR Mapping form.
 *
 * @ingroup document_ocr
 */
class MappingForm extends BaseMappingForm {

  use CustomValueTrait;

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'document_ocr_mapping_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $document_ocr_processor = NULL) {
    $form = parent::buildForm($form, $form_state);
    $credentials = $document_ocr_processor->getCredentials();
    $processor = $document_ocr_processor->getProcessorPlugin();
    $template_file = $this->getKeyVal('template_file');
    if ((empty($credentials) && $processor->requireCredentials()) || $this->getKeyVal('processor') != $document_ocr_processor->id()
      || ($this->isTemplateRequired($document_ocr_processor) && !file_exists($template_file->getFileUri()))) {
      return $this->redirecToListing();
    }
    if ($processor->requireCredentials()) {
      $processor->setCredentials($credentials);
    }
    $processor->setConfiguration($document_ocr_processor->getConfiguration())
      ->setFile($template_file);
    $mapping_data = $processor->getMappingData();
    if (!$mapping_data || empty($mapping_data->getOptions())) {
      return [
        '#markup' => $this->t('Unable to retreive document properties, please retry or use a different file as a template. Please see the logs for the errors.')
      ];
    }
    $fields = [];
    $field_types = [];
    $destination_bundle = $this->getKeyVal('destination_bundle');
    if ($drupal_fields = $this->entityFieldManager->getFieldDefinitions($destination_bundle['entity_type_id'], $destination_bundle['bundle'])) {
      foreach ($drupal_fields as $field_name => $field_definition) {
        if (!in_array($field_name, $this->excluded_fields)) {
          $field_types[$field_name] = $field_definition->getType();
          $fields[$field_name] = $this->t('<strong>@label</strong> <span class="form-item__description">(@name) (%type)</span>', [
            '@label' => $field_definition->getLabel(),
            '@name' => $field_name,
            '%type' => $field_definition->getType()
          ]);
        }
      }
    }
    $form['mapping'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Entity Field'), $this->t('Document Property'), $this->t('Property Transformer'), '',
      ],
      '#empty' => $this->t('There are no field mapping configured. Please add mapping below.'),
    ];

    foreach ($fields as $field_name => $field_label) {
      $form['mapping'][$field_name]['field'] = [
        '#markup' => $field_label,
      ];

      $form['mapping'][$field_name]['property'] = [
        '#type' => 'select',
        '#title' => $this->t('Property'),
        '#title_display' => 'invisible',
        '#empty_option' => $this->t('- Select Document Property -'),
        '#options' => $mapping_data->getOptions(),
        '#description' => $this->t('Document property'),
      ];

      $transformers = $this->getTransformers($field_types[$field_name]);
      $transformers['custom_value'] = $this->t('Custom Value');

      $form['mapping'][$field_name]['transformer'] = [
        '#type' => 'select',
        '#title' => $this->t('Property Transformer'),
        '#title_display' => 'invisible',
        '#options' => $transformers,
        '#default_value' => 'default',
        '#attributes' => ['data-transformer' => $field_name],
        '#description' => $this->t('Transformer plugin'),
      ];

      $form['mapping'][$field_name]['custom_value'] = $this->customFieldType($field_name, $field_types);
    }
    $form['actions']['submit']['#value'] = $this->t('Continue to Settings');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mapping = [];
    foreach ($form_state->getValue('mapping') as $field_name => $details) {
      $transformer = !empty($details['transformer']) ? $details['transformer'] : '';
      if (!empty($details['property']) && $transformer != 'custom_value') {
        $mapping[$field_name] = [
          'property' => $details['property'],
          'transformer' => $transformer,
        ];
      }
      if ($transformer == 'custom_value') {
        $mapping[$field_name]['transformer'] = 'custom_value';
        $mapping[$field_name]['custom_value'] = $details['custom_value'];
      }
    }
    $this->setKeyVal('mapping', $mapping);
    $form_state->setRedirect('document_ocr_mapping.settings', [
      'document_ocr_processor' => $this->getKeyVal('processor'),
    ]);
  }

}
