<?php

namespace Drupal\document_ocr\Form\Mapping;

use Drupal\document_ocr\Form\BaseForm;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * BaseMappingForm class.
 *
 * @ingroup document_ocr
 */
abstract class BaseMappingForm extends BaseForm {

  protected $temporary_storage_key = DocumentOcrInterface::MAPPING_STORAGE_KEY;
  protected $step_routes = [
    'document_ocr_mapping.new' => 'Details',
    'document_ocr_mapping.mapping' => 'Mapping',
    'document_ocr_mapping.settings' => 'Settings',
  ];

}
