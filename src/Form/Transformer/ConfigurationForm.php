<?php

namespace Drupal\document_ocr\Form\Transformer;

use Drupal\Core\Form\FormStateInterface;

/**
 * Processor plugin configuration form.
 *
 * @ingroup document_ocr
 */
class ConfigurationForm extends BaseTransformerForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    $transformer = $this->currentRouteMatch->getParameter('document_ocr_transformer');
    return 'document_ocr_transformer_' . $transformer->getPluginId();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $document_ocr_transformer = NULL) {
    if ($this->getKeyVal('transformer') != $document_ocr_transformer->getPluginId()) {
      return $this->redirecToListing();
    }
    try {
      $form_state->set('document_ocr_transformer', $document_ocr_transformer);
      $form = parent::buildForm($form, $form_state);
      $form['#attached']['library'][] = 'document_ocr/document_ocr.admin';
      $document_ocr_transformer->setCredentials($this->getKeyVal('credentials'));
      $configuration_form = $document_ocr_transformer->configurationForm($form, $form_state);
      if (empty($configuration_form)) {
        $form['empty'] = [
          '#markup' => '<p>' . $this->t('This transformer does not provide configuration options.') . '</p>'
        ];
      }
      else {
        if ($configuration_form !== $form) {
          $form = array_merge($form, $configuration_form);
          $form['actions']['submit']['#value'] = $this->t('Add %name', ['%name' => $this->getKeyVal('label')]);
        }
      }
    }
    catch (\Exception $ex) {
      \Drupal::messenger()->addMessage($document_ocr_transformer->getPluginId() . ': ' . $ex->getMessage(), 'error');
      return $this->redirecToListing();
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($configuration = $form_state->get('document_ocr_transformer')->configurationValues($form, $form_state)) {
      $this->setKeyVal('configuration', $configuration);
    }
    $this->saveTransformer();
    $form_state->setRedirect('entity.document_ocr_transformer.collection');
  }

}
