<?php

namespace Drupal\document_ocr\Form\Transformer;

use Drupal\Core\Form\FormStateInterface;

/**
 * Document OCR new transformer form.
 *
 * @ingroup document_ocr
 */
class NewForm extends BaseTransformerForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'document_ocr_transformer_new_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Reset all previously set temp variables.
    $this->deleteKeyVal();
    $form = parent::buildForm($form, $form_state);

    $form['#attached']['drupalSettings']['document_ocr']['transformers'] = $this->getTransformerRequireCredentials();
    $form['#attached']['library'][] = 'document_ocr/document_ocr.admin';
    $form['#attached']['library'][] = 'document_ocr/document_ocr.transformer.admin';

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#size' => 100,
      '#description' => $this->t('A short name to help you identify this transformer in the list.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('ID'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#machine_name' => [
        'exists' => 'Drupal\document_ocr\Entity\Transformer::load',
      ],
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#rows' => 3,
    ];

    $form['transformer'] = [
      '#title' => $this->t('Transformer'),
      '#type' => 'select',
      '#description' => $this->t('Choose property transformer plugin.'),
      '#options' => $this->getTransformerPlugins(),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::ajaxGetExtensionsForm',
        'wrapper' => 'description-container',
        'method' => 'replace',
      ],
    ];

    $form['credentials'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Transformer Credentials'),
      '#placeholder' => $this->getUploadPath() . '/you-config.json',
      '#description' => $this->t('JSON file with required credentials for the transformer.'),
    ];

    $form['description_container'] = [
      '#type' => 'container',
      '#prefix' => '<div id="description-container">',
      '#suffix' => '</div>',
    ];

    if ($form_state->getValue('transformer')) {
      if ($transformer = $this->getTransformerPlugin($form_state->getValue('transformer'))) {
        $form['description_container']['description'] = [
          '#type' => 'item',
          '#title' => $this->t('Description'),
          '#title_display' => 'invisible',
          '#markup' => $transformer->getDescription(),
        ];
      }
    }

    $form['actions']['submit']['#value'] = $this->t('Continue to Configuration');
    $form['actions']['cancel']['#attributes']['class'] = [
      'button', 'dialog-cancel',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->setKeyVal('id', $form_state->getValue('id'))
      ->setKeyVal('label', $form_state->getValue('label'))
      ->setKeyVal('description', $form_state->getValue('description'))
      ->setKeyVal('transformer', $form_state->getValue('transformer'))
      ->setKeyVal('credentials', $form_state->getValue('credentials'));
    $form_state->setRedirect('document_ocr_transformer.configuration', [
      'document_ocr_transformer' => $form_state->getValue('transformer'),
    ]);
  }

  /**
   * Handles submit call when alias type is selected.
   */
  public function submitSelectType(array $form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * Handles switching the processor selector.
   */
  public function ajaxGetExtensionsForm($form, FormStateInterface $form_state) {
    return $form['description_container'];
  }

}
