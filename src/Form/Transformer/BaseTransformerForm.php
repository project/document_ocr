<?php

namespace Drupal\document_ocr\Form\Transformer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\document_ocr\Form\BaseForm;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * BaseTransformerForm class.
 *
 * @ingroup document_ocr
 */
abstract class BaseTransformerForm extends BaseForm {

  protected $temporary_storage_key = DocumentOcrInterface::TRANSFORMER_STORAGE_KEY;
  protected $step_routes = [
    'document_ocr_transformer.new' => 'Details',
    'document_ocr_transformer.configuration' => 'Configuration',
  ];

  /**
   * {@inheritdoc}
   */
  protected function redirecToListing() {
    return $this->redirect('entity.document_ocr_transformer.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['actions']['cancel']['#url'] = Url::fromRoute('entity.document_ocr_transformer.collection');
    return $form;
  }

  /**
   * Check if plugin configuration require credentials.
   */
  protected function getTransformerRequireCredentials() {
    $plugins = [];
    foreach ($this->transformerManager->getDefinitions() as $id => $details) {
      $plugin = $this->transformerManager->createInstance($id);
      $plugins[$id] = $plugin->requireCredentials();
    }
    return $plugins;
  }

}
