<?php

namespace Drupal\document_ocr\Form\OneTime;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\file\Entity\File;

/**
 * Document OCR New form.
 *
 * @ingroup document_ocr
 */
class NewForm extends BaseOneTimeForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'document_ocr_onetime_new_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Reset all previously set temp variables.
    $this->deleteKeyVal();
    $form = parent::buildForm($form, $form_state);
    $form['#attached']['library'][] = 'document_ocr/document_ocr.mapping.admin';

    $form['processor'] = [
      '#title' => $this->t('Processor'),
      '#type' => 'select',
      '#description' => $this->t('Choose document processor.'),
      '#options' => $this->getProcessors(),
      '#limit_validation_errors' => [['processor']],
      '#validated' => TRUE,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'ajaxGetTemplateFileForm'],
        'wrapper' => 'template-file-container',
        'method' => 'replace',
      ],
    ];

    $bundle_options = [];
    // Get all applicable entity types.
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if (is_subclass_of($entity_type->getClass(), FieldableEntityInterface::class) && $entity_type->hasLinkTemplate('canonical')) {
        if ($entity_type->hasKey('bundle')) {
          if ($bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type->id())) {
            foreach ($bundles as $id => $info) {
              $bundle_options[(string)$entity_type->getLabel()][$entity_type->id() . '::' . $id] = $info['label'];
            }
          }
        }
      }
    }
    $form['destination_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Destination Bundle'),
      '#options' => $bundle_options,
      '#required' => TRUE,
      '#description' => $this->t('Entity type where you want to save document properties.'),
      '#limit_validation_errors' => [['destination_bundle']],
      '#submit' => ['::submitSelectType'],
      '#executes_submit_callback' => TRUE,
      '#ajax' => [
        'callback' => '::ajaxGetFieldsForm',
        'wrapper' => 'fields-container',
        'method' => 'replace',
      ],
    ];

    $form['template_file_wrapper'] = [
      '#type' => 'container',
      '#prefix' => '<div id="template-file-container">',
      '#suffix' => '</div>',
    ];
    if (!empty($form_state->getUserInput()['processor'])) {
      $document_ocr_processor = $this->getProcessor($form_state->getUserInput()['processor']);
      $processor = $document_ocr_processor->getProcessorPlugin();
      $processor_extensions = $processor->getExtensions();
      if ($this->isTemplateRequired($document_ocr_processor)) {
        $form['template_file_wrapper']['template_file'] = [
          '#type' => 'managed_file',
          '#title' => $this->t('Template File'),
          '#upload_location' => $this->getUploadPath(),
          '#limit_validation_errors' => [['template_file']],
          '#upload_validators' => [
            'file_validate_extensions' => [$processor_extensions],
          ],
          '#description' => $this->t('Upload template file for the processor. The file will be used to get data for the mapping tool.<br/>Allowed extensions: %list', [
            '%list' => $processor_extensions,
          ]),
          '#required' => TRUE,
        ];
      }
    }

    $form['fields_container'] = [
      '#type' => 'container',
      '#prefix' => '<div id="fields-container">',
      '#suffix' => '</div>',
    ];

    if (!empty($form_state->getUserInput()['destination_bundle'])) {
      if (!empty($form_state->getUserInput()['processor'])) {
        $processor_extensions = explode(' ', $this->getProcessor($form_state->getUserInput()['processor'])->getProcessorPlugin()->getExtensions());
        list($entity_type_id, $bundle) = explode('::', $form_state->getUserInput()['destination_bundle']);
        if ($fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle)) {
          $field_options = [];
          foreach ($fields as $field_name => $field_definition) {
            if (!empty($field_definition->getTargetBundle())) {               
              if (in_array($field_definition->getType(), ['image', 'file'])) {
                $extensions = $field_definition->getSetting('file_extensions');
                if (array_intersect($processor_extensions, explode(' ', $extensions))) {
                  $field_options[$field_name] = $field_definition->getLabel() . ' (' . $field_definition->getType() . ') ' . $extensions;
                }
              }
            }
          }
          $form['fields_container']['field'] = [
            '#title' => $this->t('Field (optional)'),
            '#type' => 'select',
            '#options' => $field_options,
            '#empty_option' => $this->t('- Select Field -'),
            '#description' => $this->t('Attach processing file to a field.'),
          ];
        }
      }
    }

    $form['actions']['submit']['#value'] = $this->t('Continue to Mapping');
    $form['actions']['cancel']['#attributes']['class'] = [
      'button', 'dialog-cancel',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getUserInput()['processor'])) {
      $document_ocr_processor = $this->getProcessor($form_state->getUserInput()['processor']);
      $processor = $document_ocr_processor->getProcessorPlugin();
      $template_file = $form_state->getValue('template_file', 0);
      if (empty($template_file[0]) && $this->isTemplateRequired($document_ocr_processor)) {
        $form_state->setErrorByName('template_file', $this->t('Template File field is required for the mapping.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->setKeyVal('processor', $form_state->getValue('processor'))
      ->setKeyVal('destination_bundle', $form_state->getValue('destination_bundle'))
      ->setKeyVal('field', $form_state->getUserInput()['field']);
    $template_file = $form_state->getValue('template_file', 0);
    if (!empty($template_file[0])) {
      if ($file = File::load($template_file[0])) {
        $this->setKeyVal('template_file', $file);
      }
    }
    $form_state->setRedirect('document_ocr_mapping.onetime_mapping', [
      'document_ocr_processor' => $form_state->getValue('processor'),
    ]);
  }

  /**
   * Handles submit call when alias type is selected.
   */
  public function submitSelectType(array $form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * Handles switching the type selector.
   */
  public function ajaxGetFieldsForm($form, FormStateInterface $form_state) {
    return $form['fields_container'];
  }

  /**
   * Handles switching the processor selector.
   */
  public function ajaxGetTemplateFileForm($form, FormStateInterface $form_state) {
    return $form['template_file_wrapper'];
  }

}
