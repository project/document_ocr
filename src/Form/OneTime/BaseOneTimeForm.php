<?php

namespace Drupal\document_ocr\Form\OneTime;

use Drupal\document_ocr\Form\BaseForm;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * BaseOneTimeForm class.
 *
 * @ingroup document_ocr
 */
abstract class BaseOneTimeForm extends BaseForm {

  protected $temporary_storage_key = DocumentOcrInterface::ONETIME_STORAGE_KEY;
  protected $step_routes = [
    'document_ocr_mapping.onetime_new' => 'Details',
    'document_ocr_mapping.onetime_mapping' => 'Mapping',
    'document_ocr_mapping.onetime_files' => 'Upload Files',
  ];

}
