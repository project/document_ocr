<?php

namespace Drupal\document_ocr\Form\OneTime;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Document OCR Mapping form.
 *
 * @ingroup document_ocr
 */
class FilesForm extends BaseOneTimeForm {

  protected $document_ocr_processor;

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'document_ocr_onetime_files_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $document_ocr_processor = NULL) {
    $credentials = $document_ocr_processor->getCredentials();
    $processor = $document_ocr_processor->getProcessorPlugin();
    if ((empty($credentials) && $processor->requireCredentials()) || $this->getKeyVal('processor') != $document_ocr_processor->id()) {
      return $this->redirecToListing();
    }
    $form = parent::buildForm($form, $form_state);
    $this->document_ocr_processor = $document_ocr_processor;
    $form['files'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Files'),
      '#upload_location' => $this->getUploadPath(),
      '#upload_validators' => [
        'file_validate_extensions' => [$processor->getExtensions()],
      ],
      '#multiple' => TRUE,
      '#description' => $this->t('Upload files to process.'),
      '#required' => TRUE,
    ];
    $form['actions']['submit']['#value'] = $this->t('Process Files');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $files = $form_state->getValue('files', 0);
    $batch = [
      'title' => $this->t('Processing Files'),
      'finished' => '\Drupal\document_ocr\Services\Batch::completeProcessCallback',
      'operations' => [],
    ];
    foreach ($files as $fid) {
      if ($file = File::load($fid)) {
        $item = new \stdClass();
        $item->processor = $this->document_ocr_processor;
        $item->field = $this->getKeyVal('field');
        $item->mapping = $this->getKeyVal('mapping');
        $item->destination_bundle = $this->getKeyVal('destination_bundle');
        $item->file = $file;
        $batch['operations'][] = [
          '\Drupal\document_ocr\Services\Batch::processOneTime', [$item],
        ];
      }
    }
    batch_set($batch);
    $this->deleteKeyVal();
    $form_state->setRedirect('entity.document_ocr_mapping.collection');
  }

}
