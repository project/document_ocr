<?php

namespace Drupal\document_ocr\Form\Processor;

use Drupal\Core\Form\FormStateInterface;

/**
 * Document OCR new processor form.
 *
 * @ingroup document_ocr
 */
class NewForm extends BaseProcessorForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'document_ocr_processor_new_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Reset all previously set temp variables.
    $this->deleteKeyVal();
    $form = parent::buildForm($form, $form_state);
    $form['#attached']['drupalSettings']['document_ocr']['processors'] = $this->getProcessorRequireCredentials();
    $form['#attached']['library'][] = 'document_ocr/document_ocr.admin';
    $form['#attached']['library'][] = 'document_ocr/document_ocr.processor.admin';

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#size' => 100,
      '#description' => $this->t('A short name to help you identify this processor in the list.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('ID'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#machine_name' => [
        'exists' => 'Drupal\document_ocr\Entity\Processor::load',
      ],
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#rows' => 3,
    ];

    $form['processor'] = [
      '#title' => $this->t('Processor'),
      '#type' => 'select',
      '#description' => $this->t('Choose document processor plugin.'),
      '#options' => $this->getProcessorPlugins(TRUE),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::ajaxGetExtensionsForm',
        'wrapper' => 'extensions-container',
        'method' => 'replace',
      ],
    ];

    $form['credentials'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Processor Credentials'),
      '#placeholder' => $this->getUploadPath() . '/you-config.json',
      '#description' => $this->t('JSON file with required credentials for the processor.'),
    ];

    $form['extensions_container'] = [
      '#type' => 'container',
      '#prefix' => '<div id="extensions-container">',
      '#suffix' => '</div>',
    ];

    if ($form_state->getValue('processor')) {
      if ($processor = $this->getProcessorPlugin($form_state->getValue('processor'))) {
        $form['extensions_container']['credentials'] = [
          '#type' => 'item',
          '#title' => $this->t('%name supported extensions', ['%name' => $processor->getName()]),
          '#markup' => '<span class="form-item__description">' . $processor->getExtensions() . '</span>',
        ];
      }
    }

    $form['actions']['submit']['#value'] = $this->t('Continue to Configuration');
    $form['actions']['cancel']['#attributes']['class'] = [
      'button', 'dialog-cancel',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->setKeyVal('id', $form_state->getValue('id'))
      ->setKeyVal('label', $form_state->getValue('label'))
      ->setKeyVal('description', $form_state->getValue('description'))
      ->setKeyVal('processor', $form_state->getValue('processor'))
      ->setKeyVal('credentials', $form_state->getValue('credentials'));

    $form_state->setRedirect('document_ocr_processor.configuration', [
      'document_ocr_processor' => $form_state->getValue('processor'),
    ]);
  }

  /**
   * Handles switching the processor selector.
   */
  public function ajaxGetExtensionsForm($form, FormStateInterface $form_state) {
    return $form['extensions_container'];
  }

}
