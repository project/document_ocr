<?php

namespace Drupal\document_ocr\Form\Processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\document_ocr\Form\BaseForm;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * BaseProcessorForm class.
 *
 * @ingroup document_ocr
 */
abstract class BaseProcessorForm extends BaseForm {

  protected $temporary_storage_key = DocumentOcrInterface::PROCESSOR_STORAGE_KEY;
  protected $step_routes = [
    'document_ocr_processor.new' => 'Details',
    'document_ocr_processor.configuration' => 'Configuration',
  ];

  /**
   * {@inheritdoc}
   */
  protected function redirecToListing() {
    return $this->redirect('entity.document_ocr_processor.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['actions']['cancel']['#url'] = Url::fromRoute('entity.document_ocr_processor.collection');
    $form['actions']['#weight'] = 999;
    return $form;
  }

  /**
   * Check if plugin configuration require credentials.
   */
  protected function getProcessorRequireCredentials() {
    $plugins = [];
    foreach ($this->processorManager->getDefinitions() as $id => $details) {
      $plugin = $this->processorManager->createInstance($id);
      $plugins[$id] = $plugin->requireCredentials();
    }
    return $plugins;
  }

}
