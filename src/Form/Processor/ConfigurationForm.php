<?php

namespace Drupal\document_ocr\Form\Processor;

use Drupal\Core\Form\FormStateInterface;

/**
 * Processor plugin configuration form.
 *
 * @ingroup document_ocr
 */
class ConfigurationForm extends BaseProcessorForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    $processor = $this->currentRouteMatch->getParameter('document_ocr_processor');
    return 'document_ocr_processor_' . $processor->getPluginId();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $document_ocr_processor = NULL) {
    if ($this->getKeyVal('processor') != $document_ocr_processor->getPluginId()) {
      return $this->redirecToListing();
    }
    try {
      $form_state->set('document_ocr_processor', $document_ocr_processor);
      $form = parent::buildForm($form, $form_state);
      /** @var \Drupal\document_ocr\Plugin\ProcessorBase $document_ocr_processor */
      $document_ocr_processor->setCredentials($this->getKeyVal('credentials'));
      if ($document_ocr_processor->isJsonResponse()) {
        $form['store_json'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Store API response as JSON'),
          '#default_value' => TRUE,
          '#description' => $this->t('This option enables to store API response as JSON and it can be accessed via REST resource.'),
          '#weight' => 99,
        ];
      }
      if ($document_ocr_processor->isAsynchronous()) {
        $form['asynchronous'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Asynchronous requests (best for large files, <em>queue processing only</em>)'),
          '#default_value' => !empty($configuration['asynchronous']),
          '#weight' => 100,
        ];
        $attempt_values = range(3, 20);
        $form['asynchronous_attempts'] = [
          '#type' => 'select',
          '#title' => $this->t('Attempt limit'),
          '#options' => array_combine($attempt_values, $attempt_values),
          '#description' => $this->t('Set the limit for the asynchronous processing attempts. The status of the processing task will be set to <em>Failed</em> upon reaching the limit.'),
          '#default_value' => !empty($configuration['asynchronous_attempts']) ? $configuration['asynchronous_attempts'] : 3,
          '#weight' => 101,
          '#states' => [
            'visible' => [
              ':input[name="asynchronous"]' => ['checked' => TRUE],
            ],
          ],
        ];
        $form['asynchronous_initiate'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Initiate processing right when mapped entity is saved'),
          '#description' => $this->t('This will allow perform initial async steps like uploading files to the cloud right when mapped entity is saved. This could be noticeably slower page load when saving mapped entities, but will process documents faster.'),
          '#default_value' => !empty($configuration['asynchronous_initiate']),
          '#weight' => 102,
          '#states' => [
            'visible' => [
              ':input[name="asynchronous"]' => ['checked' => TRUE],
            ],
          ],
        ];
      }
      $configuration_form = $document_ocr_processor->configurationForm($form, $form_state);
      if ($configuration_form !== $form) {
        $form = array_merge($form, $configuration_form);
        $form['actions']['submit']['#value'] = $this->t('Add %name', ['%name' => $this->getKeyVal('label')]);
      }
    }
    catch (\Exception $ex) {
      \Drupal::messenger()->addMessage($document_ocr_processor->getPluginId() . ': ' . $ex->getMessage(), 'error');
      return $this->redirecToListing();
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $document_ocr_processor = $form_state->get('document_ocr_processor');
    if ($configuration = $document_ocr_processor->configurationValues($form, $form_state)) {
      $configuration['store_json'] = (bool) $form_state->getValue('store_json');
      if ($document_ocr_processor->isAsynchronous()) {
        $configuration['asynchronous'] = (bool) $form_state->getValue('asynchronous');
        $configuration['asynchronous_attempts'] = (int) $form_state->getValue('asynchronous_attempts');
        $configuration['asynchronous_initiate'] = (bool) $form_state->getValue('asynchronous_initiate');
      }
      $this->setKeyVal('configuration', $configuration);
    }
    $this->saveProcessor();
    $form_state->setRedirect('entity.document_ocr_processor.collection');
  }

}
