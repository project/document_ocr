<?php

namespace Drupal\document_ocr\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Url;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\document_ocr\Plugin\ProcessorManager;
use Drupal\document_ocr\Plugin\TransformerManager;
use Drupal\document_ocr\Traits\TempKeyValTrait;
use Drupal\document_ocr\Traits\UploadPathTrait;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * BaseForm abstract class.
 *
 * @ingroup document_ocr
 */
abstract class BaseForm extends FormBase {
  use TempKeyValTrait;
  use UploadPathTrait;

  /**
   * Private temporary factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Private temporary factory.
   *
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * The processor manager.
   *
   * @var \Drupal\document_ocr\Plugin\ProcessorManager
   */
  protected $processorManager;

  /**
   * The transformer manager.
   *
   * @var \Drupal\document_ocr\Plugin\TransformerManager
   */
  protected $transformerManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * Exclude fiels from the mapping tool.
   */
  protected $excluded_fields = DocumentOcrInterface::MAPPING_EXCLUDE_FIELDS;

  /**
   * Constructs a \Drupal\document_ocr\BaseForm.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, FileSystemInterface $file_system, ProcessorManager $processor, TransformerManager $transformer, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityFieldManager $entity_field_manager, RouteMatchInterface $current_route_match) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->fileSystem = $file_system;
    $this->store = $this->tempStoreFactory->get($this->temporary_storage_key);
    $this->processorManager = $processor;
    $this->transformerManager = $transformer;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('file_system'),
      $container->get('plugin.manager.document_ocr_processor'),
      $container->get('plugin.manager.document_ocr_transformer'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'document_ocr/document_ocr.admin';
    $form['progressbar'] = [
      '#theme' => 'document_ocr_progressbar',
      '#steps' => $this->step_routes,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
      '#name' => 'primarySubmit',
      '#weight' => 10,
      '#prefix' => '<div class="document-ocr-button-wrapper"><span class="document-ocr-button-spinner"></span>',
      '#suffix' => '</div>',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('entity.document_ocr_mapping.collection'),
      '#weight' => 10,
      '#attributes' => ['class' => 'button'],
    ];
    $form_state->disableCache();
    return $form;
  }

  /**
   * Save new processor data.
   */
  protected function saveMapping() {
    $document = $this->entityTypeManager->getStorage('document_ocr_mapping')->create(['type' => 'document_ocr_mapping']);
    $document->set('id', $this->getKeyVal('id'));
    $document->set('label', $this->getKeyVal('label'));
    $document->set('field_selector', $this->getKeyVal('field_selector'));
    $document->set('destination_bundle', $this->getKeyVal('destination_bundle'));
    $document->set('processor', $this->getKeyVal('processor'));
    $document->set('settings', $this->getKeyVal('settings'));
    $document->set('mapping', $this->getKeyVal('mapping'));
    if ($document->save()) {
      $message = t('New mapping %name successfully created.', ['%name' => $document->label()]);
      $this->getLogger('document_ocr')->notice($message);
      $this->messenger()->addStatus($message);
    }
    else {
      $message = t('Could not save %name mapping.', ['%name' => $document->label()]);
      $this->getLogger('document_ocr')->error($message);
      $this->messenger()->addWarning($message);
    }
    $this->deleteKeyVal();
  }

  /**
   * Save new transformer data.
   */
  protected function saveTransformer() {
    $transformer = $this->entityTypeManager->getStorage('document_ocr_transformer')->create(['type' => 'document_ocr_transformer']);
    $transformer->set('id', $this->getKeyVal('id'));
    $transformer->set('label', $this->getKeyVal('label'));
    $transformer->set('description', $this->getKeyVal('description'));
    $transformer->set('credentials', $this->getKeyVal('credentials'));
    $transformer->set('configuration', $this->getKeyVal('configuration'));
    $transformer->set('transformer', $this->getKeyVal('transformer'));
    if ($transformer->save()) {
      $message = t('Transformer %name successfully added.', ['%name' => $transformer->label()]);
      $this->getLogger('document_ocr')->notice($message);
      $this->messenger()->addStatus($message);
    }
    else {
      $message = t('Could not add %name transformer.', ['%name' => $transformer->label()]);
      $this->getLogger('document_ocr')->error($message);
      $this->messenger()->addWarning($message);
    }
    $this->deleteKeyVal();
  }

  /**
   * Save new processor data.
   */
  protected function saveProcessor() {
    $processor = $this->entityTypeManager->getStorage('document_ocr_processor')->create(['type' => 'document_ocr_processor']);
    $processor->set('id', $this->getKeyVal('id'));
    $processor->set('label', $this->getKeyVal('label'));
    $processor->set('description', $this->getKeyVal('description'));
    $processor->set('credentials', $this->getKeyVal('credentials'));
    $processor->set('configuration', $this->getKeyVal('configuration'));
    $processor->set('processor', $this->getKeyVal('processor'));
    if ($processor->save()) {
      $message = t('Processor %name successfully added.', ['%name' => $processor->label()]);
      $this->getLogger('document_ocr')->notice($message);
      $this->messenger()->addStatus($message);
    }
    else {
      $message = t('Could not add %name processor.', ['%name' => $processor->label()]);
      $this->getLogger('document_ocr')->error($message);
      $this->messenger()->addWarning($message);
    }
    $this->deleteKeyVal();
  }

  /**
   * Redirect to listing page.
   */
  protected function redirecToListing() {
    return $this->redirect('entity.document_ocr_mapping.collection');
  }

  /**
   * Get processor plugins.
   * @param bool $check_requirements
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getProcessorPlugins($check_requirements = FALSE) {
    $plugins = [];
    if ($check_requirements) {
      foreach ($this->processorManager->getDefinitions() as $id => $plugin) {
        $plugin = $this->processorManager->createInstance($plugin['id']);
        if ($plugin->requirementsAreMet()) {
          $group = ($group_name = $plugin->getGroup()) ? (string) $group_name : 'Other';
          $plugins[$group][$id] = $plugin->getName();
        }
      }
    }
    else {
      foreach ($this->processorManager->getDefinitions() as $id => $plugin) {
        $plugin = $this->processorManager->createInstance($plugin['id']);
        $group = ($group_name = $plugin->getGroup()) ? (string) $group_name : 'Other';
        $plugins[$group][$id] = $plugin->getName();
      }
    }
    return $plugins;
  }

  /**
   * Get processor plugin by ID.
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getProcessorPlugin($plugin_id) {
    if ($plugin = $this->processorManager->createInstance($plugin_id)) {
      return $plugin;
    }
  }

  /**
   * Get available processors.
   */
  protected function getProcessors() {
    $options = [];
    $processors = $this->entityTypeManager->getStorage('document_ocr_processor')->loadByProperties([]);
    foreach ($processors as $processor) {
      $options[$processor->id()] = $processor->label();
    }
    return $options;
  }

  /**
   * Get processor by ID.
   */
  protected function getProcessor($id) {
    if ($processor = $this->entityTypeManager->getStorage('document_ocr_processor')->load($id)) {
      return $processor;
    }
  }

  /**
   * Get transformer plugins.
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getTransformerPlugins() {
    $plugins = [];
    foreach ($this->transformerManager->getDefinitions() as $id => $plugin) {
      $plugin = $this->transformerManager->createInstance($plugin['id']);
      $group = ($group_name = $plugin->getGroup()) ? (string) $group_name : 'Other';
      $plugins[$group][$id] = $plugin->getName();
    }
    return $plugins;
  }

  /**
   * Get transformer plugin by ID.
   */
  protected function getTransformerPlugin($plugin_id) {
    if ($plugin = $this->transformerManager->createInstance($plugin_id)) {
      return $plugin;
    }
  }

  /**
   * Get available transformers.
   */
  protected function getTransformers($field_type) {
    $options = [];
    $transformers = $this->entityTypeManager->getStorage('document_ocr_transformer')->loadByProperties([]);
    foreach ($transformers as $transformer) {
      $field_types = $transformer->getTransformerPlugin()->getFieldTypes();
      if (empty($field_types)) {
        $options[$transformer->id()] = $transformer->label();
      }
      else {
        if (in_array($field_type, $field_types)) {
          $options[$transformer->id()] = $transformer->label();
        }
      }
    }
    return $options;
  }

  /**
   * Check if processor require template or there is an exception for certain processors.
   */
  protected function isTemplateRequired($document_ocr_processor) {
    $processor = $document_ocr_processor->getProcessorPlugin()->setCredentials($document_ocr_processor->getCredentials());
    if ($skip_template = $processor->skipTemplate($document_ocr_processor)) {
      if ($processor->requireTemplate()) {
        return FALSE;
      }
    }
    else {
      if ($processor->requireTemplate()) {
        return TRUE;
      }
    }
  }

  /**
   * Check if processor is using asynchronous method.
   */
  protected function isProcessorAsynchronous($document_ocr_processor) {
    $processor = $document_ocr_processor->getProcessorPlugin();
    $configuration = $document_ocr_processor->getConfiguration();
    return ($processor->isAsynchronous() && !empty($configuration['asynchronous']));
  }

}
