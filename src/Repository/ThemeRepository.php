<?php

namespace Drupal\document_ocr\Repository;

/**
 * Theme repository class.
 *
 * @package document_ocr
 */
class ThemeRepository extends JsonRepositoryBase {
  protected $repository = 'theme';
}
