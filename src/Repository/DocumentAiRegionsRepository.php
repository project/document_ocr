<?php

namespace Drupal\document_ocr\Repository;

/**
 * Languages repository class.
 *
 * @package document_ocr
 */
class DocumentAiRegionsRepository extends JsonRepositoryBase {
  protected $repository = 'document-ai-regions';
}
