<?php

namespace Drupal\document_ocr\Repository;

use Drupal\document_ocr\Services\OpenAi as OpenAiService;

/**
 * OpenAi Models repository class.
 *
 * @package document_ocr
 */
class OpenAiModelsRepository extends JsonRepositoryBase {
  /**
   * Get supported models.
   */
  public function getModels(string $filter, OpenAiService $openai) {
    $list = [];
    if ($models = $openai->getModels()) {
      foreach ($models as $key => $val) {
        if (substr($key,0, strlen($filter)) === $filter) {
          $list[$key] = $val;
        }
      }
    }
    return $list;
  }

}
