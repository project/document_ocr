<?php

namespace Drupal\document_ocr\Repository;

/**
 * Languages repository class.
 *
 * @package document_ocr
 */
class LanguagesRepository extends JsonRepositoryBase {
  protected $repository = 'languages';
}
