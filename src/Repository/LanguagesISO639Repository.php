<?php

namespace Drupal\document_ocr\Repository;

/**
 * Languages ISO 639-1 repository class.
 *
 * @package document_ocr
 */
class LanguagesISO639Repository extends JsonRepositoryBase {
  protected $repository = 'languages-iso-639-1';
}
