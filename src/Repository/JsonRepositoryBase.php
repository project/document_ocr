<?php

namespace Drupal\document_ocr\Repository;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * JsonRepository base class.
 *
 * @package document_ocr
 */
abstract class JsonRepositoryBase {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Respitory name.
   */
  protected $repository;

  /**
   * Respitory contents.
   */
  protected $contents;

  /**
   * Respitory module.
   */
  protected $module = 'document_ocr';

  /**
   * {@inheritdoc}
   */
  public function __construct(FileSystemInterface $file_system, ModuleHandlerInterface $module_handler) {
    $this->fileSystem = $file_system;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Set repository.
   */
  public function setRespository() {
    $repository_path = $this->moduleHandler->getModule($this->module)->getPath() . '/repository/';
    $path = !empty($this->repository) ? $this->fileSystem->realpath($repository_path . $this->repository . '.json') : FALSE;
    if (!empty($path) && file_exists($path)) {
      $this->contents = trim(file_get_contents($path));
    }
    return $this;
  }

  /**
   * Get repository.
   */
  public function getRespository() {
    return $this->contents;
  }

  /**
   * Convert JSON to array.
   */
  public function toArray() {
    $this->setRespository();
    return json_decode($this->contents, TRUE);
  }

}
