<?php

namespace Drupal\document_ocr\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Processor plugin manager.
 *
 * @package document_ocr
 */
class ProcessorManager extends DefaultPluginManager {

  /**
   * Constructs an Processor object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations,.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/document_ocr/processor', $namespaces, $module_handler, 'Drupal\document_ocr\Plugin\ProcessorInterface', 'Drupal\document_ocr\Annotation\DocumentOcrProcessor');
    $this->alterInfo('document_ocr_processor');
    $this->setCacheBackend($cache_backend, 'processor');
  }

}
