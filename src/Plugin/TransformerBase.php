<?php

namespace Drupal\document_ocr\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * Transformer plugin base class.
 *
 * @package document_ocr
 */
class TransformerBase extends PluginBase implements TransformerInterface, ContainerFactoryPluginInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  protected $credentials;
  protected $configuration = [];
  protected $mapping_data;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Logger service.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->loggerFactory = $logger_factory->get('document_ocr');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->pluginDefinition['name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getGroup() {
    return !empty($this->pluginDefinition['group']) ? $this->pluginDefinition['group'] : '';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigDialogWidth() {
    return !empty($this->pluginDefinition['config_dialog_width'])
      ? $this->pluginDefinition['config_dialog_width']
      : DocumentOcrInterface::TRANSFORMER_CONFIG_DIALOG_WIDTH;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function requireCredentials() {
    return in_array('credentials', $this->pluginDefinition['requires']);
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldTypes() {
    return $this->pluginDefinition['field_types'];
  }

  /**
   * {@inheritdoc}
   */
  public function requirementsAreMet() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCredentials() {
    $path = !empty($this->credentials) ? $this->fileSystem->realpath($this->credentials) : FALSE;
    if (!empty($path) && file_exists($path)) {
      return json_decode(trim(file_get_contents($path)), TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setCredentials($credentials) {
    $this->credentials = $credentials;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value) {
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingData() {
    return $this->mapping_data;
  }

  /**
   * {@inheritdoc}
   */
  public function setMappingData($mapping_data) {
    $this->mapping_data = $mapping_data;
    return $this;
  }

  /**
   * Get available transformers.
   */
  public function getTransformers($exclude = []) {
    $list = [];
    $transformers = $this->entityTypeManager->getStorage('document_ocr_transformer')->loadByProperties([]);
    foreach ($transformers as $transformer) {
      $plugin_id = $transformer->getTransformer();
      if (empty($exclude)) {
        $list[$transformer->id()] = $transformer->label();
      }
      else {
        if (!in_array($plugin_id, $exclude)) {
          $list[$transformer->id()] = $transformer->label();
        }
      }
    }
    return $list;
  }

  /**
   * Get transformer by entity ID.
   */
  public function getTransformer($id) {
    if ($transformer = $this->entityTypeManager->getStorage('document_ocr_transformer')->load($id)) {
      return $transformer;
    }
  }

}
