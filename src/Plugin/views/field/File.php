<?php

namespace Drupal\document_ocr\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Component\Render\FormattableMarkup;

/**
 * A handler to provide a field that displays imported file field.
 *
 * @ViewsField("document_ocr_task_file")
 */
class File extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if ($task = $values->_entity) {
      if ($file = $task->getFile()) {
        return new FormattableMarkup('@name <span class="form-item__description">@description</span>', [
          '@name' => $file->getFilename(),
          '@description' => $file->getMimeType(),
        ]);
      }
      $file_details = $task->getFileDetails();
      return new FormattableMarkup('@name <span class="form-item__description">(deleted)</span>', [
        '@name' => $file_details['filename'],
      ]);
    }
  }

}
