<?php

namespace Drupal\document_ocr\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A handler to provide a field that displays imported destination field.
 *
 * @ViewsField("document_ocr_task_destination")
 */
class Destination extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if ($task = $values->_entity) {
      if ($destination = $task->getDestinationEntity()) {
        return $destination->toLink()->toString();
      }
      return $this->t('Pending');
    }
  }

}
