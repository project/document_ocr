<?php

namespace Drupal\document_ocr\Plugin\document_ocr\transformer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\document_ocr\Plugin\TransformerBase;

/**
 * Pipeline transformer plugin.
 *
 * @DocumentOcrTransformer(
 *   id = "pipeline",
 *   name = @Translation("Pipeline"),
 *   group = @Translation("General"),
 *   description = @Translation("Pipeline various transformer plugins."),
 * )
 */
class Pipeline extends TransformerBase {

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $group_class = 'pipeline-items-order-weight';
    $form['description'] = [
      '#markup' => $this->t('Enable transformers and change the order of execution (top to bottom).'),
    ];
    $form['pipeline'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'), $this->t('Enabled'), $this->t('Weight'),
      ],
      '#tabledrag' => [['action' => 'order', 'relationship' => 'sibling', 'group' => $group_class]],
      '#empty' => $this->t('Please add more transformers so you can pipline them here.'),
    ];

    $pipeline = !empty($this->configuration['pipeline']) ? $this->configuration['pipeline'] : [];
    $transformers = [];
    foreach ($pipeline as $id => $details) {
      if ($transformer = $this->getTransformer($id)) {
        $transformers[$id] = $details;
      }
    }
    foreach ($this->getTransformers([$this->getPluginId()]) as $id => $label) {
      if (!in_array($id, array_keys($transformers))) {
        $transformers[$id] = [
          'enabled' => FALSE,
          'weight' => 0,
        ];
      }
    }

    $weight = 0;
    foreach ($transformers as $id => $label) {
      if ($transformer = $this->getTransformer($id)) {
        $form['pipeline'][$id]['#attributes']['class'][] = 'draggable';
        $form['pipeline'][$id]['#weight'] = !empty($pipeline[$id]['weight']) ? $pipeline[$id]['weight'] : $weight;
        $form['pipeline'][$id]['name'] = [
          '#markup' => $transformer->label() . ' <div class="form-item__description">' . $transformer->getDescription() . '</div>',
        ];
        $form['pipeline'][$id]['enabled'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Enabled'),
          '#title_display' => 'invisible',
          '#default_value' => !empty($pipeline[$id]['enabled']),
        ];
        $form['pipeline'][$id]['weight'] = [
          '#type' => 'weight',
          '#title' => $this->t('Weight'),
          '#title_display' => 'invisible',
          '#default_value' => !empty($pipeline[$id]['weight']) ? $pipeline[$id]['weight'] : $weight,
          '#attributes' => ['class' => [$group_class]],
        ];
        $weight++;
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    $pipeline = [];
    foreach ($form_state->getValue('pipeline') as $id => $item) {
      $pipeline[$id] = [
        'enabled' => (bool) $item['enabled'],
        'weight' => (int) $item['weight'],
      ];
    }
    return ['pipeline' => $pipeline];
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value) {
    if (!empty($this->configuration['pipeline'])) {
      foreach ($this->configuration['pipeline'] as $id => $item) {
        if (!empty($item['enabled'])) {
          try {
            /** @var \Drupal\document_ocr\Entity\Transformer $transformer */
            $transformer = $this->getTransformer($id);
            $value = $transformer->getTransformerPlugin()
              ->setCredentials($transformer->getCredentials())
              ->setConfiguration($transformer->getConfiguration())
              ->setMappingData($this->getMappingData())
              ->transform($value);
          }
          catch (\Exception $ex) {
            $this->loggerFactory->error($ex->getMessage());
          }
        }
      }
    }
    return $value;
  }

}
