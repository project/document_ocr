<?php

namespace Drupal\document_ocr\Plugin\document_ocr\transformer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\document_ocr\Plugin\TransformerBase;

/**
 * Encoding converter transformer plugin.
 *
 * @DocumentOcrTransformer(
 *   id = "encoding_converter",
 *   name = @Translation("Encoding Coverter"),
 *   group = @Translation("General"),
 *   description = @Translation("Convert text from one character encoding to another."),
 * )
 */
class EncodingConverter extends TransformerBase {

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $form['to_encoding'] = [
      '#type' => 'textfield',
      '#title' => $this->t('To Encoding'),
      '#description' => $this->t('The desired encoding of the result (single value only).'),
      '#default_value' => !empty($this->configuration['to_encoding']) ? $this->configuration['to_encoding'] : 'UTF-8',
      '#required' => TRUE,
    ];
    $form['from_encoding'] = [
      '#type' => 'textfield',
      '#title' => $this->t('From Encoding'),
      '#description' => $this->t('The current encoding used to interpret text. Multiple encodings may be specified as comma separated list.'),
      '#default_value' => !empty($this->configuration['from_encoding']) ? $this->configuration['from_encoding'] : '',
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    return [
      'to_encoding' => $form_state->getValue('to_encoding'),
      'from_encoding' => $form_state->getValue('from_encoding'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value) {
    if (!empty($this->configuration['to_encoding']) && !empty($this->configuration['from_encoding'])) {
      return mb_convert_encoding($value, $this->configuration['to_encoding'], $this->configuration['from_encoding']);
    }
    return $value;
  }

}
