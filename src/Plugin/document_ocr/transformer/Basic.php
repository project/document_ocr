<?php

namespace Drupal\document_ocr\Plugin\document_ocr\transformer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\document_ocr\Plugin\TransformerBase;

/**
 * Basic transformer plugin.
 *
 * @DocumentOcrTransformer(
 *   id = "basic",
 *   name = @Translation("Basic"),
 *   group = @Translation("General"),
 *   description = @Translation("Basic transformer to process document property before passing it to the entity field."),
 * )
 */
class Basic extends TransformerBase {

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $form['trim'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove whitespace from the beginning and end of a string'),
      '#default_value' => !empty($this->configuration['trim']) ? $this->configuration['trim'] : FALSE,
    ];
    $form['manipulation'] = [
      '#type' => 'radios',
      '#title' => $this->t(' String Manipulation'),
      '#options' => [
        'strtoupper' => $this->t('Make a string uppercase'),
        'strtolower' => $this->t('Make a string lowercase'),
        'ucfirst' => $this->t('Make a string\'s first character uppercase'),
        'ucwords' => $this->t('Uppercase the first character of each word in a string'),
        'preg_replace' => $this->t('Perform a regular expression search and replace'),
        'str_replace' => $this->t('Replace all occurrences of the search string with the replacement string'),
        '' => $this->t('None'),
      ],
      '#default_value' => !empty($this->configuration['manipulation']) ? $this->configuration['manipulation'] : '',
    ];
    $form['regex'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['document-ocr-inline-elements']],
      '#states' => [
        'visible' => [
          ':input[name="manipulation"]' => ['value' => 'preg_replace'],
        ],
      ],
    ];
    $form['regex']['regex_pattern'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The pattern to search'),
      '#title_display' => 'invisible',
      '#placeholder' => $this->t('The pattern to search'),
      '#description' => $this->t('Regular expression'),
      '#default_value' => !empty($this->configuration['regex_pattern']) ? $this->configuration['regex_pattern'] : FALSE,
      '#prefix' => '<div class="document-ocr-inline-item two-textfield">',
      '#suffix' => '</div>',
    ];
    $form['regex']['regex_replace'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The string to replace'),
      '#title_display' => 'invisible',
      '#placeholder' => $this->t('Replacement string'),
      '#description' => $this->t('The replacement value that replaces the match.'),
      '#default_value' => !empty($this->configuration['regex_replace']) ? $this->configuration['regex_replace'] : FALSE,
      '#prefix' => '<div class="document-ocr-inline-item two-textfield">',
      '#suffix' => '</div>',
    ];
    $form['str'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['document-ocr-inline-elements']],
      '#states' => [
        'visible' => [
          ':input[name="manipulation"]' => ['value' => 'str_replace'],
        ],
      ],
    ];
    $form['str']['str_search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The pattern to search'),
      '#title_display' => 'invisible',
      '#placeholder' => $this->t('String to replace'),
      '#description' => $this->t('The value being searched for, otherwise known as the needle.'),
      '#default_value' => !empty($this->configuration['str_search']) ? $this->configuration['str_search'] : FALSE,
      '#prefix' => '<div class="document-ocr-inline-item two-textfield">',
      '#suffix' => '</div>',
    ];
    $form['str']['str_replace'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The string to replace'),
      '#title_display' => 'invisible',
      '#placeholder' => $this->t('Replacement string'),
      '#description' => $this->t('The replacement value that replaces found search value.'),
      '#default_value' => !empty($this->configuration['str_replace']) ? $this->configuration['str_replace'] : FALSE,
      '#prefix' => '<div class="document-ocr-inline-item two-textfield">',
      '#suffix' => '</div>',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    return [
      'trim' => $form_state->getValue('trim'),
      'manipulation' => $form_state->getValue('manipulation'),
      'regex_pattern' => $form_state->getValue('regex_pattern'),
      'regex_replace' => $form_state->getValue('regex_replace'),
      'str_search' => $form_state->getValue('str_search'),
      'str_replace' => $form_state->getValue('str_replace'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value) {
    if (!empty($this->configuration['trim'])) {
      $value = trim($value);
    }
    if ($manipulation = $this->configuration['manipulation']) {
      switch ($manipulation) {
        case 'preg_replace':
          $value = preg_replace($this->configuration['regex_pattern'], $this->configuration['regex_replace'], $value);
          break;
        case 'str_replace':
          $value = str_replace($this->configuration['str_search'], $this->configuration['str_replace'], $value);
          break;
        default:
          return call_user_func_array($manipulation, [$value]);
          break;
      }
    }
    return $value;
  }

}
