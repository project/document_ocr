<?php

namespace Drupal\document_ocr\Plugin\document_ocr\transformer;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\FormStateInterface;
use Drupal\document_ocr\Plugin\TransformerBase;

/**
 * Truncate transformer plugin.
 *
 * @DocumentOcrTransformer(
 *   id = "truncate",
 *   name = @Translation("Truncate"),
 *   group = @Translation("General"),
 *   description = @Translation("Truncates a UTF-8-encoded string safely to a number of characters."),
 * )
 */
class Truncate extends TransformerBase {

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $form['max_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Max Length'),
      '#description' => $this->t('An upper limit on the returned string length, including trailing ellipsis if <em>Add Ellipsis</em> is checked'),
      '#default_value' => !empty($this->configuration['max_length']) ? $this->configuration['max_length'] : '',
      '#required' => TRUE,
    ];
    $form['wordsafe'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Wordsafe'),
      '#description' => $this->t('If checked, attempt to truncate on a word boundary. Word boundaries are spaces, punctuation, and Unicode characters used as word boundaries in non-Latin languages.  If a word boundary cannot be found that would make the length of the returned string fall within length guidelines (see parameters <em>Max Length</em> and <em>Min Wordsafe Length</em>), word boundaries are ignored.'),
      '#default_value' => !empty($this->configuration['wordsafe']) ? $this->configuration['wordsafe'] : FALSE,
    ];
    $form['add_ellipsis'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add Ellipsis'),
      '#description' => $this->t('If checked, add \'...\' to the end of the truncated string. The string length will still fall within <em>Max Length</em>'),
      '#default_value' => !empty($this->configuration['add_ellipsis']) ? $this->configuration['add_ellipsis'] : FALSE,
    ];
    $form['min_wordsafe_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Min Wordsafe Length'),
      '#description' => $this->t('If <em>Wordsafe</em> is checked, the minimum acceptable length for truncation (before adding an ellipsis, if <em>Add Ellipsis</em> is checked). This can be used to prevent having a very short resulting string that will not be understandable.'),
      '#default_value' => !empty($this->configuration['min_wordsafe_length']) ? $this->configuration['min_wordsafe_length'] : 1,
      '#states' => [
        'visible' => [
          ':input[name="wordsafe"]' => ['checked' => TRUE],
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    return [
      'max_length' => (int) $form_state->getValue('max_length'),
      'wordsafe' => (bool) $form_state->getValue('wordsafe'),
      'add_ellipsis' => (bool) $form_state->getValue('add_ellipsis'),
      'min_wordsafe_length' => (int) $form_state->getValue('min_wordsafe_length'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value) {
    return Unicode::truncate($value, $this->configuration['max_length'], $this->configuration['wordsafe'], $this->configuration['add_ellipsis'], $this->configuration['min_wordsafe_length']);
  }

}
