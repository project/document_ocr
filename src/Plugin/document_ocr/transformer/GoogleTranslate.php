<?php

namespace Drupal\document_ocr\Plugin\document_ocr\transformer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\document_ocr\Plugin\TransformerBase;
use Drupal\document_ocr\Services\GoogleTranslate as GoogleTranslateService;

/**
 * Google Translate integration transformer plugin.
 *
 * @DocumentOcrTransformer(
 *   id = "google_translate",
 *   name = @Translation("Google Translate"),
 *   group = @Translation("Google Cloud"),
 *   description = @Translation("Google Translate integration transformer."),
 *   requires = {
 *     "credentials"
 *   }
 * )
 */
class GoogleTranslate extends TransformerBase {

  /**
   * Google Translate serivce.
   *
   * @var \Drupal\document_ocr\Services\GoogleTranslate as GoogleTranslateService
   */
  protected $service;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, LoggerChannelFactoryInterface $logger_factory, GoogleTranslateService $google_translate) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $file_system, $logger_factory);
    $this->service = $google_translate;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('logger.factory'),
      $container->get('document_ocr.google_translate')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function requirementsAreMet() {
    return class_exists('\Google\Cloud\Translate\V3\TranslationServiceClient');
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $credentials = $this->getCredentials();
    $this->service->setCredentials($credentials);
    $form['project_id'] = [
      '#type' => 'item',
      '#title' => $this->t('Project ID'),
      '#markup' => '<em>' . $credentials['project_id'] . '</em>',
    ];
    $form['language_from'] = [
      '#type' => 'select',
      '#title' => $this->t('Translate from Language'),
      '#description' => $this->t('If not selected the source language will be autodetected (when possible).'),
      '#options' => $this->service->getSupportedLanguages(),
      '#empty_option' => $this->t('- Automatic Detection  -'),
      '#default_value' => !empty($this->configuration['language_from']) ? $this->configuration['language_from'] : '',
    ];
    $form['language_to'] = [
      '#type' => 'select',
      '#title' => $this->t('Translate to Language'),
      '#description' => $this->t('Google Translate supported languages. <a href="@link" target="_blank">Learn more</a>.', ['@link' => 'https://cloud.google.com/translate/docs/languages']),
      '#options' => $this->service->getSupportedLanguages(),
      '#default_value' => !empty($this->configuration['language_to']) ? $this->configuration['language_to'] : '',
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    return [
      'language_from' => $form_state->getValue('language_from'),
      'language_to' => $form_state->getValue('language_to'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value) {
    try {
      $translated_text = [];
      $response = $this->service->setCredentials($this->getCredentials())
        ->translate($value, $this->configuration['language_from'], $this->configuration['language_to']);
      foreach ($response->getTranslations() as $translation) {
        $translated_text[] = $translation->getTranslatedText();
      }
      return join('', $translated_text);
    }
    catch (\Exception $ex) {
      return $value;
    }
  }

}
