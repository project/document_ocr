<?php

namespace Drupal\document_ocr\Plugin\document_ocr\transformer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\FileRepository;
use Drupal\document_ocr\Plugin\TransformerBase;
use Drupal\document_ocr\Services\GoogleTextToSpeech as GoogleTextToSpeechService;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * Google Text to Speech integration transformer plugin.
 *
 * @DocumentOcrTransformer(
 *   id = "google_text2speech",
 *   name = @Translation("Google Text to Speech"),
 *   group = @Translation("Google Cloud"),
 *   description = @Translation("Google Text to Speech integration transformer."),
 *   requires = {
 *     "credentials"
 *   },
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class GoogleTextToSpeech extends TransformerBase {

  /**
   * Google Text to Speech serivce.
   *
   * @var \Drupal\document_ocr\Services\GoogleTextToSpeech as GoogleTextToSpeechService
   */
  protected $service;

  /**
   * File repositoy serivce.
   *
   * @var \Drupal\file\FileRepository
   */
  protected $fileRepository;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, LoggerChannelFactoryInterface $logger_factory, FileRepository $file_repository, GoogleTextToSpeechService $google_text2speech) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $file_system, $logger_factory);
    $this->service = $google_text2speech;
    $this->fileRepository = $file_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('logger.factory'),
      $container->get('file.repository'),
      $container->get('document_ocr.google_text2speech')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function requirementsAreMet() {
    return class_exists('\Google\Cloud\TextToSpeech\V1\TextToSpeechClient');
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $credentials = $this->getCredentials();
    $this->service->setCredentials($credentials);
    $form['project_id'] = [
      '#type' => 'item',
      '#title' => $this->t('Project ID'),
      '#markup' => '<em>' . $credentials['project_id'] . '</em>',
    ];

    $languages = $this->service->getVoices();
    $language_options = static::getLanguagesOptions($languages);
    $selected_language = '';
    if (empty($form_state->getValue('language'))) {
      if (!empty($this->configuration['language'])) {
        $selected_language = $this->configuration['language'];
      }
    }
    else {
      $selected_language = $form_state->getValue('language');
    }

    $form['language'] = [
      '#type' => 'select',
      '#title' => $this->t('Language/Locale'),
      '#description' => $this->t('<a href="@link" target="_blank">Learn more</a> about supported languages.', ['@link' => 'https://cloud.google.com/text-to-speech/docs/voices']),
      '#empty_option' => $this->t('- Select Language -'),
      '#options' => $language_options,
      '#default_value' => $selected_language,
      '#ajax' => [
        'callback' => [$this, 'voiceNamesCallback'],
        'wrapper' => 'voice-names-wrapper',
      ],
      '#required' => TRUE,
    ];

    $form['voice_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Voice Name'),
      '#empty_option' => $this->t('- Select Voice -'),
      '#options' => static::getVoiceNames($languages, $selected_language),
      '#default_value' => !empty($form_state->getValue('voice_name')) ? $form_state->getValue('voice_name')
        : (!empty($this->configuration['voice_name']) ? $this->configuration['voice_name'] : ''),
      '#validated' => TRUE,
      '#prefix' => '<div id="voice-names-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['speed'] = [
      '#type' => 'number',
      '#title' => $this->t('Speed'),
      '#min' => 0.25,
      '#max' => 4,
      '#step' => 0.01,
      '#default_value' => !empty($this->configuration['speed']) ? $this->configuration['speed'] : 1.00,
    ];

    $form['pitch'] = [
      '#type' => 'number',
      '#title' => $this->t('Pitch'),
      '#min' => -20,
      '#max' => 20,
      '#step' => 1,
      '#default_value' => !empty($this->configuration['pitch']) ? $this->configuration['pitch'] : 0,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    return [
      'language' => $form_state->getValue('language'),
      'voice_name' => $form_state->getValue('voice_name'),
      'speed' => (float) $form_state->getValue('speed'),
      'pitch' => (int) $form_state->getValue('pitch'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value) {
    try {
      $path = DocumentOcrInterface::TEXT_TO_SPEECH_URI;
      /** @var \Google\Cloud\TextToSpeech\V1\SynthesizeSpeechResponse $response */
      $response = $this->service
        ->setCredentials($this->getCredentials())
        ->convert($value, $this->configuration);
      $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
      $file = $this->fileRepository->writeData($response->getAudioContent(), $path . '/' . uniqid() . '.mp3');
      $file->setTemporary();
      $file->save();
      return ['target_id' => $file->id()];
    }
    catch (\Exception $ex) {
      return $value;
    }
  }

  /**
   * Voice Name field callback.
   */
  public function voiceNamesCallback(array $form, FormStateInterface $form_state) {
    return $form['voice_name'];
  }

  /**
   * Get voice names for the selected languages.
   */
  protected static function getVoiceNames($languages, $langcode) {
    $options = [];
    if (!empty($languages[$langcode]['voices'])) {
      foreach ($languages[$langcode]['voices'] as $voice_type => $voices) {
        foreach ($voices as $details) {
          $options[$details['name']] = str_replace($langcode . '-', '', $details['name']) . ' (' . strtolower($details['ssml_gender']) . ')';
        }
      }
    }
    return $options;
  }

  /**
   * Get languages list.
   */
  protected static function getLanguagesOptions($languages) {
    $options = [];
    foreach ($languages as $langcode => $details) {
      $options[$langcode] = $details['name'];
    }
    return $options;
  }

}
