<?php

namespace Drupal\document_ocr\Plugin\document_ocr\transformer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\document_ocr\Plugin\TransformerBase;
use Drupal\document_ocr\Services\MicrosoftTranslate as MicrosoftTranslateService;

/**
 * Microsoft Translate integration transformer plugin.
 *
 * @DocumentOcrTransformer(
 *   id = "microsoft_translate",
 *   name = @Translation("Microsoft Translate"),
 *   group = @Translation("Microsoft Azure"),
 *   description = @Translation("Microsoft Translate integration transformer."),
 *   requires = {
 *     "credentials"
 *   }
 * )
 */
class MicrosoftTranslate extends TransformerBase {

  /**
   * Microsoft Translate serivce.
   *
   * @var \Drupal\document_ocr\Services\MicrosoftTranslate as MicrosoftTranslateService
   */
  protected $service;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, LoggerChannelFactoryInterface $logger_factory, MicrosoftTranslateService $microsoft_translate) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $file_system, $logger_factory);
    $this->service = $microsoft_translate;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('logger.factory'),
      $container->get('document_ocr.microsoft_translate')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function requirementsAreMet() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $credentials = $this->getCredentials();
    $this->service->setCredentials($credentials);
    $languages = [];
    $supported_languages = $this->service->getSupportedLanguages();
    if (!empty($supported_languages['translation'])) {
      foreach ($supported_languages['translation'] as $langcode => $language) {
        $languages[$langcode] = $language['name'] . ' (' . $language['nativeName'] . ')';
      }
    }
    $form['language_from'] = [
      '#type' => 'select',
      '#title' => $this->t('Translate from Language'),
      '#description' => $this->t('If not selected the source language will be autodetected (when possible).'),
      '#options' => $languages,
      '#empty_option' => $this->t('- Automatic Detection  -'),
      '#default_value' => !empty($this->configuration['language_from']) ? $this->configuration['language_from'] : '',
    ];
    $form['language_to'] = [
      '#type' => 'select',
      '#title' => $this->t('Translate to Language'),
      '#description' => $this->t('Microsoft Translate supported languages. <a href="@link" target="_blank">Learn more</a>.', ['@link' => 'https://learn.microsoft.com/en-us/azure/cognitive-services/translator/language-support']),
      '#options' => $languages,
      '#default_value' => !empty($this->configuration['language_to']) ? $this->configuration['language_to'] : '',
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    return [
      'language_from' => $form_state->getValue('language_from'),
      'language_to' => $form_state->getValue('language_to'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value) {
    try {
      $translated_text = [];
      $response = $this->service->setCredentials($this->getCredentials())
        ->translate($value, $this->configuration['language_from'], $this->configuration['language_to']);
      if (!empty($response[0]['translations'])) {
        foreach ($response[0]['translations'] as $translation) {
          $translated_text[] = $translation['text'];
        }
      }
      return join('', $translated_text);
    }
    catch (\Exception $ex) {
      return $value;
    }
  }

}
