<?php

namespace Drupal\document_ocr\Plugin\document_ocr\transformer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Unicode;
use Drupal\document_ocr\Plugin\TransformerBase;
use Drupal\document_ocr\Services\OpenAi as OpenAiService;
use Drupal\document_ocr\Repository\OpenAiModelsRepository;

/**
 * Open AI Chat integration transformer plugin.
 *
 * @DocumentOcrTransformer(
 *   id = "openai_chat",
 *   name = @Translation("OpenAI Chat"),
 *   group = @Translation("OpenAI"),
 *   description = @Translation("Given a prompt, the model will return generated natural language or code."),
 *   requires = {
 *     "credentials"
 *   }
 * )
 */
class OpenAiChat extends TransformerBase {

  /**
   * OpenAI serivce.
   *
   * @var \Drupal\document_ocr\Services\OpenAi as OpenAiService
   */
  protected $service;

  /**
   * OpenAI Models Repository.
   *
   * @var \Drupal\document_ocr\Repository\OpenAiModelsRepository
   */
  protected $modelsRepository;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, LoggerChannelFactoryInterface $logger_factory, OpenAiService $openai, OpenAiModelsRepository $models_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $file_system, $logger_factory);
    $this->service = $openai;
    $this->modelsRepository = $models_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('logger.factory'),
      $container->get('document_ocr.openai'),
      $container->get('document_ocr.openai_models_repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $form['system_prompt'] = [
      '#type' => 'textarea',
      '#title' => $this->t('System Prompt'),
      '#description' => $this->t('Tell OpenAI what to do. Example: <em>Summarize the following text</em> and provide the Text with the text that needs a summary.'),
      '#placeholder' => $this->t('Example: Summarize the following text'),
      '#default_value' => !empty($this->configuration['system_prompt']) ? $this->configuration['system_prompt'] : '',
      '#rows' => 2,
      '#required' => TRUE,
    ];
    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text'),
      '#description' => $this->t('Use <strong>%text%</strong> for the document content, or use <strong>%text:600%</strong> to limit text to 600 characters'),
      '#default_value' => !empty($this->configuration['text']) ? $this->configuration['text'] : '%text%',
      '#placeholder' => $this->t('%text%'),
      '#rows' => 2,
      '#required' => TRUE,
    ];
    $form['model'] = [
      '#type' => 'select',
      '#title' => $this->t('Model'),
      '#description' => $this->t('The model which will generate the completion. Some models are suitable for natural language tasks, others specialize in code. <a href="@link" target="_blank">Learn more</a>.', ['@link' => 'https://platform.openai.com/docs/models']),
      '#options' => $this->modelsRepository->getModels('gpt', $this->service->setCredentials($this->getCredentials())),
      '#default_value' => !empty($this->configuration['model']) ? $this->configuration['model'] : 'gpt-3.5-turbo',
      '#required' => TRUE,
    ];
    $form['temperature'] = [
      '#type' => 'number',
      '#title' => $this->t('Temperature'),
      '#description' => $this->t('What sampling temperature to use, between 0 and 2. Higher values like 0.8 will make the output more random, while lower values like 0.2 will make it more focused and deterministic. Generally recommend altering this or <em>Top P</em> but not both.'),
      '#min' => 0,
      '#max' => 2,
      '#step' => 0.01,
      '#default_value' => !empty($this->configuration['temperature']) ? $this->configuration['temperature'] : 0.7,
    ];
    $form['max_tokens'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum length'),
      '#description' => $this->t('The maximum number of tokens to generate in the chat completion. (One token is roughly 4 characters for normal English text)'),
      '#min' => 1,
      '#max' => 32768,
      '#default_value' => !empty($this->configuration['max_tokens']) ? $this->configuration['max_tokens'] : 2000,
    ];
    $form['top_p'] = [
      '#type' => 'number',
      '#title' => $this->t('Top P'),
      '#description' => $this->t('Controls diversity via nucleus sampling: 0.5 means half of all likelihood-weighted options are considered.'),
      '#min' => 0,
      '#max' => 1,
      '#step' => 0.01,
      '#default_value' => !empty($this->configuration['top_p']) ? $this->configuration['top_p'] : 1.0,
    ];
    $form['frequency_penalty'] = [
      '#type' => 'number',
      '#title' => $this->t('Frequency penalty'),
      '#description' => $this->t("How much to penalize new tokens based on their existing frequency in the text so far. Decreases the model's likelihood to repeat the same line verbatim."),
      '#min' => 0,
      '#max' => 2,
      '#step' => 0.01,
      '#default_value' => !empty($this->configuration['frequency_penalty']) ? $this->configuration['frequency_penalty'] : 0.0,
    ];
    $form['presence_penalty'] = [
      '#type' => 'number',
      '#title' => $this->t('Presence penalty'),
      '#description' => $this->t("How much to penalize new tokens based on whether they appear in the text so far. Increases the model's likelihood to talk about new topics."),
      '#min' => 0,
      '#max' => 2,
      '#step' => 0.01,
      '#default_value' => !empty($this->configuration['presence_penalty']) ? $this->configuration['presence_penalty'] : 1,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    return [
      'system_prompt' => $form_state->getValue('system_prompt'),
      'text' => $form_state->getValue('text'),
      'model' => (string) $form_state->getValue('model'),
      'temperature' => (float) $form_state->getValue('temperature'),
      'max_tokens' => (int) $form_state->getValue('max_tokens'),
      'top_p' => (float) $form_state->getValue('top_p'),
      'frequency_penalty' => (float) $form_state->getValue('frequency_penalty'),
      'presence_penalty' => (float) $form_state->getValue('presence_penalty'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value) {
    try {
      $client = $this->service->setCredentials($this->getCredentials());
      if (preg_match('/%text:(.*)%/', $this->configuration['text'], $match)) {
        if (!empty($match[1])) {
          $this->configuration['text'] = str_replace($match[0], Unicode::truncate($value, (int) $match[1], TRUE), $this->configuration['text']);
        }
      }
      else {
        $this->configuration['text'] = str_replace('%text%', $value, $this->configuration['text']);
      }
      $this->configuration['messages'] = [
        [
          'role' => 'system',
          'content' => $this->configuration['system_prompt']
        ],
        [
          'role' => 'user',
          'content' => $this->configuration['text']
        ],
      ];

      unset($this->configuration['system_prompt'], $this->configuration['text']);
      if ($response = $client->chatRequest($this->configuration)) {
        if (!empty($response->choices[0])) {
          $choice = $response->choices[0]->toArray();
          return !empty($choice['message']['content']) ? $choice['message']['content'] : '';
        }
      }
    }
    catch (\Exception $ex) {
      return $value;
    }
  }

}
