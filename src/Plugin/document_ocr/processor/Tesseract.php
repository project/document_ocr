<?php

namespace Drupal\document_ocr\Plugin\document_ocr\processor;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\document_ocr\Traits\UploadPathTrait;
use thiagoalessio\TesseractOCR\TesseractOCR;
use Drupal\document_ocr\Plugin\ProcessorBase;
use Drupal\file\Entity\File;

/**
 * Tesseract Ocr processor plugin.
 *
 * @DocumentOcrProcessor(
 *   id = "tesseract_ocr",
 *   name = @Translation("Tesseract OCR"),
 *   group = @Translation("Shell Commands"),
 *   extensions = "jpg jpeg png",
 * )
 */
class Tesseract extends ProcessorBase {

  use DependencySerializationTrait;
  use UploadPathTrait;

  /**
   * {@inheritdoc}
   */
  public function requirementsAreMet() {
    return class_exists('\thiagoalessio\TesseractOCR\TesseractOCR');
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    // Disable cache to avoid errors with storing files in tempstore
    $form_state->disableCache();
    $form['user_patterns'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('User Patterns'),
      '#upload_location' => $this->getUploadPath(),
      '#description' => $this->t('If the contents you are dealing with have known patterns, this option can help a lot tesseract\'s recognition accuracy.'),
    ];
    $form['user_words'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('User Words'),
      '#upload_location' => $this->getUploadPath(),
      '#description' => $this->t('This is a plain text file containing a list of words that you want to be considered as a normal dictionary words by tesseract.'),
    ];
    if (!empty($this->configuration['user_patterns'])) {
      $form['user_patterns']['#default_value'] = [$this->configuration['user_patterns']];
    }
    if (!empty($this->configuration['user_words'])) {
      $form['user_words']['#default_value'] = [$this->configuration['user_words']];
    }
    $languages = [];
    $languages[''] = '-';
    $languages = array_merge($languages, (new TesseractOCR())->availableLanguages());
    $form['lang'] = [
      '#type' => 'select',
      '#title' => $this->t('Language'),
      '#options' => array_combine($languages, $languages),
      '#description' => $this->t('Define language to be used during the recognition.'),
      '#default_value' => !empty($this->configuration['lang']) ? $this->configuration['lang'] : '',
    ];
    $form['psm'] = [
      '#type' => 'number',
      '#title' => $this->t('PSM'),
      '#description' => $this->t('Specify the Page Segmentation Method, which instructs tesseract how to interpret the given image.'),
      '#default_value' => !empty($this->configuration['psm']) ? (int) $this->configuration['psm'] : 6,
    ];
    $form['oem'] = [
      '#type' => 'number',
      '#title' => $this->t('OEM'),
      '#description' => $this->t('Specify the OCR Engine Mode. See <a href="https://tesseract-ocr.github.io/" target="_blank">documentation</a> for more details.'),
      '#default_value' => !empty($this->configuration['oem']) ? (int) $this->configuration['oem'] : 2,
    ];
    $form['dpi'] = [
      '#type' => 'number',
      '#title' => $this->t('DPI'),
      '#description' => $this->t('Specify the image DPI. It is useful if your image does not contain this information in its metadata.'),
      '#default_value' => !empty($this->configuration['dpi']) ? (int) $this->configuration['dpi'] : 300,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    $user_patterns = NULL;
    $user_words = NULL;
    $user_patterns_file = $form_state->getValue('user_patterns', 0);
    if (!empty($user_patterns_file[0])) {
      if ($file = File::load($user_patterns_file[0])) {
        $user_patterns = $file->id();
        $file->setPermanent(TRUE);
        $file->save();
      }
    }
    $user_words_file = $form_state->getValue('user_words', 0);
    if (!empty($user_words_file[0])) {
      if ($file = File::load($user_words_file[0])) {
        $user_words = $file->id();
        $file->setPermanent(TRUE);
        $file->save();
      }
    }
    return [
      'user_patterns' => $user_patterns,
      'user_words' => $user_words,
      'lang' => $form_state->getValue('lang'),
      'psm' => (int) $form_state->getValue('psm'),
      'oem' => (int) $form_state->getValue('oem'),
      'dpi' => (int) $form_state->getValue('dpi'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingData() {
    try {
      if ($file = $this->getFile()) {
        $tesseract = new TesseractOCR($this->fileSystem->realpath($file->getFileUri()));
        if (!empty($this->configuration['user_patterns'])) {
          if ($file = File::load($this->configuration['user_patterns'])) {
            $tesseract->userPatterns($this->fileSystem->realpath($file->getFileUri()));
          }
        }
        if (!empty($this->configuration['user_words'])) {
          if ($file = File::load($this->configuration['user_words'])) {
            $tesseract->userWords($this->fileSystem->realpath($file->getFileUri()));
          }
        }
        if (!empty($this->configuration['lang']) && $this->configuration['lang'] != '-') {
          $tesseract->lang($this->configuration['lang']);
        }
        if (!empty($this->configuration['psm'])) {
          $tesseract->psm($this->configuration['psm']);
        }
        if (!empty($this->configuration['oem'])) {
          $tesseract->oem($this->configuration['oem']);
        }
        if (!empty($this->configuration['dpi'])) {
          $tesseract->dpi($this->configuration['dpi']);
        }
        $contents = $tesseract->run();
        $this->setOption('extracted_text', $this->t('Extracted Text'));
        $this->setOptionValue('extracted_text', $contents);
        $this->setDocument($tesseract);
      }
      else {
        $this->setOption('extracted_text', $this->t('Extracted Text'));
        $this->setOptionValue('extracted_text', '');
      }
      return $this;
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
      return FALSE;
    }
  }

}
