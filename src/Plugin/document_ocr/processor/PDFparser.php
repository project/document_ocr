<?php

namespace Drupal\document_ocr\Plugin\document_ocr\processor;

use Drupal\Core\Form\FormStateInterface;
use Smalot\PdfParser\Parser;
use Drupal\document_ocr\Plugin\ProcessorBase;

/**
 * PDFparser processor plugin.
 *
 * @DocumentOcrProcessor(
 *   id = "pdf_parser",
 *   name = @Translation("PDF parser"),
 *   extensions = "pdf",
 *   requires = {
 *     "template"
 *   }
 * )
 */
class PDFparser extends ProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function requirementsAreMet() {
    return class_exists('\Smalot\PdfParser\Parser');
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $form['decode_memory_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Decode Memory Limit'),
      '#description' => $this->t('If parsing fails because of memory exhaustion, you can set a lower memory limit for decoding operations.'),
      '#default_value' => !empty($this->configuration['decode_memory_limit']) ? $this->configuration['decode_memory_limit'] : 0,
    ];
    $form['font_space_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Font Space Limit'),
      '#description' => $this->t('Changing font space limit can be helpful if text contains too many spaces.'),
      '#default_value' => !empty($this->configuration['font_space_limit']) ? $this->configuration['font_space_limit'] : -50,
    ];
    $form['horizontal_offset'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Horizontal Offset'),
      '#description' => $this->t('When words are broken up or when the structure of a table is not preserved, you may get better results when adapting this option. An empty string can prevent words from breaking up. A tab can help preserve the structure of your document.'),
      '#default_value' => !empty($this->configuration['horizontal_offset']) ? $this->configuration['horizontal_offset'] : '\t',
    ];
    $form['pdf_whitespaces'] = [
      '#type' => 'textfield',
      '#title' => $this->t('PDF Whitespaces'),
      '#description' => $this->t('Set PDF whitespaces.'),
      '#default_value' => !empty($this->configuration['pdf_whitespaces']) ? $this->configuration['pdf_whitespaces'] : '\0\t\n\f\r',
    ];
    $form['pdf_whitespaces_regex'] = [
      '#type' => 'textfield',
      '#title' => $this->t('PDF Whitespaces (regex)'),
      '#description' => $this->t('Set PDF whitespaces (regex).'),
      '#default_value' => !empty($this->configuration['pdf_whitespaces_regex']) ? $this->configuration['pdf_whitespaces_regex'] : '[\0\t\n\f\r]',
    ];
    $form['retain_image_content'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Retain image content (not recommeded)'),
      '#description' => $this->t('If parsing fails because of memory exhaustion, you can uncheck this option. It wont retain image content anymore, but will use less memory too.'),
      '#default_value' => !empty($this->configuration['retain_image_content']) ? $this->configuration['retain_image_content'] : FALSE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    return [
      'decode_memory_limit' => (int) $form_state->getValue('decode_memory_limit'),
      'font_space_limit' => (int) $form_state->getValue('font_space_limit'),
      'horizontal_offset' => (string) $form_state->getValue('horizontal_offset'),
      'pdf_whitespaces' => (string) $form_state->getValue('pdf_whitespaces'),
      'pdf_whitespaces_regex' => (string) $form_state->getValue('pdf_whitespaces_regex'),
      'retain_image_content' => (bool) $form_state->getValue('retain_image_content'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingData() {
    try {
      if ($file = $this->getFile()) {
        $parser = new Parser($this->configuration);
        $pdf = $parser->parseFile($this->fileSystem->realpath($file->getFileUri()));
        $this->setOption('extracted_text', $this->t('Extracted Text'));
        $this->setOptionValue('extracted_text', $pdf->getText());
        if ($metadata = $pdf->getDetails()) {
          foreach ($metadata as $key => $val) {
            $this->setOption('metadata:' . $key, $this->t('PDF Metadata: @key', ['@key' => $key]));
            $this->setOptionValue('metadata:' . $key, $val);
          }
        }
        $this->setDocument($pdf);
      }
      return $this;
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
      return FALSE;
    }
  }

}
