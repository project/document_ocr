<?php

namespace Drupal\document_ocr\Plugin\document_ocr\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\document_ocr\Plugin\ProcessorBase;
use Drupal\document_ocr\Services\OpenAi as OpenAiService;
use Drupal\document_ocr\Repository\OpenAiModelsRepository;
use Drupal\document_ocr\Repository\LanguagesISO639Repository;

/**
 * OpenAiAudio processor plugin.
 *
 * @DocumentOcrProcessor(
 *   id = "openai_audio",
 *   name = @Translation("OpenAI Audio"),
 *   group = @Translation("OpenAI"),
 *   extensions = "mp3 mp4 mpeg mpga m4a wav webm",
 *   requires = {
 *     "credentials"
 *   },
 *   supports = {
 *     "store_json"
 *   }
 * )
 */
class OpenAiAudio extends ProcessorBase {

  /**
   * OpenAI serivce.
   *
   * @var \Drupal\document_ocr\Services\OpenAi as OpenAiService
   */
  protected $service;

  /**
   * OpenAI Models Repository.
   *
   * @var \Drupal\document_ocr\Repository\OpenAiModelsRepository
   */
  protected $modelsRepository;

  /**
   * Languages Repository.
   *
   * @var \Drupal\document_ocr\Repository\LanguagesISO639Repository
   */
  protected $languagesRepository;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PrivateTempStoreFactory $temp_store_factory, FileSystemInterface $file_system, LoggerChannelFactoryInterface $logger_factory, OpenAiService $openai, OpenAiModelsRepository $models_repository, LanguagesISO639Repository $languages_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $temp_store_factory, $file_system, $logger_factory);
    $this->service = $openai;
    $this->modelsRepository = $models_repository;
    $this->languagesRepository = $languages_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('tempstore.private'),
      $container->get('file_system'),
      $container->get('logger.factory'),
      $container->get('document_ocr.openai'),
      $container->get('document_ocr.openai_models_repository'),
      $container->get('document_ocr.languages_iso639_repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function requirementsAreMet() {
    return class_exists('\OpenAI\Client');
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $form['model'] = [
      '#title' => $this->t('Model'),
      '#type' => 'select',
      '#empty_option' => $this->t('- Select OpenAI model -'),
      '#description' => $this->t('If not selected default model will be used.'),
      '#validated' => TRUE,
      '#options' => $this->modelsRepository->getModels('whisper', $this->service->setCredentials($this->getCredentials())),
      '#default_value' => !empty($this->configuration['model']) ? $this->configuration['model'] : 'whisper-1',
      '#required' => TRUE,
    ];
    $form['task'] = [
      '#type' => 'select',
      '#title' => $this->t('Task'),
      '#options' => [
        'transcribe' => $this->t('Transcribe'),
        'translate' => $this->t('Translate'),
      ],
      '#description' => $this->t('The task to use to process the audio file. "Transcribe": transcribes the audio to the same language as the audio. "Translate": translates and transcribes the audio into English. See the <a href="@link" target="_blank">speech to text guide</a> for further details.', ['@link' => 'https://platform.openai.com/docs/guides/speech-to-text']),
      '#default_value' => !empty($this->configuration['task']) ? $this->configuration['task'] : 'transcribe',
    ];
    $languages = [];
    $repository = $this->languagesRepository->toArray();
    foreach ($repository as $item) {
      $languages[$item['code']] = $item['name'];
    }
    $form['language'] = [
      '#type' => 'select',
      '#title' => $this->t('Language (optional)'),
      '#description' => $this->t('The language of the input audio. Supplying the input language in ISO-639-1 format will improve accuracy and latency.'),
      '#options' => $languages,
      '#default_value' => !empty($this->configuration['language']) ? $this->configuration['language'] : 'en',
      '#states' => [
        'visible' => [
          ':input[name="task"]' => ['value' => 'transcribe'],
        ],
      ],
    ];
    $form['temperature'] = [
      '#type' => 'number',
      '#title' => $this->t('Temperature (optional)'),
      '#description' => $this->t('The sampling temperature, between 0 and 1. Higher values like 0.8 will make the output more random, while lower values like 0.2 will make it more focused and deterministic. If set to 0, the model will use log probability to automatically increase the temperature until certain thresholds are hit.'),
      '#min' => 0,
      '#max' => 1,
      '#step' => 0.1,
      '#default_value' => !empty($this->configuration['temperature']) ? $this->configuration['temperature'] : 0,
    ];
    $form['prompt'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Prompt (optional)'),
      '#description' => $this->t('An optional text to guide the model\'s style or continue a previous audio segment. The prompt for the <strong>transcription</strong> task should match the audio language. The prompt should be in English for <strong>translation</strong> task.'),
      '#default_value' => !empty($this->configuration['prompt']) ? $this->configuration['prompt'] : '',
      '#rows' => 3,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    return [
      'model' => $form_state->getValue('model'),
      'task' => $form_state->getValue('task'),
      'temperature' => (float) $form_state->getValue('temperature'),
      'prompt' => $form_state->getValue('prompt'),
      'language' => $form_state->getValue('language'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingData() {
    try {
      $client = $this->service->setCredentials($this->getCredentials());
      // This integration does not require template file, but we still need to pass properties to the mapping tool.
      $this->setOption('extracted_text', $this->t('Extracted Text'));
      if ($file = $this->getFile()) {
        $response = $client->audio($this->fileSystem->realpath($file->getFileUri()), $this->configuration);
        $document = $response->toArray();
        $this->setOptionValue('extracted_text', $document['text']);
        $this->setDocument($document);
      }
      else {
        $this->setDocument(NULL);
      }
      return $this;
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
      return FALSE;
    }
  }

}
