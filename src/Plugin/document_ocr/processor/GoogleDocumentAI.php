<?php

namespace Drupal\document_ocr\Plugin\document_ocr\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\document_ocr\Plugin\ProcessorBase;
use Drupal\document_ocr\Services\GoogleDocumentAi as GoogleDocumentAiService;

/**
 * GoogleDocumentAI processor plugin.
 *
 * @DocumentOcrProcessor(
 *   id = "google_document_ai",
 *   name = @Translation("Google Document AI"),
 *   group = @Translation("Google Cloud"),
 *   extensions = "pdf gif tiff tif jpg jpeg png bmp webp",
 *   config_dialog_width = 700,
 *   requires = {
 *     "credentials",
 *     "template"
 *   },
 *   supports = {
 *     "asynchronous",
 *     "store_json",
 *   }
 * )
 */
class GoogleDocumentAI extends ProcessorBase {

  /**
   * Google Document AI serivce.
   *
   * @var \Drupal\document_ocr\Services\GoogleDocumentAi as GoogleDocumentAiService
   */
  protected $googleDocumentAi;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PrivateTempStoreFactory $temp_store_factory, FileSystemInterface $file_system, LoggerChannelFactoryInterface $logger_factory, GoogleDocumentAiService $google_document_ai) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $temp_store_factory, $file_system, $logger_factory);
    $this->googleDocumentAi = $google_document_ai;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('tempstore.private'),
      $container->get('file_system'),
      $container->get('logger.factory'),
      $container->get('document_ocr.google_documentai')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function requirementsAreMet() {
    return class_exists('\Google\Cloud\DocumentAI\V1\DocumentProcessorServiceClient');
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $this->googleDocumentAi->setCredentials($this->getCredentials())->serviceClient();
    $locations = $this->googleDocumentAi->getLocations();
    $project_id = !empty($this->configuration['project_id']) ? $this->configuration['project_id'] : $this->googleDocumentAi->getProjectId();
    $form['#attached']['library'][] = 'document_ocr/document_ocr.google_document_ai.admin';
    $form['project_id'] = [
      '#type' => 'item',
      '#title' => $this->t('Project ID'),
      '#markup' => '<em>' . $project_id . '</em>',
    ];
    $form['location_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Region'),
      '#empty_option' => $this->t('- Select Region -'),
      '#options' => $locations,
      '#description' => $this->t('Specify either a regional or multi-regional location for data storage and document processing. <a href="@link" target="_blank">See supported regions</a>', ['@link' => 'https://cloud.google.com/document-ai/docs/regions']),
      '#default_value' => !empty($this->configuration['location_name']) ? $this->configuration['location_name'] : '',
      '#required' => TRUE,
      '#disabled' => !empty($this->configuration['location_name']),
      '#ajax' => [
        'callback' => [$this, 'ajaxProcessorForm'],
        'wrapper' => 'processors-container',
        'method' => 'replace',
      ],
    ];
    $form['processors_container'] = [
      '#type' => 'container',
      '#prefix' => '<div id="processors-container">',
      '#suffix' => '</div>',
    ];

    if (!empty($this->configuration)) {
      // Edit config form.
      if (!empty($this->configuration['location_name'])) {
        $processors = [];
        $processors_without_default = [];
        $processors_list = $this->googleDocumentAi->getProcessors($this->configuration['location_name']);
        foreach ($processors_list as $name => $element) {
          $processors[$name . '::' . $element->getDefaultProcessorVersion()] = $element->getDisplayName() . ' (' . $element->getType() . ')';
          $processors_without_default[$name] = $element->getDisplayName() . ' (' . $element->getType() . ')';
        }
        $form['processors_container']['processor_name'] = [
          '#title' => $this->t('Processor'),
          '#type' => 'select',
          '#empty_option' => $this->t('- Select Processor -'),
          '#options' => $processors_without_default,
          '#description' => $this->t('General document and schematized processors. <a href="@link" target="_blank">Add more processors</a> on Google Document AI page.', ['@link' => 'https://console.cloud.google.com/ai/document-ai']),
          '#required' => TRUE,
          '#disabled' => TRUE,
          '#default_value' => $this->configuration['processor_name'],
        ];
      }
      if (!empty($this->configuration['processor_name'])) {
        $processors_versions = [];
        $versions_list = $this->googleDocumentAi->setParams($this->configuration['processor_name'])->getProcessorVersions();
        $default_versions = [];
        foreach ($processors as $string => $processor_concat_name) {
          list ($pname, $pdversion) = explode('::', $string);
          $default_versions[$pdversion] = $pdversion;
        }
        foreach ($versions_list as $name => $element) {
          $array = explode('/', $name);
          $version_label = $element->getDisplayName()
            . ' (' . $this->googleDocumentAi->getVersionStateName($element->getState()) . ') ' . $array[count($array) - 1];
          $version = !empty($default_versions[$name]) ? $version_label  . ' - ' . $this->t('DEFAULT VERSION') : $version_label;
          $processors_versions[$name] = $version;
        }
        $form['processor_version_container']['processor_version'] = [
          '#title' => $this->t('Processor Version'),
          '#type' => 'select',
          '#empty_option' => $this->t('- Select Processor Version -'),
          '#description' => $this->t('If not selected default processor version will be used.'),
          '#options' => $processors_versions,
          '#default_value' => $this->configuration['processor_version'],
        ];
      }
    }
    else {
      // New config form.
      $form['processors_container']['processor_name'] = [
        '#title' => $this->t('Processor'),
        '#type' => 'select',
        '#empty_option' => $this->t('- Select Processor -'),
        '#description' => $this->t('General document and schematized processors. <a href="@link" target="_blank">Add more processors</a> on Google Document AI page.', ['@link' => 'https://console.cloud.google.com/ai/document-ai']),
        '#limit_validation_errors' => [['processor_name']],
        '#default_value' => !empty($this->configuration['processor_name']) ? $this->configuration['processor_name'] : '',
        '#required' => TRUE,
        '#active' => FALSE,
        '#validated' => TRUE,
        '#ajax' => [
          'callback' => [$this, 'ajaxProcessorVersionForm'],
          'event' => 'change',
          'wrapper' => 'processor-version-container',
        ],
      ];
      if (!empty($form_state->getValue('location_name'))) {
        $processors = [];
        $processors_list = $this->googleDocumentAi->getProcessors($form_state->getValue('location_name'));
        foreach ($processors_list as $name => $element) {
          $processors[$name . '::' . $element->getDefaultProcessorVersion()] = $element->getDisplayName() . ' (' . $element->getType() . ')';
        }
        $form['processors_container']['processor_name'] = [
          '#title' => $this->t('Processor'),
          '#type' => 'select',
          '#empty_option' => $this->t('- Select Processor -'),
          '#options' => $processors,
          '#description' => $this->t('General document and schematized processors. <a href="@link" target="_blank">Add more processors</a> on Google Document AI page.', ['@link' => 'https://console.cloud.google.com/ai/document-ai']),
          '#required' => TRUE,
          '#active' => FALSE,
          '#validated' => TRUE,
          '#ajax' => [
            'callback' => '::ajaxProcessorVersionForm',
            'event' => 'change',
            'wrapper' => 'processor-version-container',
          ],
        ];
      }
      $form['processor_version_container'] = [
        '#type' => 'container',
        '#prefix' => '<div id="processor-version-container">',
        '#suffix' => '</div>',
      ];

      if (!empty($form_state->getUserInput()['processor_name'])) {
        list($processor_name, $processor_default_version) = explode('::', $form_state->getUserInput()['processor_name']);
        if (!empty($processor_name)) {
          $processors_versions = [];
          $versions_list = $this->googleDocumentAi->setParams($processor_name)->getProcessorVersions();
          foreach ($versions_list as $name => $element) {
            $array = explode('/', $name);
            $version_label = $element->getDisplayName()
              . ' (' . $this->googleDocumentAi->getVersionStateName($element->getState()) . ') ' . $array[count($array) - 1];
            $version = ($processor_default_version == $name) ? $version_label  . ' - ' . $this->t('DEFAULT VERSION') : $version_label;
            $processors_versions[$name] = $version;
          }
          $form['processor_version_container']['processor_version'] = [
            '#title' => $this->t('Processor Version'),
            '#type' => 'select',
            '#empty_option' => $this->t('- Select Processor Version -'),
            '#description' => $this->t('If not selected default processor version will be used.'),
            '#validated' => TRUE,
            '#options' => $processors_versions,
          ];
        }
      }
    }
    if (!empty($form['asynchronous'])) {
      $form['asynchronous']['#description'] = $this->t('<strong>Important:</strong> PDF files with more than 15 pages should use <em>asynchronous requests</em>.<br/>Also see Document AI <a href="@link" target="_blank">quotas and limits</a>.', ['@link' => 'https://cloud.google.com/document-ai/quotas']);
      $form['delete_bucket_objects'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Delete objects from the Google Cloud Storage when processing is complete'),
        '#description' => $this->t('For large file processing the module is using Google Cloud Stoage to store processed files. When this option is checked those files will be automatically deleted when processing is complete to save space in your cloud storage bucket.'),
        '#default_value' => !empty($this->configuration['delete_bucket_objects']),
        '#weight' => 102,
        '#states' => [
          'visible' => [
            ':input[name="asynchronous"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    if (!empty($this->configuration)) {
      return [
        'project_id' => $this->configuration['project_id'],
        'location_name' => $this->configuration['location_name'],
        'processor_name' => $this->configuration['processor_name'],
        'processor_version' => $form_state->getValue('processor_version'),
        'processor_display_name' => $this->configuration['processor_display_name'],
        'processor_type' => $this->configuration['processor_type'],
        'delete_bucket_objects' => (bool) $form_state->getValue('delete_bucket_objects'),
      ];
    }
    else {
      $input = $form_state->getUserInput();
      $processors_list = $this->googleDocumentAi->getProcessors($form_state->getValue('location_name'));
      list($processor_name, $processor_default_version) = explode('::', $input['processor_name']);
      return [
        'project_id' => $this->googleDocumentAi->getProjectId(),
        'location_name' => $form_state->getValue('location_name'),
        'processor_name' => $processor_name,
        'processor_version' => $input['processor_version'],
        'processor_display_name' => $processors_list[$processor_name]->getDisplayName(),
        'processor_type' => $processors_list[$processor_name]->getType(),
        'delete_bucket_objects' => (bool) $form_state->getValue('delete_bucket_objects'),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingData() {
    try {
      $this->googleDocumentAi->setCredentials($this->getCredentials())->serviceClient();
      $this->setOption('extracted_text', $this->t('Extracted Text'));
      // If template not required we only show `extracted_text` option.
      if ($file = $this->getFile()) {
        $document = $this->googleDocumentAi
            ->setParams(!empty($this->configuration['processor_version']) ? $this->configuration['processor_version'] : $this->configuration['processor_name'])
            ->processDocument($file);
        $this->setOptionValue('extracted_text', $document->getText());
        foreach ($document->getEntities() as $entity) {
          $this->setOption($entity->getType(), $entity->getType());
          $this->setOptionValue($entity->getType(), $entity->getMentionText());
        }
        $this->setDocument($document);
      }
      return $this;
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getAsyncMappingData($task) {
    try {
      $file = $this->getFile();
      $this->googleDocumentAi->setCredentials($this->getCredentials())->serviceClient();
      $document = $this->googleDocumentAi
        ->setParams(!empty($this->configuration['processor_version']) ? $this->configuration['processor_version'] : $this->configuration['processor_name'])
        ->processAsynchronousDocument($task, $file, $this->configuration);
      if ($document) {
        $this->setOptionValue('extracted_text', $document->getText());
        foreach ($document->getEntities() as $entity) {
          $this->setOptionValue($entity->getType(), $entity->getMentionText());
        }
        $this->setDocument($document);
        return $this;
      }
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function asyncComplete($task) {
    if (!empty($this->configuration['delete_bucket_objects'])) {
      try {
        $this->googleDocumentAi
          ->setCredentials($this->getCredentials())
          ->serviceClient()
          ->asynchronousComplete($task, $this->configuration);
        return $this;
      }
      catch (\Exception $ex) {
        $this->loggerFactory->error($ex->getMessage());
        return FALSE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function skipTemplate($processor) {
    $configuration = $processor->getConfiguration();
    // OCR processing does not require a template for the mapping tool.
    return (!empty($configuration['processor_type']) && $configuration['processor_type'] == 'OCR_PROCESSOR');
  }

  /**
   * {@inheritdoc}
   */
  public function getDocumentAsJson() {
    $document = $this->getDocument();
    if (is_object($document)) {
      return $document->serializeToJsonString();
    }
    return '';
  }

  /**
   * Handles switching region selector.
   */
  public function ajaxProcessorForm($form, FormStateInterface $form_state) {
    return $form['processors_container'];
  }

  /**
   * Handles switching prcoessor selector.
   */
  public function ajaxProcessorVersionForm($form, FormStateInterface $form_state) {
    return $form['processor_version_container'];
  }

}
