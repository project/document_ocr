<?php

namespace Drupal\document_ocr\Plugin\document_ocr\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\document_ocr\Plugin\ProcessorBase;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Docconv processor plugin.
 *
 * @DocumentOcrProcessor(
 *   id = "docconv_extractor",
 *   name = @Translation("Docconv Extractor"),
 *   group = @Translation("Shell Commands"),
 *   extensions = "pdf doc docx xml html rtf odt pages",
 * )
 */
class Docconv extends ProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function requirementsAreMet() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $form['bin_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Command Location'),
      '#default_value' => !empty($this->configuration['bin_path']) ? $this->configuration['bin_path'] : '',
      '#description' => $this->t('Set the path to the docd command. See <a href="https://github.com/sajari/docconv" target="_blank">details on how to install this utility</a>.'),
      '#placeholder' => $this->t('Example: /usr/bin/docd'),
      '#required' => TRUE,
    ];
    $form['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout'),
      '#default_value' => !empty($this->configuration['timeout']) ? (int) $this->configuration['timeout'] : 60,
      '#placeholder' => $this->t('Set execution timeout value in seconds. Recommended is 60 seconds.'),
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    return [
      'bin_path' => $form_state->getValue('bin_path'),
      'timeout' => (int) $form_state->getValue('timeout'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingData() {
    try {
      if ($file = $this->getFile()) {
        $process = new Process([$this->configuration['bin_path'], '-input', $this->fileSystem->realpath($file->getFileUri())]);
        $process->setTimeout($this->configuration['timeout']);
        $process->run();
        if (!$process->isSuccessful()) {
          throw new ProcessFailedException($process);
        }
        $this->setOption('extracted_text', $this->t('Extracted Text'));
        $this->setOptionValue('extracted_text', $process->getOutput());
        $this->setDocument($process);
      }
      else {
        $this->setOption('extracted_text', $this->t('Extracted Text'));
        $this->setOptionValue('extracted_text', '');
      }
      return $this;
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
      return FALSE;
    }
  }

}
