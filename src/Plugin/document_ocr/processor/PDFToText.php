<?php

namespace Drupal\document_ocr\Plugin\document_ocr\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\document_ocr\Plugin\ProcessorBase;
use Spatie\PdfToText\Pdf;

/**
 * PDF to Text processor plugin.
 *
 * @DocumentOcrProcessor(
 *   id = "pdf_to_text",
 *   name = @Translation("PDFtoText (Poppler)"),
 *   group = @Translation("Shell Commands"),
 *   extensions = "pdf",
 * )
 */
class PDFToText extends ProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function requirementsAreMet() {
    return class_exists('\Spatie\PdfToText\Pdf');
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $form['bin_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Command Location'),
      '#default_value' => !empty($this->configuration['bin_path']) ? $this->configuration['bin_path'] : '/usr/bin/pdftotext',
      '#description' => $this->t('By default the package will assume that the pdftotext command is located at /usr/bin/pdftotext.'),
      '#placeholder' => $this->t('/usr/bin/pdftotext'),
      '#required' => TRUE,
    ];
    $form['options'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Options'),
      '#description' => $this->t('One option parameter per line. See all the options in the <a href="https://linux.die.net/man/1/pdftotext" target="_blank">documentation</a>.'),
      '#default_value' => !empty($this->configuration['options']) ? $this->configuration['options'] : '',
      '#placeholder' => $this->t("Example: layout\r\nr 96"),
    ];
    $form['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout'),
      '#default_value' => !empty($this->configuration['timeout']) ? (int) $this->configuration['timeout'] : 60,
      '#placeholder' => $this->t('Set execution timeout value in seconds. Recommended is 60 seconds.'),
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    return [
      'bin_path' => $form_state->getValue('bin_path'),
      'options' => $form_state->getValue('options'),
      'timeout' => (int) $form_state->getValue('timeout'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingData() {
    try {
      if ($file = $this->getFile()) {
        $pdf = (new Pdf($this->configuration['bin_path']))->setPdf($this->fileSystem->realpath($file->getFileUri()));
        if (!empty($this->configuration['options'])) {
          $pdf->setOptions($this->linesIntoArray($this->configuration['options']));
        }
        if (!empty($this->configuration['timeout'])) {
          $pdf->setTimeout($this->configuration['timeout']);
        }
        $this->setOption('extracted_text', $this->t('Extracted Text'));
        $this->setOptionValue('extracted_text', $pdf->text());
        $this->setDocument($pdf);
      }
      else {
        $this->setOption('extracted_text', $this->t('Extracted Text'));
        $this->setOptionValue('extracted_text', '');
      }
      return $this;
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
      return FALSE;
    }
  }

}
