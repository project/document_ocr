<?php

namespace Drupal\document_ocr\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface Processor plugins.
 */
interface ProcessorInterface extends PluginInspectionInterface {

  /**
   * Return the name of the Processor plugin.
   */
  public function getName();

  /**
   * Return a list of plugin supported extensions.
   */
  public function getExtensions();

  /**
   * Return TRUE if plugin requirements are met otherwise FALSE.
   */
  public function requirementsAreMet();

  /**
   * Processor plugin configuration form.
   */
  public function configurationForm(array $form, FormStateInterface $form_state);

  /**
   * Get configration form values.
   */
  public function configurationValues(array &$form, FormStateInterface $form_state);

  /**
   * Get mapping options.
   */
  public function getMappingData();

}
