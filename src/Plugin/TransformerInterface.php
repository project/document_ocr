<?php

namespace Drupal\document_ocr\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines an interface Transformer plugins.
 */
interface TransformerInterface extends PluginInspectionInterface {

  /**
   * Return the name of the Transformer plugin.
   */
  public function getName();

  /**
   * Return TRUE if plugin requirements are met otherwise FALSE.
   */
  public function requirementsAreMet();

  /**
   * Transformer plugin configuration form.
   */
  public function configurationForm(array $form, FormStateInterface $form_state);

  /**
   * Get configration form values.
   */
  public function configurationValues(array &$form, FormStateInterface $form_state);


  /**
   * Transform value before passing it into entity field.
   */
  public function transform($value);

}
