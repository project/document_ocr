<?php

namespace Drupal\document_ocr\Plugin;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Component\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\document_ocr\Traits\TempKeyValTrait;
use Drupal\document_ocr\Traits\UploadPathTrait;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * Processor plugin base class.
 *
 * @package document_ocr
 */
class ProcessorBase extends PluginBase implements ProcessorInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;
  use TempKeyValTrait;
  use UploadPathTrait;

  /**
   * Private temporary store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Stores private data for the process multistep.
   *
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

   /**
   * Logger service.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  protected $credentials;
  protected $configuration = [];
  protected $values = [];
  protected $options = [];
  protected $document;
  protected $file;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PrivateTempStoreFactory $temp_store_factory, FileSystemInterface $file_system, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get(DocumentOcrInterface::MAPPING_STORAGE_KEY);
    $this->fileSystem = $file_system;
    $this->loggerFactory = $logger_factory->get('document_ocr');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('tempstore.private'),
      $container->get('file_system'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->pluginDefinition['name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getGroup() {
    return !empty($this->pluginDefinition['group']) ? $this->pluginDefinition['group'] : '';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigDialogWidth() {
    return !empty($this->pluginDefinition['config_dialog_width'])
      ? $this->pluginDefinition['config_dialog_width']
      : DocumentOcrInterface::PROCESSOR_CONFIG_DIALOG_WIDTH;
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensions() {
    return $this->pluginDefinition['extensions'];
  }

  /**
   * {@inheritdoc}
   */
  public function requireTemplate() {
    return in_array('template', $this->pluginDefinition['requires']);
  }

  /**
   * {@inheritdoc}
   */
  public function requireCredentials() {
    return in_array('credentials', $this->pluginDefinition['requires']);
  }

  /**
   * {@inheritdoc}
   */
  public function isAsynchronous() {
    return in_array('asynchronous', $this->pluginDefinition['supports']);
  }

  /**
   * {@inheritdoc}
   */
  public function isJsonResponse() {
    return in_array('store_json', $this->pluginDefinition['supports']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCredentials() {
    $path = !empty($this->credentials) ? $this->fileSystem->realpath($this->credentials) : FALSE;
    if (!empty($path) && file_exists($path)) {
      return json_decode(trim(file_get_contents($path)), TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setCredentials($credentials) {
    $this->credentials = $credentials;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration($configuration) {
    $this->configuration = is_array($configuration) ? $configuration : [];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->file;
  }

  /**
   * {@inheritdoc}
   */
  public function setFile($file) {
    $this->file = $file;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function requirementsAreMet() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function setOption($key, $value) {
    $this->options[$key] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOptionValue($key, $value) {
    $this->values[$key] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setDocument($document) {
    $this->document = $document;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDocument() {
    return $this->document;
  }

  /**
   * {@inheritdoc}
   */
  public function getDocumentAsJson() {
    if ($array = (array)$this->getDocument()) {
      return json_encode($array);
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getValues() {
    return $this->values;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {
    return $this->options;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingData() {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAsyncMappingData($task) {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function asyncComplete($task) {
    // Perform operation when async process task is complete.
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function skipTemplate($processor) {
    return;
  }

  /**
   * Helper function to turn lines into arrays.
   * @param $string
   * @return array
   */
  protected function linesIntoArray($string) {
    return explode("\n", str_replace("\r\n","\n", $string));
  }

}
