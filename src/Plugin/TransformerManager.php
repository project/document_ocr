<?php

namespace Drupal\document_ocr\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Transformer plugin manager.
 *
 * @package document_ocr
 */
class TransformerManager extends DefaultPluginManager {

  /**
   * Constructs an Transformer object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations,.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/document_ocr/transformer', $namespaces, $module_handler, 'Drupal\document_ocr\Plugin\TransformerInterface', 'Drupal\document_ocr\Annotation\DocumentOcrTransformer');
    $this->alterInfo('document_ocr_transformer');
    $this->setCacheBackend($cache_backend, 'transformer');
  }

}
