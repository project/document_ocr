<?php

namespace Drupal\document_ocr;

/**
 * Provides an interface for helpers that operate on uploads.
 *
 * @ingroup document_ocr
 */
interface DocumentOcrInterface {

  /**
   * File upload directory (public).
   */
  const PUBLIC_URI = 'public://document-ocr';

  /**
   * File upload directory (private).
   */
  const PRIVATE_URI = 'private://document-ocr';

  /**
   * File upload directory (public) for audio generated from text.
   */
  const TEXT_TO_SPEECH_URI = 'public://document-ocr-text-to-speech';

  /**
   * Mapping tmp storage key.
   */
  const MAPPING_STORAGE_KEY = 'document_ocr_mapping';

  /**
   * Transformer tmp storage key.
   */
  const TRANSFORMER_STORAGE_KEY = 'document_ocr_transformer';

  /**
   * Transformer configuration dialog deafult width.
   */
  const TRANSFORMER_CONFIG_DIALOG_WIDTH = 620;

  /**
   * Processor tmp storage key.
   */
  const PROCESSOR_STORAGE_KEY = 'document_ocr_processor';

  /**
   * Processor configuration dialog deafult width.
   */
  const PROCESSOR_CONFIG_DIALOG_WIDTH = 620;

  /**
   * One-time process tmp storage key.
   */
  const ONETIME_STORAGE_KEY = 'document_ocr_onetime';

  /**
   * Settings config key.
   */
  const SETTINGS_KEY = 'document_ocr.settings';

  /**
   * Exclude fiels from the mapping tool.
   */
  const MAPPING_EXCLUDE_FIELDS = [
      'uuid', 'type', 'id', 'nid', 'vid', 'revision_uid', 'revision_timestamp',
      'revision_log', 'revision_default', 'revision_translation_affected',
      'default_langcode', 'revision_user', 'revision_created', 'revision_id',
      'bundle',
    ];

  /**
   * Task is initiated.
   */
  const TASK_STATUS_INIT = 1;

  /**
   * Task is pending.
   */
  const TASK_STATUS_PENDING = 2;

  /**
   * Task processed.
   */
  const TASK_STATUS_PROCESSED = 3;

  /**
   * Task failed.
   */
  const TASK_STATUS_FAILED = 4;

}
