<?php

namespace Drupal\document_ocr\Entity\Form\Transformer;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\document_ocr\Plugin\TransformerManager;

/**
 * Transfomer abstract base form class.
 */
abstract class BaseForm extends EntityForm {

  /**
   * @var \Drupal\document_ocr\Transformer
   */
  protected $entity;

  /**
   * The transformer manager.
   *
   * @var \Drupal\document_ocr\Plugin\TransformerManager
   */
  protected $transformerManager;

  /**
   * Save entity message.
   *
   * @var string
   */
  protected $success_message;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.document_ocr_transformer')
    );
  }

  /**
   * BaseForm constructor.
   */
  public function __construct(TransformerManager $transformer_manager) {
    $this->transformerManager = $transformer_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['#attached']['library'][] = 'document_ocr/document_ocr.admin';
    $form['actions']['submit']['#prefix'] = '<div class="document-ocr-button-wrapper"><span class="document-ocr-button-spinner"></span>';
    $form['actions']['submit']['#suffix'] = '</div>';
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' =>  Url::fromRoute('entity.document_ocr_transformer.collection'),
      '#weight' => 10,
      '#attributes' => ['class' => ['button', 'dialog-cancel']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $this->messenger()->addMessage($this->t($this->success_message, [
      '%label' => $this->entity->label(),
    ]));
    $form_state->setRedirect('entity.document_ocr_transformer.collection');
  }

}
