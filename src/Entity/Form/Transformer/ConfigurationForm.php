<?php

namespace Drupal\document_ocr\Entity\Form\Transformer;

use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration entity edit form.
 */
class ConfigurationForm extends BaseForm {

  /**
   * @var \Drupal\document_ocr\Transformer
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  protected $success_message = '%label transformer configuration updated.';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'document_ocr_transformer_' . $this->entity->getTransformer();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['#attached']['library'][] = 'document_ocr/document_ocr.admin';
    $document_ocr_transformer = $this->entity->getTransformerPlugin();
    $form_state->set('document_ocr_transformer', $document_ocr_transformer);
    $document_ocr_transformer
      ->setCredentials($this->entity->getCredentials())
      ->setConfiguration($this->entity->getConfiguration());
    $configuration_form = $document_ocr_transformer->configurationForm($form, $form_state);
    if (empty($configuration_form)) {
      $form['empty'] = [
        '#markup' => '<p>' . $this->t('This transformer does not provide configuration options.') . '</p>'
      ];
      unset($form['actions']['submit']);
    }
    else {
      if ($configuration_form !== $form) {
        $form = array_merge($form, $configuration_form);
        $form['actions']['submit']['#value'] = $this->t('Update %name', ['%name' => $this->entity->label()]);
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\document_ocr\Transformer $entity */
    $entity = parent::buildEntity($form, $form_state);
    if ($configuration = $form_state->get('document_ocr_transformer')->configurationValues($form, $form_state)) {
      $entity->setConfiguration($configuration);
    }
    return $entity;
  }

}
