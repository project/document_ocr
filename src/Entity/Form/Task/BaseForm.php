<?php

namespace Drupal\document_ocr\Entity\Form\Task;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * Processor abstract base form class.
 */
abstract class BaseForm extends ConfirmFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new BaseForm.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->form_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t($this->question);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t($this->confirm_text);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.document_ocr_task.collection');
  }

  /**
   * Get mappings with the number of pending items in each mapping.
   */
  protected function getPendingMappingList() {
    $options = [];
    $query = $this->entityTypeManager->getStorage('document_ocr_task')->getQuery()
      ->notExists('document_ocr_mapping')
      ->condition('status', [DocumentOcrInterface::TASK_STATUS_INIT, DocumentOcrInterface::TASK_STATUS_PENDING], 'IN')
      ->accessCheck(FALSE);
    $count = (int) $query->count()->execute();
    $options[-1] = $this->t('One-Time (%count)', ['%count' => $count]);
    $entities = $this->entityTypeManager->getStorage('document_ocr_mapping')->loadByProperties(['status' => 1]);
    foreach ($entities as $mapping) {
      $settings = $mapping->getSettings();
      $query = $this->entityTypeManager->getStorage('document_ocr_task')->getQuery()
        ->condition('document_ocr_mapping', $mapping->id())
        ->condition('status', [DocumentOcrInterface::TASK_STATUS_INIT, DocumentOcrInterface::TASK_STATUS_PENDING], 'IN')
        ->condition('attempts', $settings['attempts'], '<')
        ->accessCheck(FALSE);
      $count = (int) $query->count()->execute();
      $options[$mapping->id()] = $this->t('@label (%count)', ['@label' => $mapping->label(), '%count' => $count]);
    }
    return $options;
  }

  /**
   * Get all entities based on mapping configuration.
   */
  protected function getAllEntities($mapping) {
    $selectors = $mapping->getFieldSelector();
    $imported_ids = [];
    $storage = $this->entityTypeManager->getStorage('document_ocr_task');
    $query = $storage->getQuery()
      ->condition('document_ocr_mapping', $mapping->id())
      ->condition('source_entity_type_id', $selectors['entity_type_id'])
      ->condition('source_bundle', $selectors['bundle'])
      ->accessCheck(FALSE);
    $ids = $query->execute();
    foreach ($storage->loadMultiple($ids) as $task) {
      $imported_ids[] = $task->get('source_entity_id')->value;
    }

    $entity_type_id = $selectors['entity_type_id'];
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $query = $storage->getQuery()
      ->condition('status', 1);
    if ($entity_type_id == 'node') {
      $query->condition('type', $selectors['bundle']);
    }
    else {
      $query->condition('bundle', $selectors['bundle']);
    }
    $query->accessCheck(FALSE);
    $ids = $query->execute();
    $entities = [];
    foreach ($storage->loadMultiple($ids) as $entity) {
      if (!empty($imported_ids)) {
        if (!in_array($entity->id(), $entities)) {
          $entities[] = $entity;
        }
      }
      else {
        $entities[] = $entity;
      }
    }
    return $entities;
  }

}
