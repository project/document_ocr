<?php

namespace Drupal\document_ocr\Entity\Form\Task;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Process pending task form.
 */
class ProcessForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\document_ocr\Entity\Task */
    $form = parent::buildForm($form, $form_state);
    $form['description'] = [
      '#markup' => '<p>' . $this->t('This action will attempt processing the task. You might need to process it multiple times this depeneds if the processor is using asynchronous method or not.') . '</p>'
    ];
    $form['actions']['submit']['#value'] = $this->t('Process @status Task', ['@status' => $this->entity->getTaskStatus(TRUE)]);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $batch = [
      'title' => $this->t('Processing Pending Task'),
      'finished' => '\Drupal\document_ocr\Services\Batch::completeProcessCallback',
      'operations' => [],
    ];
    $item = new \stdClass();
    $item->id = $this->entity->id();
    $batch['operations'][] = [
      '\Drupal\document_ocr\Services\Batch::process', [$item],
    ];
    batch_set($batch);
    $form_state->setRedirect('entity.document_ocr_task.collection');
  }

}
