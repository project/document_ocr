<?php

namespace Drupal\document_ocr\Entity\Form\Task;

use Drupal\Core\Form\FormStateInterface;

/**
 * Process doucments for older entities for alreadt configured mapping.
 */
class OlderEntitiesForm extends BaseForm {

  protected $form_id = 'document_ocr_process_older_entities_form';
  protected $question = 'Queue Older Entities';
  protected $confirm_text = 'Queue Entities';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['#attached']['library'][] = 'document_ocr/document_ocr.admin';
    $form['description']['#prefix'] = '<p>';
    $form['description']['#suffx'] = '</p>';
    $form['actions']['cancel']['#attributes']['class'][] = 'dialog-cancel';
    $form['description']['#markup'] = $this->t('This action will lookup older entities created before enabling the module for configured and active mapping and process each file.');
    $form['mapping'] = [
      '#type' => 'select',
      '#title' => $this->t('Mapping to Process'),
      '#options' => $this->getPendingItems(),
      '#empty_option' => $this->t('- Select Mapping -'),
      '#description' => $this->t('If unselected all older entity documents will be processed.'),
    ];
    $form['actions']['submit']['#prefix'] = '<div class="document-ocr-button-wrapper"><span class="document-ocr-button-spinner"></span>';
    $form['actions']['submit']['#suffix'] = '</div>';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch = [
      'title' => $this->t('Processing Older Files'),
      'finished' => '\Drupal\document_ocr\Services\Batch::completeProcessCallback',
      'operations' => [],
    ];
    if ($mapping = $this->entityTypeManager->getStorage('document_ocr_mapping')->load($form_state->getValue('mapping'))) {
      $batch['title'] = $this->t('Processing Older Entity for %mapping', ['%mapping' => $mapping->label()]);
    }
    $storage = $this->entityTypeManager->getStorage('document_ocr_mapping');
    $query = $storage->getQuery();
    if (!empty($form_state->getValue('mapping'))) {
      $query->condition('id', $form_state->getValue('mapping'));
    }
    $query->condition('status', 1);
    $ids = $query->accessCheck(FALSE)->execute();
    foreach ($storage->loadMultiple($ids) as $mapping) {
      if ($entities = $this->getAllEntities($mapping)) {
        foreach ($entities as $entity) {
          $batch['operations'][] = [
            '\Drupal\document_ocr\Services\Batch::processOlderEntity', [$entity],
          ];
        }
      }
    }
    batch_set($batch);
    $form_state->setRedirect('entity.document_ocr_task.collection');
  }

  /**
   * Get mappings with the number of pending items in each mapping.
   */
  protected function getPendingItems() {
    $options = [];
    $entities = $this->entityTypeManager->getStorage('document_ocr_mapping')->loadByProperties(['status' => 1]);
    foreach ($entities as $mapping) {
      $options[$mapping->id()] = $mapping->label();
    }
    return $options;
  }

}
