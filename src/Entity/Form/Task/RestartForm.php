<?php

namespace Drupal\document_ocr\Entity\Form\Task;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Restart task form.
 */
class RestartForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\document_ocr\Entity\Task */
    $form = parent::buildForm($form, $form_state);
    $form['description'] = [
      '#markup' => '<p>' . $this->t('This action will restart the task however you will need to process it as a separate action.') . '</p>'
    ];
    $form['actions']['submit']['#value'] = $this->t('Restart Task');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->restart();
    $message = $this->t('Task %label has been restarted.', [
      '%label' => $this->entity->label(),
    ]);
    $this->logger('document_ocr')->notice($message);
    $this->messenger()->addMessage($message);
    parent::save($form, $form_state);
    $form_state->setRedirect('entity.document_ocr_task.collection');
  }

}
