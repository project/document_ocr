<?php

namespace Drupal\document_ocr\Entity\Form\Processor;

use Drupal\Core\Form\FormStateInterface;

/**
 * Processor entity edit form.
 */
class EditForm extends BaseForm {

  /**
   * {@inheritdoc}
   */
  protected $success_message = '%label processor updated.';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('A short name to help you identify this processor in the list.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('ID'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->id(),
      '#required' => TRUE,
      '#disabled' => !$this->entity->isNew(),
      '#machine_name' => [
        'exists' => 'Drupal\document_ocr\Entity\Processor::load',
      ],
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->getDescription(),
    ];

    $processor = $this->entity->getProcessorPlugin();
    if ($processor->requireCredentials()) {
      $form['credentials'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Processor Credentials'),
        '#default_value' => $this->entity->getCredentials(),
        '#placeholder' => $this->t('Private (private://you-config.json) path is highly recommended'),
        '#description' => $this->t('JSON file with required credentials for the processor.'),
        '#required' => TRUE,
      ];
    }

    return $form;
  }

}
