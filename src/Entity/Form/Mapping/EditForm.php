<?php

namespace Drupal\document_ocr\Entity\Form\Mapping;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Processor entity edit form.
 */
class EditForm extends BaseForm {

  /**
   * {@inheritdoc}
   */
  protected $success_message = '%label mapping details updated.';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['#attached']['library'][] = 'document_ocr/document_ocr.admin';
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#size' => 100,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('A short name to help you identify this mapping in the list.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('ID'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->id(),
      '#required' => TRUE,
      '#disabled' => !$this->entity->isNew(),
      '#machine_name' => [
        'exists' => 'Drupal\document_ocr\Entity\Mapping::load',
      ],
    ];
    if ($processor = $this->getProcessor($this->entity->getProcessor())->getProcessorPlugin()) {

      $field_selector = $this->entity->getFieldSelector();
      $destination_bundle = $this->entity->getDestinationBundle();

      $bundle_options = [];
      // Get all applicable entity types.
      foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
        if (is_subclass_of($entity_type->getClass(), FieldableEntityInterface::class) && $entity_type->hasLinkTemplate('canonical')) {
          if ($entity_type->hasKey('bundle')) {
            if ($bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type->id())) {
              foreach ($bundles as $id => $info) {
                $bundle_options[(string)$entity_type->getLabel()][$entity_type->id() . '::' . $id] = $info['label'];
              }
            }
          }
        }
      }
      $form['bundle_info_ne'] = [
        '#type' => 'select',
        '#title' => $this->t('Source Bundle'),
        '#options' => $bundle_options,
        '#default_value' => $field_selector['entity_type_id'] . '::' . $field_selector['bundle'],
        '#disabled' => TRUE,
        '#required' => TRUE,
        '#description' => $this->t('Entity type with the file field.'),
      ];

      $form['fields_container_ne'] = [
        '#type' => 'container',
        '#prefix' => '<div id="fields-container">',
        '#suffix' => '</div>',
      ];

      if ($fields = $this->entityFieldManager->getFieldDefinitions($field_selector['entity_type_id'], $field_selector['bundle'])) {
        $field_options = [];
        foreach ($fields as $field_name => $field_definition) {
          if (!empty($field_definition->getTargetBundle())) {               
            if (in_array($field_definition->getType(), ['image', 'file'])) {
              $extensions = $field_definition->getSetting('file_extensions');
              $field_options[$field_name] = $field_definition->getLabel() . ' (' . $field_definition->getType() . ') ' . $extensions;
            }
          }
        }
        $form['fields_container_ne']['field_ne'] = [
          '#title' => $this->t('Field'),
          '#type' => 'select',
          '#options' => $field_options,
          '#default_value' => $field_selector['field'],
          '#description' => $this->t('File field to process.'),
          '#disabled' => TRUE,
          '#required' => TRUE,
        ];
      }

      $form['destination_bundle_ne'] = [
        '#type' => 'select',
        '#title' => $this->t('Destination Bundle'),
        '#options' => $bundle_options,
        '#default_value' => $destination_bundle['entity_type_id'] . '::' . $destination_bundle['bundle'],
        '#disabled' => TRUE,
        '#description' => $this->t('Extracted data storage.'),
        '#required' => TRUE,
      ];

      $form['processor'] = [
        '#title' => $this->t('Processor'),
        '#type' => 'select',
        '#description' => $this->t('Choose document processor.'),
        '#options' => $this->getProcessors(),
        '#default_value' => $this->entity->getProcessor(),
        '#disabled' => !$this->entity->isNew(),
        '#required' => TRUE,
      ];
    }

    return $form;
  }

}
