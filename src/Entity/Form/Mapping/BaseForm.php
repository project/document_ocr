<?php

namespace Drupal\document_ocr\Entity\Form\Mapping;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\document_ocr\Traits\TempKeyValTrait;
use Drupal\document_ocr\Traits\UploadPathTrait;
use Drupal\document_ocr\Plugin\ProcessorManager;
use Drupal\document_ocr\Plugin\TransformerManager;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * Processor abstract base form class.
 */
abstract class BaseForm extends EntityForm {

  use TempKeyValTrait;
  use UploadPathTrait;

  protected $temporary_storage_key = DocumentOcrInterface::MAPPING_STORAGE_KEY;

  /**
   * Exclude fiels from the mapping tool.
   */
  protected $excluded_fields = DocumentOcrInterface::MAPPING_EXCLUDE_FIELDS;

  /**
   * @var \Drupal\document_ocr\Mapping
   */
  protected $entity;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Private temporary factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Private temporary factory.
   *
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * The processor manager.
   *
   * @var \Drupal\document_ocr\Plugin\ProcessorManager
   */
  protected $processorManager;

  /**
   * The transformer manager.
   *
   * @var \Drupal\document_ocr\Plugin\TransformerManager
   */
  protected $transformerManager;

  /**
   * Save entity message.
   *
   * @var string
   */
  protected $success_message;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_field.manager'),
      $container->get('file_system'),
      $container->get('tempstore.private'),
      $container->get('plugin.manager.document_ocr_processor'),
      $container->get('plugin.manager.document_ocr_transformer')
    );
  }

  /**
   * BaseForm constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityFieldManager $entity_field_manager, FileSystemInterface $file_system, PrivateTempStoreFactory $temp_store_factory, ProcessorManager $processor_manager, TransformerManager $transformer_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
    $this->fileSystem = $file_system;
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get($this->temporary_storage_key);
    $this->processorManager = $processor_manager;
    $this->transformerManager = $transformer_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['actions']['submit']['#prefix'] = '<div class="document-ocr-button-wrapper"><span class="document-ocr-button-spinner"></span>';
    $form['actions']['submit']['#suffix'] = '</div>';
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' =>  Url::fromRoute('entity.document_ocr_mapping.collection'),
      '#weight' => 10,
      '#attributes' => ['class' => ['button', 'dialog-cancel']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $this->messenger()->addMessage($this->t($this->success_message, [
      '%label' => $this->entity->label(),
    ]));
    $form_state->setRedirect('entity.document_ocr_mapping.collection');
  }

  /**
   * Get available processors.
   */
  protected function getProcessors() {
    $options = [];
    $processors = $this->entityTypeManager->getStorage('document_ocr_processor')->loadByProperties([]);
    foreach ($processors as $processor) {
      $options[$processor->id()] = $processor->label();
    }
    return $options;
  }

  /**
   * Get processor by ID.
   */
  protected function getProcessor($id) {
    if ($processor = $this->entityTypeManager->getStorage('document_ocr_processor')->load($id)) {
      return $processor;
    }
  }

  /**
   * Get available transformers.
   */
  protected function getTransformers($field_type) {
    $options = [];
    $transformers = $this->entityTypeManager->getStorage('document_ocr_transformer')->loadByProperties([]);
    foreach ($transformers as $transformer) {
      $field_types = $transformer->getTransformerPlugin()->getFieldTypes();
      if (empty($field_types)) {
        $options[$transformer->id()] = $transformer->label();
      }
      else {
        if (in_array($field_type, $field_types)) {
          $options[$transformer->id()] = $transformer->label();
        }
      }
    }
    return $options;
  }

  /**
   * Check if processor require template or there is an exception for certain processors.
   */
  protected function isTemplateRequired($document_ocr_processor) {
    $processor = $document_ocr_processor->getProcessorPlugin()->setCredentials($document_ocr_processor->getCredentials());
    if ($skip_template = $processor->skipTemplate($document_ocr_processor)) {
      if ($processor->requireTemplate()) {
        return FALSE;
      }
    }
    else {
      if ($processor->requireTemplate()) {
        return TRUE;
      }
    }
  }

  /**
   * Check if processor is using asynchronous method.
   */
  protected function isProcessorAsynchronous($document_ocr_processor) {
    $processor = $document_ocr_processor->getProcessorPlugin();
    $configuration = $document_ocr_processor->getConfiguration();
    return ($processor->isAsynchronous() && !empty($configuration['asynchronous']));
  }

}
