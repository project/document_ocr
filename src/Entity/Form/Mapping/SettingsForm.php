<?php

namespace Drupal\document_ocr\Entity\Form\Mapping;

use Drupal\Core\Form\FormStateInterface;

/**
 * Settings entity edit form.
 */
class SettingsForm extends BaseForm {

  /**
   * @var \Drupal\document_ocr\Mapping
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  protected $success_message = '%label mapping settings updated.';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['#attached']['library'][] = 'document_ocr/document_ocr.admin';
    $settings = $this->entity->getSettings();
    $document_ocr_processor = $this->entity->getProcessorReference();
    if (!$this->isProcessorAsynchronous($document_ocr_processor)) {
      $attempt_values = range(3, 20);
      $form['realtime_processing'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Real-time processing (slower, not recommeded)'),
        '#description' => $this->t('Document OCR configured entity will be processed immediately, otherwise entity processing will be queued and executed on cron. When a processor is using asynchronous method for processing this option is ignored.'),
        '#default_value' => !empty($settings['realtime_processing']),
      ];
      $form['attempts'] = [
        '#type' => 'select',
        '#title' => $this->t('Attempt limit'),
        '#options' => array_combine($attempt_values, $attempt_values),
        '#description' => $this->t('Set the limit for the processing attempts. When a processor is using asynchronous method for processing this option is ignored.'),
        '#default_value' => !empty($settings['attempts']) ? $settings['attempts'] : 3,
      ];
    }
    else {
      $form['processing_info'] = [
        '#type' => 'item',
        '#markup' => $this->t('%name processor is using asynchronous method for document processing thus attempt limits are configured in the processor configuration form.', ['%name' => $document_ocr_processor->label()]),
        '#prefix' => '<div class="form-item__description">',
        '#suffix' => '</div>'
      ];
    }
    $form['delete_destination'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete destination entity on source file delete'),
      '#description' => $this->t('This option allows imported content deletion when files associated with it are deleted.'),
      '#default_value' => !empty($settings['delete_destination']),
    ];
    $form['process_update'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Re-run document processing on source entity update'),
      '#description' => $this->t('This option allows to re-run document processing when source entity is updated.'),
      '#default_value' => !empty($settings['process_update']),
    ];
    $form['actions']['submit']['#value'] = $this->t('Save Settings');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\document_ocr\Mapping $entity */
    $entity = parent::buildEntity($form, $form_state);
    $entity->setSettings([
      'realtime_processing' => !empty($form_state->getValue('realtime_processing')),
      'attempts' => !empty($form_state->getValue('attempts')) ? (int) $form_state->getValue('attempts') : 3,
      'delete_destination' => (bool) $form_state->getValue('delete_destination'),
      'process_update' => (bool) $form_state->getValue('process_update'),
    ]);
    return $entity;
  }

}
