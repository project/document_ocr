<?php

namespace Drupal\document_ocr\Entity\Form\Mapping;

use Drupal\Core\Form\FormStateInterface;

/**
 * Process pending mapping tasks form.
 */
class ProcessForm extends BaseForm {

  /**
   * {@inheritdoc}
   */
  protected $success_message = 'Attempt to process pending tasks for %label mapping initiated You may need to run this process multiple times.';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['#attached']['library'][] = 'document_ocr/document_ocr.admin';

    $form['description'] = [
      '#markup' => $this->t('Attempt to process all pending tasks for %name mapping', ['%name' => $this->entity->label()])
    ];

    $form['actions']['submit']['#value'] = $this->t('Run Attempt');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch = [
      'title' => $this->t('Processing Pending Tasks for %name mapping.', ['%name' => $this->entity->label()]),
      'finished' => '\Drupal\document_ocr\Services\Batch::completeProcessCallback',
      'operations' => [],
    ];
    $tasks = $this->entityTypeManager->getStorage('document_ocr_task')->loadByProperties([
      'document_ocr_mapping' => $this->entity->id()
    ]);
    foreach ($tasks as $task) {
      $item = new \stdClass();
      $item->id = $task->id();
      $batch['operations'][] = [
        '\Drupal\document_ocr\Services\Batch::process', [$item],
      ];
    }
    batch_set($batch);
    $form_state->setRedirect('entity.document_ocr_mapping.collection');
  }

}
