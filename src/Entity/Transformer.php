<?php

namespace Drupal\document_ocr\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the Document OCR Transformer entity.
 *
 * @ingroup document_ocr
 *
 * @ConfigEntityType(
 *   id = "document_ocr_transformer",
 *   label = @Translation("Document OCR Transformer"),
 *   handlers = {
 *     "list_builder" = "Drupal\document_ocr\Controller\TransformerListBuilder",
 *     "form" = {
 *       "delete" = "\Drupal\Core\Entity\EntityDeleteForm",
 *       "edit" = "\Drupal\document_ocr\Entity\Form\Transformer\EditForm",
 *       "configuration" = "\Drupal\document_ocr\Entity\Form\Transformer\ConfigurationForm",
 *     },
 *   },
 *   admin_permission = "administer document ocr",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_prefix = "transformer",
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "credentials",
 *     "configuration",
 *     "transformer",
 *   },
 *   lookup_keys = {
 *     "id",
 *   },
 *   links = {
 *     "collection" = "/admin/config/structure/document-ocr/transformers",
 *     "new" = "/admin/config/structure/document-ocr/transformers/new",
 *     "edit" = "/admin/config/structure/document-ocr/transformers/{document_ocr_transformer}/edit",
 *     "configuration" = "/admin/config/structure/document-ocr/transformers/{document_ocr_transformer}/configuration",
 *     "delete" = "/admin/config/structure/document-ocr/transformers/{document_ocr_transformer}/delete",
 *   }
 * )
 */
class Transformer extends ConfigEntityBase implements ConfigEntityInterface {

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The label.
   *
   * @var string
   */
  protected $label;

  /**
   * The transformer description.
   *
   * @var string
   */
  protected $description;

  /**
   * Transformer credentials.
   *
   * @var string
   */
  protected $credentials;

  /**
   * Transformer configuration settings.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * Transformer plugin ID.
   *
   * @var string
   */
  protected $transformer;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCredentials() {
    return $this->credentials;
  }

  /**
   * {@inheritdoc}
   */
  public function setCredentials($credentials) {
    $this->credentials = $credentials;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    if (!is_array($this->configuration) ||empty(array_values($this->configuration))) {
      return [];
    }
    else {
      return $this->configuration;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTransformer() {
    return $this->transformer;
  }

  /**
   * {@inheritdoc}
   */
  public function setTransformer($transformer) {
    $this->transformer = $transformer;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTransformerPlugin() {
    try {
      return \Drupal::service('plugin.manager.document_ocr_transformer')->createInstance($this->getTransformer());
    }
    catch (\Exception $e) {
      \Drupal::logger('document_ocr')->error($e->getMessage());
    }
  }

}
