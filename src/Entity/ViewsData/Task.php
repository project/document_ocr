<?php

namespace Drupal\document_ocr\Entity\ViewsData;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for the Task entities.
 */
class Task extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    $data['document_ocr_task']['last_attempt']['field']['id'] = 'date';
    $data['document_ocr_task']['last_attempt']['sort']['id'] = 'date';
    $data['document_ocr_task']['last_attempt']['filter']['id'] = 'date';
    $data['document_ocr_task']['last_attempt']['argument']['id'] = 'date';
    // Source field
    $data['document_ocr_task']['source'] = [
      'title' => $this->t('Source Entity'),
      'help'  => $this->t('Document OCR source entity.'),
      'field' => [
        'id' => 'document_ocr_task_source',
      ],
    ];
    // Destination field
    $data['document_ocr_task']['destination'] = [
      'title' => $this->t('Destination Entity'),
      'help'  => $this->t('Document OCR destination entity.'),
      'field' => [
        'id' => 'document_ocr_task_destination',
      ],
    ];
    // File field
    $data['document_ocr_task']['task_file'] = [
      'title' => $this->t('Task File'),
      'help'  => $this->t('Document OCR task file.'),
      'field' => [
        'id' => 'document_ocr_task_file',
      ],
    ];
    // Status field
    $data['document_ocr_task']['task_status'] = [
      'title' => $this->t('Task Status'),
      'help'  => $this->t('Document OCR task status.'),
      'field' => [
        'id' => 'document_ocr_task_status',
      ],
    ];
    return $data;
  }

}
