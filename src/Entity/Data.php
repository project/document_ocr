<?php

namespace Drupal\document_ocr\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\FileInterface;

/**
 * Defines the Document OCR Data entity.
 *
 * @ingroup document_ocr
 *
 * @ContentEntityType(
 *   id = "document_ocr_data",
 *   label = @Translation("Document OCR Data"),
 *   base_table = "document_ocr_data",
 *   admin_permission = "administer document ocr",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   }
 * )
 */
class Data extends ContentEntityBase implements ContentEntityInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['file'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('File'))
      ->setDescription(t('File reference.'))
      ->setSetting('target_type', 'file')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['entity_type_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity type ID'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['bundle'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Bundle'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Entity ID'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['data'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Data'))
      ->setDescription(t('Store processed data.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    if ($file = $this->getFile()) {
      return $file->getFilename();
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function setData($data) {
    $this->set('data', $data);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return !empty($this->get('data')->value) ? json_decode($this->get('data')->value, TRUE) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function attachDestination($entity) {
    $this->set('entity_type_id', $entity->getEntityTypeId());
    $this->set('bundle', $entity->bundle());
    $this->set('entity_id', $entity->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationEntity() {
    if (!$this->get('entity_type_id')->isEmpty()) {
      if ($entity = $this->entityTypeManager()
          ->getStorage($this->get('entity_type_id')->value)
          ->load($this->get('entity_id')->value)) {
        return $entity;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    if (!$this->get('file')->isEmpty()) {
      if ($entity = $this->get('file')->first()->get('entity')->getTarget()) {
        return $entity->getValue();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function attachFile(FileInterface $file) {
    $this->set('file', $file->id());
    return $this;
  }

}
