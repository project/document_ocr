<?php

namespace Drupal\document_ocr\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Drupal\document_ocr\DocumentOcrInterface;

/**
 * Defines the Document OCR Task entity.
 *
 * @ingroup document_ocr
 *
 * @ContentEntityType(
 *   id = "document_ocr_task",
 *   label = @Translation("Document OCR Task"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\document_ocr\Controller\TaskListBuilder",
 *     "views_data" = "Drupal\document_ocr\Entity\ViewsData\Task",
 *     "form" = {
 *       "entities" = "\Drupal\document_ocr\Entity\Form\Task\OlderEntitiesForm",
 *       "process" = "\Drupal\document_ocr\Entity\Form\Task\ProcessForm",
 *       "restart" = "\Drupal\document_ocr\Entity\Form\Task\RestartForm",
 *       "delete" = "\Drupal\document_ocr\Entity\Form\Task\DeleteForm"
 *     },
 *   },
 *   base_table = "document_ocr_task",
 *   admin_permission = "administer document ocr",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/config/structure/document-ocr/tasks",
 *     "entities" = "/admin/config/structure/document-ocr/tasks/older",
 *     "process" = "/admin/config/structure/document-ocr/task/{document_ocr_task}/process",
 *     "restart" = "/admin/config/structure/document-ocr/task/{document_ocr_task}/restart",
 *     "delete" = "/admin/config/structure/document-ocr/task/{document_ocr_task}/delete"
 *   }
 * )
 */
class Task extends ContentEntityBase implements ContentEntityInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['document_ocr_mapping'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Document OCR'))
      ->setDescription(t('Document OCR Mapping entity reference.'))
      ->setSetting('target_type', 'document_ocr_mapping')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['processor'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Processor'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['source_entity_type_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Source Entity type ID'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['source_bundle'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Source Bundle'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['source_entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Source Entity ID'))
      ->setDescription(t('Imported entity ID.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['source_entity_revision_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Source Entity Revision ID'))
      ->setDescription(t('Imported entity revision ID.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['field_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Field Name'))
      ->setDescription(t('Document OCR Mapping field name.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['file'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('File'))
      ->setDescription(t('Document File entity reference.'))
      ->setSetting('target_type', 'file')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['file_details'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('File Details'))
      ->setDescription(t('Store task file details.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['destination_entity_type_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Destination Entity type ID'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['destination_bundle'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Destination Bundle'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['destination_entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Destination Entity ID'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['status'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Task Status'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['attempts'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Process Attempts'))
      ->setDescription(t('Number of process attempts.'))
      ->setDefaultValue(0)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['last_attempt'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Last Attempt'))
      ->setDescription(t('Last process attempt timestamp.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['data'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Data'))
      ->setDescription(t('Store task related data.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    if ($file = $this->getFile()) {
      return $this->t('%filename (File ID: %fid)', [
        '%filename' => $file->getFilename(),
        '%fid' => $file->id()
      ]);
    }
    else {
      $file_details = $this->getFileDetails();
      return $this->t('%filename (non-existing file)', [
        '%filename' => $file_details['filename']
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function attempt() {
    return $this->setAttempts($this->getAttempts() + 1);
  }

  /**
   * {@inheritdoc}
   */
  public function getAttempts() {
    return (int) $this->get('attempts')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAttempts($attempts) {
    $this->set('attempts', (int) $attempts);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function resetAttempts() {
    return $this->setAttempts(0);
  }

  /**
   * {@inheritdoc}
   */
  public function getLastAttemptTime() {
    return $this->get('last_attempt')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLastAttemptTime($timestamp = NULL) {
    $this->set('last_attempt', !empty($timestamp) ? $timestamp : time());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDocumentOcr() {
    if (!$this->get('document_ocr_mapping')->isEmpty()) {
      if ($entity = $this->get('document_ocr_mapping')->first()->get('entity')->getTarget()) {
        return $entity->getValue();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setDocumentOcr($document_ocr_mapping) {
    $this->set('document_ocr_mapping', $document_ocr_mapping);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    if (!$this->get('file')->isEmpty()) {
      if ($entity = $this->get('file')->first()->get('entity')->getTarget()) {
        return $entity->getValue();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function attachFile(File $file) {
    $this->set('file', $file->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setProcessed() {
    $this->set('status', DocumentOcrInterface::TASK_STATUS_PROCESSED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setPending() {
    $this->set('status', DocumentOcrInterface::TASK_STATUS_PENDING);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setInitiated() {
    $this->set('status', DocumentOcrInterface::TASK_STATUS_INIT);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setFailed() {
    $this->set('status', DocumentOcrInterface::TASK_STATUS_FAILED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isProcessed() {
    return $this->get('status')->value == DocumentOcrInterface::TASK_STATUS_PROCESSED;
  }

  /**
   * {@inheritdoc}
   */
  public function isPending() {
    return $this->get('status')->value == DocumentOcrInterface::TASK_STATUS_PENDING;
  }

  /**
   * {@inheritdoc}
   */
  public function isInitiated() {
    return $this->get('status')->value == DocumentOcrInterface::TASK_STATUS_INIT;
  }

  /**
   * {@inheritdoc}
   */
  public function isFailed() {
    return $this->get('status')->value == DocumentOcrInterface::TASK_STATUS_FAILED;
  }

  /**
   * {@inheritdoc}
   */
  public function getTaskStatus($labels = FALSE) {
    if ($labels) {
      switch ($this->get('status')->value) {
        case DocumentOcrInterface::TASK_STATUS_INIT:
          $status = $this->t('Initiated');
          break;
        case DocumentOcrInterface::TASK_STATUS_PENDING:
          $status = $this->t('Pending');
          break;
        case DocumentOcrInterface::TASK_STATUS_PROCESSED:
          $status = $this->t('Processed');
          break;
        case DocumentOcrInterface::TASK_STATUS_FAILED:
          $status = $this->t('Failed');
          break;
        default:
          $status = $this->t('N/A');
          break;
      }
      return  Markup::create('<span class="views-field">' . (
        ($this->get('status')->value == DocumentOcrInterface::TASK_STATUS_PROCESSED)
          ? '<span class="marker marker--published">' . $status . '</span>'
          : '<span class="marker marker--' . $this->get('status')->value . '">' . $status . '</span>') . '</span>');
    }
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProcessor($processor) {
    $this->set('processor', $processor);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessor() {
    return $this->get('processor')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessorReference() {
    return $this->entityTypeManager()->getStorage('document_ocr_processor')->load($this->getProcessor());
  }

  /**
   * {@inheritdoc}
   */
  public function setTaskData($data = []) {
    if ($existing = $this->getTaskData()) {
      $data = array_merge($existing, $data);
    }
    $this->set('data', json_encode($data));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTaskData() {
    return !empty($this->get('data')->value) ? json_decode($this->get('data')->value, TRUE) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function restart() {
    return $this->resetAttempts()
    ->setInitiated();
  }

  /**
   * {@inheritdoc}
   */
  public function setFileDetails($file_details = []) {
    if ($existing = $this->getFileDetails()) {
      $file_details = array_merge($existing, $file_details);
    }
    $this->set('file_details', json_encode($file_details));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileDetails($file_details = []) {
    return !empty($this->get('file_details')->value) ? json_decode($this->get('file_details')->value, TRUE) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteFileDetails($props) {
    if (!empty($props)) {
      $file_details = $this->getFileDetails();
      if (is_array($props)) {
        $file_details = array_diff_key($file_details, array_flip($props));
      }
      else {
        if (!empty($file_details[$props])) {
          unset($file_details[$props]);
        }
      }
      $this->set('file_details', json_encode($file_details));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function attachSource($entity) {
    $this->set('source_entity_type_id', $entity->getEntityTypeId());
    $this->set('source_bundle', $entity->bundle());
    $this->set('source_entity_id', $entity->id());
    $this->set('source_entity_revision_id', $entity->getRevisionId());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceEntity() {
    if (!$this->get('source_entity_type_id')->isEmpty()) {
      if (empty($this->get('source_entity_id')->value)) {
        return NULL;
      }
      if ($entity = $this->entityTypeManager()
          ->getStorage($this->get('source_entity_type_id')->value)
          ->load($this->get('source_entity_id')->value)) {
        return $entity;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function attachField($field_name) {
    $this->set('field_name', $field_name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function attachDestination($entity) {
    $this->set('destination_entity_type_id', $entity->getEntityTypeId());
    $this->set('destination_bundle', $entity->bundle());
    $this->set('destination_entity_id', $entity->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationEntity() {
    if (!$this->get('destination_entity_type_id')->isEmpty()) {
      if ($entity = $this->entityTypeManager()
          ->getStorage($this->get('destination_entity_type_id')->value)
          ->load($this->get('destination_entity_id')->value)) {
        return $entity;
      }
    }
  }

}
