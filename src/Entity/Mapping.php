<?php

namespace Drupal\document_ocr\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the Document OCR Mapping entity.
 *
 * @ingroup document_ocr
 *
 * @ConfigEntityType(
 *   id = "document_ocr_mapping",
 *   label = @Translation("Document OCR Mapping"),
 *   handlers = {
 *     "list_builder" = "Drupal\document_ocr\Controller\MappingListBuilder",
 *     "form" = {
 *       "delete" = "\Drupal\Core\Entity\EntityDeleteForm",
 *       "edit" = "\Drupal\document_ocr\Entity\Form\Mapping\EditForm",
 *       "settings" = "\Drupal\document_ocr\Entity\Form\Mapping\SettingsForm",
 *       "template_file" = "\Drupal\document_ocr\Form\Mapping\TemplateFileForm",
 *       "mapping" = "\Drupal\document_ocr\Entity\Form\Mapping\MappingForm",
 *       "process" = "\Drupal\document_ocr\Entity\Form\Mapping\ProcessForm",
 *     },
 *   },
 *   admin_permission = "administer document ocr",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "status" = "status"
 *   },
 *   config_prefix = "mapping",
 *   config_export = {
 *     "id",
 *     "label",
 *     "field_selector",
 *     "destination_bundle",
 *     "settings",
 *     "processor",
 *     "mapping",
 *   },
 *   lookup_keys = {
 *     "id",
 *   },
 *   links = {
 *     "collection" = "/admin/config/structure/document-ocr",
 *     "new" = "/admin/config/structure/document-ocr/new",
 *     "edit" = "/admin/config/structure/document-ocr/{document_ocr_mapping}/edit",
 *     "settings" = "/admin/config/structure/document-ocr/{document_ocr_mapping}/settings",
 *     "mapping" = "/admin/config/structure/document-ocr/{document_ocr_mapping}/mapping",
 *     "process" = "/admin/config/structure/document-ocr/{document_ocr_mapping}/process",
 *     "template_file" = "/admin/config/structure/document-ocr/{document_ocr_mapping}/template-file",
 *     "enable" = "/admin/config/structure/document-ocr/{document_ocr_mapping}/enable",
 *     "disable" = "/admin/config/structure/document-ocr/{document_ocr_mapping}/disable",
 *     "delete" = "/admin/config/structure/document-ocr/{document_ocr_mapping}/delete",
 *   }
 * )
 */
class Mapping extends ConfigEntityBase implements ConfigEntityInterface {

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The label.
   *
   * @var string
   */
  protected $label;

  /**
   * Field selector configuration settings.
   *
   * @var array
   */
  protected $field_selector = [];

  /**
   * Destination entity type.
   *
   * @var array
   */
  protected $destination_bundle = [];

  /**
   * Processor credentials.
   *
   * @var array
   */
  protected $credentials = [];

  /**
   * Processor configuration.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * Processor settings.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * Processor plugin ID.
   *
   * @var array
   */
  protected $processor;

  /**
   * Processor and Drupal entity mapping.
   *
   * @var array
   */
  protected $mapping = [];

  /**
   * {@inheritdoc}
   */
  public function getFieldSelector() {
    if (!is_array($this->field_selector) || empty(array_values($this->field_selector))) {
      return FALSE;
    }
    else {
      return $this->field_selector;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldSelector(array $field_selector) {
    $this->field_selector = $field_selector;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationBundle() {
    if (!is_array($this->destination_bundle) || empty(array_values($this->destination_bundle))) {
      return FALSE;
    }
    else {
      return $this->destination_bundle;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setDestinationBundle(array $destination_bundle) {
    $this->destination_bundle = $destination_bundle;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCredentials() {
    return $this->credentials;
  }

  /**
   * {@inheritdoc}
   */
  public function setCredentials(array $credentials) {
    $this->credentials = $credentials;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    if (!is_array($this->configuration) || empty(array_values($this->configuration))) {
      return FALSE;
    }
    else {
      return $this->configuration;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings() {
    if (!is_array($this->settings) || empty(array_values($this->settings))) {
      return FALSE;
    }
    else {
      return $this->settings;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setSettings(array $settings) {
    $this->settings = $settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessor() {
    return $this->processor;
  }

  /**
   * {@inheritdoc}
   */
  public function setProcessor($processor) {
    $this->processor = $processor;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessorReference() {
    return $this->entityTypeManager()->getStorage('document_ocr_processor')->load($this->getProcessor());
  }

  /**
   * {@inheritdoc}
   */
  public function getMapping() {
    if (!is_array($this->mapping) || empty(array_values($this->mapping))) {
      return FALSE;
    }
    else {
      return $this->mapping;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setMapping(array $mapping) {
    $this->mapping = $mapping;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return (bool) $this->get('status');
  }

}
