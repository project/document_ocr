<?php

namespace Drupal\document_ocr\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the Document OCR Processor entity.
 *
 * @ingroup document_ocr
 *
 * @ConfigEntityType(
 *   id = "document_ocr_processor",
 *   label = @Translation("Document OCR Processor"),
 *   handlers = {
 *     "list_builder" = "Drupal\document_ocr\Controller\ProcessorListBuilder",
 *     "form" = {
 *       "delete" = "\Drupal\Core\Entity\EntityDeleteForm",
 *       "edit" = "\Drupal\document_ocr\Entity\Form\Processor\EditForm",
 *       "configuration" = "\Drupal\document_ocr\Entity\Form\Processor\ConfigurationForm",
 *     },
 *   },
 *   admin_permission = "administer document ocr",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_prefix = "processor",
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "credentials",
 *     "configuration",
 *     "processor",
 *   },
 *   lookup_keys = {
 *     "id",
 *   },
 *   links = {
 *     "collection" = "/admin/config/structure/document-ocr/processors",
 *     "new" = "/admin/config/structure/document-ocr/processors/new",
 *     "edit" = "/admin/config/structure/document-ocr/processors/{document_ocr_processor}/edit",
 *     "configuration" = "/admin/config/structure/document-ocr/processors/{document_ocr_processor}/configuration",
 *     "delete" = "/admin/config/structure/document-ocr/processors/{document_ocr_processor}/delete",
 *   }
 * )
 */
class Processor extends ConfigEntityBase implements ConfigEntityInterface {

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The label.
   *
   * @var string
   */
  protected $label;

  /**
   * The processor description.
   *
   * @var string
   */
  protected $description;

  /**
   * Processor credentials.
   *
   * @var string
   */
  protected $credentials;

  /**
   * Processor configuration settings.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * Processor plugin ID.
   *
   * @var string
   */
  protected $processor;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCredentials() {
    return $this->credentials;
  }

  /**
   * {@inheritdoc}
   */
  public function setCredentials($credentials) {
    $this->credentials = $credentials;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    if (!is_array($this->configuration) || empty(array_values($this->configuration))) {
      return FALSE;
    }
    else {
      return $this->configuration;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessor() {
    return $this->processor;
  }

  /**
   * {@inheritdoc}
   */
  public function setProcessor($processor) {
    $this->processor = $processor;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessorPlugin() {
    try {
      return \Drupal::service('plugin.manager.document_ocr_processor')->createInstance($this->getProcessor());
    }
    catch (\Exception $e) {
      \Drupal::logger('document_ocr')->error($e->getMessage());
    }
  }

}
