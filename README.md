# Document OCR

The Document OCR module is an integration interface for the OCR services like
Google Document AI and other services to get contents of image and PDF files 
as text and if supported by the OCR service export text as properties.
The module will map properties and import contents into Drupal entities.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/document_ocr).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/document_ocr).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the following libraries (included in the composer.json):

- `google/cloud-document-ai`
- `google/cloud-storage`
- `google/cloud-translate`
- `google/cloud-text-to-speech`
- `openai-php/client`
- `smalot/pdfparser`
- `thiagoalessio/tesseract_ocr`
- `spatie/pdf-to-text`

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Configure the Document OCR at (/admin/config/structure/document-ocr).

### PDF Parser

No configuration is required, some options availalbe the UI.

### [Google Document AI](https://cloud.google.com/document-ai)

1. Create new project [here](https://console.cloud.google.com/projectcreate)
2. Make sure that billing is enabled for your Google Cloud project. Learn how to
[check if billing is enabled on a project](https://cloud.google.com/billing/docs/how-to/verify-billing-enabled).
3. [Enable Google Document AI API](https://console.cloud.google.com/flows/enableapi?apiid=documentai.googleapis.com)\
4. Enable Google Cloud Stoage API. This is needed to process large PDF files (15 and more pages).
5. Make sure you grant appropirate permissions (IAM) for the principal.
6. Set up authentication and [create service account](https://console.cloud.google.com/projectselector/iam-admin/serviceaccounts/create?supportedpurview=project)
7. Get credentials JSON file and upload it to the private directory `private://document-ocr/document-ai-credentials.json`
8. Use credentials file when enabling Google Document AI processor in the module.

The credentials file should look similar to this:
```
{
  "type": "service_account",
  "project_id": "...412",
  "private_key_id": "...d0696ec7",
  "private_key": "-----BEGIN PRIVATE KEY-----\n...\n-----END PRIVATE KEY-----\n",
  "client_email": ...412.iam.gserviceaccount.com",
  "client_id": "...51918854",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/...412.iam.gserviceaccount.com",
  "universe_domain": "googleapis.com"
}

```

IMPORTANT: When you create new account it takes several minutes before you account becomes active.
If you can't see the processor configuration form please wait and try again later.

Detailed setup steps are [here](https://cloud.google.com/document-ai/docs/setup)

### Google Translate

You would need to follow the same steps as for Google Document AI but instead you would need to also
enable Google Cloud Translation API. Same credential Google Document AI JSON credentials file can be used.

### Google Text to Speech

You would need to follow the same steps as for Google Document AI but instead you would need to also
enable Google Cloud Text-to-Speech API. Same credential Google Document AI JSON credentials file can be used.

### Microsoft Azure Translate

Enable Translate service and get the API credentials. Use the following format for the credentials JSON file:
```
{
  "host": "https://api.cognitive.microsofttranslator.com/",
  "Ocp-Apim-Subscription-Key": "YOUR API KEY",
  "Ocp-Apim-Subscription-Region": "REGION"
}
```

### [OpenAI](https://openai.com/)

1. Create API Key [here](https://platform.openai.com/account/api-keys)
2. Get API key and API Organization (optional)
3. Create JSON file and store your credentials as follows:
```
{
  "apikey": "[API-KEY]",
  "apiorg": "[API-ORG]" (optional)
}
```
4. Upload it to the private directory `private://document-ocr/openai-credentials.json`
5. Point to the credentials file when adding OpenAI transformer
6. You should be all set to use OpenAI integration

### Microsoft Azure OpenAI

You can use the same OpenAI integration processor and transformer plugins with different credentials.
For Azure OpenAI integraiton you would need to change your credentials JSON file to the following format:
```
{
  "baseUrl": "{your-resource-name}.openai.azure.com/openai/deployments/{deployment-id}",
  "apiKey": "{your-api-key}",
  "apiVersion": "{version}"
}
```

## Maintainers

- [Minnur Yunusov (minnur)](https://www.drupal.org/u/minnur)
- [Chapter Three](https://www.drupal.org/chapter-three) Sponsored the module development and ongoing support
