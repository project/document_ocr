(function ($, Drupal) {

  Drupal.behaviors.processorAdmin = {
    attach: function (context, settings) {
      const credentials = '.form-item--credentials';
      const processor = 'select[name="processor"]';
      let processors = settings.document_ocr.processors;
      let current_val = $(processor).val();
      if (current_val === '') {
        $(credentials).hide();
      }
      $(processor).on('change', function() {
        let selected_val = $(this).val();
        if (processors[selected_val] !== undefined && processors[selected_val] === false) {
          $(credentials).hide();
        }
        else {
          $(credentials).show();
        }
      });
    }
  };

})(jQuery, Drupal);
