(function ($, Drupal) {

  Drupal.behaviors.GoogleDocumentAi = {
    attach: function (context, settings) {
      $('select[name="location_name"]').on('change', function() {
        $('select[name="processor_version"]').empty().append( new Option(Drupal.t('- Select Processor Version -'), '') ).trigger('change');
      });
    }
  };

})(jQuery, Drupal);
