(function ($, Drupal) {

  Drupal.behaviors.mappingAdmin = {
    attach: function (context, settings) {
      const template_file = '#template-file-container';
      const processor = 'select[name="processor"]';
      let current_val = $(processor).val();
      if (current_val === '') {
        $(template_file).hide();
      }
      $(processor).on('change', function() {
        let selected_val = $(this).val();
        if (selected_val === false || selected_val == '') {
          $(template_file).hide();
        }
        else {
          $(template_file).show();
        }
      });
    }
  };

})(jQuery, Drupal);
