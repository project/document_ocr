(function ($, Drupal) {

  Drupal.behaviors.transformerAdmin = {
    attach: function (context, settings) {
      const credentials = '.form-item--credentials';
      const transformer = 'select[name="transformer"]';
      let transformers = settings.document_ocr.transformers;
      let current_val = $(transformer).val();
      if (current_val === '') {
        $(credentials).hide();
      }
      $(transformer).on('change', function() {
        let selected_val = $(this).val();
        if (transformers[selected_val] !== undefined && transformers[selected_val] === false) {
          $(credentials).hide();
        }
        else {
          $(credentials).show();
        }
      });
    }
  };

})(jQuery, Drupal);
