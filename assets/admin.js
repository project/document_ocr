(function ($, Drupal) {

  Drupal.behaviors.documentOcrAdmin = {
    attach: function (context, settings) {
      $('form[class^="document-ocr-"]', context).on('submit', function() {
        $(this).find('.button--primary.form-submit').addClass('document-ocr-hide-text');
        $(this).find('.document-ocr-button-spinner').addClass('document-ocr-in-progress');
      });
    }
  };
  $(document).on('dialogopen', function(e, ui) {
    $('form[class^="document-ocr-"]').on('submit', function() {
      $(this).closest('.ui-dialog-buttons').find('.button--primary.form-submit').addClass('document-ocr-in-progress');
    });
  });

})(jQuery, Drupal);
